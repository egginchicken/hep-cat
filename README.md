[//]: # (Initialization)
[title_screen]: readme/images/title_screen.png "Main Title Screen"
[logo]: readme/gifs/logo.gif "Egg logo animation"
[itch_badge]: readme/images/itch_badge.png "itch.io link"
[license_img]: https://i.creativecommons.org/l/by-nc/4.0/88x31.png "Creative Commons License"
[gameplay_gif]: readme/gifs/gameplay.gif "Gameplay gif"

[//]: # (Body)
![Main Title Screen image][title_screen]

Hep Cat
=======

Hep Cat is set in the American Jazz age and tells a story about a musician who, experiencing  
artist's block, finds inspiration in the form of love towards a bizarre, almost magical figure   
he happens to encounter in the middle of a calamity.  The game follows the main character,  
Hep Cat, as he composes a serenade for his newfound muse.  

The game can be downloaded free for Windows and macOS. 

[Here's the itch.io link.](https://just-a-phantom.itch.io/hep-cat)  

[![itch.io badge][itch_badge]](https://just-a-phantom.itch.io/hep-cat)

[Trailer](http://www.youtube.com/watch?v=riA-jO9S3IQ)
-----------------------------------------------------
[![Trailer preview](http://img.youtube.com/vi/riA-jO9S3IQ/0.jpg)](http://www.youtube.com/watch?v=riA-jO9S3IQ)

[Gameplay footage](http://www.youtube.com/watch?v=JhBbqaUFGqM)
------------------------------------------------------
![Gameplay gif][gameplay_gif]

[![Gameplay preview](http://img.youtube.com/vi/JhBbqaUFGqM/0.jpg)](http://www.youtube.com/watch?v=JhBbqaUFGqM)


The team
--------
This is a term project under [CMPUT 250](https://sites.google.com/ualberta.ca/cmput250/) Winter 2020 in University of Alberta.

Made by **E.G.G. Inc.**:  

![Egg logo][logo]

* **Emily Casavant** as executive producer  
* **Samadhi Ranatunga** as producer / programmer / writer  
* **Adriana Jacobs** as lead designer / musician  
* **Cynara Cypreste** as story artist / writer  
* **Allan Manuba** as programmer  
* **Nicole Chik** as pixel artist  
* **Harrold Paderan** as programmer  

How to download and start the game (Windows)
--------------------------------------------
1. Download the appropriate zipped project build folder.  
2. Extract all files from the zipped folder.  
3. In the extracted files folder, you will find **Game.exe**. This will have an icon.  
	Click on this icon. Running the application will start the game.  
	* You also have the option to create a shortcut to it and place the shortcut   
	in an easy to access location


How to download and start the game (Mac)
--------------------------------------------
1. Download the appropriate zipped project build folder.  
2. Extract all files from the zipped folder.  
3. Go into your downloads and click on the folder.  
4. After opening the folder, click and drag the icon labelled **Game.app** into your dock  
	(the bar with your applications) or onto your desktop.  
5. *Depending on your safety settings, you can either begin the game or it will say that  
	the game file can not be opened. If that is the case:  
	6. Click on the application icon (in your dock or desktop) to open the game.  
	7. It will say that it cannot be opened.  
	8. Go to your system preferences application.  
	9. Then click on the security and privacy application.  
	10. At the bottom of the pop up page it will say **“open anyway”**.  
	11. Click on this: it gives your computer the permission to open up our application   
	12. Go back to the game application and open the game!  


Instructions for how to play
----------------------------
### General Controls
Players have the option to choose **WASD** or **arrow keys** (can change back and forth in the  
**options** menu) to walk around and play the rhythm section of the game. Players can also  
use the **spacebar** and **Enter key** to interact with interactable objects and speak to characters.

### Options
Although the game has auto-save, the player can still choose to **save** in the **options** menu  
(Press **ESC** to access it and to exit)

If the player is confident enough with their hardware’s CPU speeds (not RAM or GPU),  
they may have the option to turn on particles. Particles are additional visual effects  
in the rhythm game. Press **ESC** to toggle **Particles**. Turn this off if putting  
your hardware to max performance still causes visual lags.

### Rhythm game
In the music game, the player must use either the **arrow keys (up, down, right, left)** or **WASD**  
(according to their choice) in time.

Credits
-------
**This section is work in progress**

License
-------
[![Creative Commons License][license_img]](https://creativecommons.org/licenses/by-nc/4.0/)  
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/)