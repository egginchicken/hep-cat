/*:
 * @author Harrold Paderan
 * @plugindesc Tryna change pause menu. My First Plugin.
 * 
 * @param Show Intro Logo
 * @desc Enabled/Disables showing EGG Logo.
 * OFF - false     ON - true
 * Default: ON
 * @default true
 * 
 * @param Show Intro Video
 * @desc Enabled/Disables showing the Hep Cat Intro Video.
 * OFF - false     ON - true
 * Default: ON
 * @default true
 * 
 * @param Intro Logo Name
 * @desc The filename of the logo video. Case sensitive. 
 * Do not add the file extension. Assumes file is .webm
 * 
 * @help
 * 
 *  ** Dialogue Name Tags **
 * To use, create plugin command:
 *  - WheatNametag N 
 * where N is a number corresponing to mapping:
 *      1 : Hep Cat
 *      2 : Bongo
 *      3 : Allie
 *      4 : Charlies
 *      5 : Death
 * 
 * - The list can be expanded
 * - **Need a plugin command for each show text**
*/

var lh = Window_Base.prototype.lineHeight() * 2;

/** Pause Scene */
class Hep_PauseMenu extends Scene_Base {
    constructor() {
        super();
    }

    create() {
        super.create();
        
        this._backgroundSprite = new Sprite();
        this._backgroundSprite.bitmap = SceneManager.backgroundBitmap();
        this.pause_sprite = new Sprite(ImageManager.loadPicture("pause_back"));
        this.addChild(this._backgroundSprite);
        this.addChild(this.pause_sprite);
        this.createWindowLayer();
    }

    update() {
        if (Input.isTriggered('ok')){
            this.popScene();
        }
        if (Input.isTriggered('escape')){
            this.popScene();
        }
        super.update();
    }

    terminate() {
        super.terminate();
        //console.log("Hep pause terminate");
    }
}

/** The Custom Pause
// override the call menu func to call my pause instead of default
//console.log("debug1");
var menu_original = Scene_Map.prototype.callMenu;
Scene_Map.prototype.callMenu = function() {
    SoundManager.playCursor();
    SceneManager.push(Hep_PauseMenu);
    $gameTemp.clearDestination();
    this._mapNameWindow.hide();
    this._waitCount = 2;
};
*/

Window_MenuCommand.prototype.makeCommandList = function() {
    this.addOptionsCommand();
    this.addSaveCommand();
    this.addGameEndCommand();
};

// var scMenu_init = Scene_Menu.prototype.initialize;
// Scene_Menu.prototype.initialize = function() {
//     $gameSystem._windowSkin = "Window";
//     scMenu_init.apply(this,arguments);
// }

Scene_Menu.prototype.start = function() {
    Scene_MenuBase.prototype.start.call(this);
};

Scene_Menu.prototype.create = function() {
    Scene_MenuBase.prototype.create.call(this);
    this.createCommandWindow();
};

Scene_Menu.prototype.createCommandWindow = function() {
    var wheat_x = (Graphics.boxWidth - 240) / 2;
    var y = 4 * 36 + 6 * 2;
    var wheat_y = (Graphics.boxHeight - y) / 2 ;
    this._commandWindow = new Window_MenuCommand(wheat_x, wheat_y);
    this._commandWindow.setHandler('options',   this.commandOptions.bind(this));
    this._commandWindow.setHandler('save',      this.commandSave.bind(this));
    this._commandWindow.setHandler('gameEnd',   this.commandGameEnd.bind(this));
    this._commandWindow.setHandler('cancel',    this.popScene.bind(this));//this.cancelPauseMenu.bind(this));
    this.addWindow(this._commandWindow);
};

// Scene_Menu.prototype.cancelPauseMenu = function() {
//     $gameSystem._windowSkin = "Window";
//     this.popScene();
//     console.log("Closed pause menu");
// };

// // changing window skins
// Window_Base.prototype.loadWindowskin = function() {
//     this.windowskin = ImageManager.loadSystem($gameSystem._windowSkin);
// };

// var gSys_init = Game_System.prototype.initialize;
// Game_System.prototype.initialize = function() {
//     gSys_init.apply(this, arguments);
//     this._windowSkin = "Window";
// };

/** Song Choice Previews */

var setUpChoices_orig;
var cursorDown_orig;
var cursorUp_orig;

/** 
console.log("setting up new cursors");
var test_bgm = {
    name: "DiscoChoiceSamp",
    volume: AudioEffectManager.MASTER_VOLUME,
    pitch: 100,
    pan: 0
};
console.log(test_bgm);

var cursorDown_orig = Window_Selectable.prototype.cursorDown;
Window_Selectable.prototype.cursorDown = function(wrap) {
    cursorDown_orig.apply(this, arguments);
    bgm_preview(this.index());
};

var cursorUp_orig = Window_Selectable.prototype.cursorUp;
Window_Selectable.prototype.cursorUp = function(wrap) {
    cursorUp_orig.apply(this, arguments);
    bgm_preview(this.index());
};
console.log("finished set up cursors");*/

var _Wheat_pluginCommand = Game_Interpreter.prototype.pluginCommand;
Game_Interpreter.prototype.pluginCommand = function(command, args) {
    _Wheat_pluginCommand.call(this, command, args);
    if (typeof command != 'undefined') {
        command = command.toLowerCase();

        if (command === 'wheatchoice') {
            // handle args[0] == "set"
            if (args[0].toLowerCase() === "set") {
                var setUpChoices_orig = Game_Interpreter.prototype.setupChoices;
                Game_Interpreter.prototype.setupChoices = function(params) {
                    setUpChoices_orig.apply(this, arguments);
                    bgm_preview(0);
                };

                var cursorDown_orig = Window_Selectable.prototype.cursorDown
                Window_Selectable.prototype.cursorDown = function(wrap) {
                    cursorDown_orig.apply(this, arguments);
                    bgm_preview(this.index());
                };

                var cursorUp_orig = Window_Selectable.prototype.cursorUp
                Window_Selectable.prototype.cursorUp = function(wrap) {
                    cursorUp_orig.apply(this, arguments);
                    bgm_preview(this.index());
                };
            }

            // handle args[0] == "cancel"
            if (args[0].toLowerCase() === "cancel") {
                if (cursorDown_orig && cursorUp_orig){
                    Game_Interpreter.prototype.setupChoices = setUpChoices_orig;
                    Window_Selectable.prototype.cursorDown = cursorDown_orig;
                    Window_Selectable.prototype.cursorUp = cursorUp_orig;
                } else {
                    console.log("Origs are null");
                }
            }
        }

        if (command === 'wheatvolume') {
            // fade into new vol: where can i put code that runs on every frame??
            if (args[0] !== undefined) {
                var v_ = parseFloat(args[0]);
                AudioManager.bgmVolume = v_;
            } else {
                console.log("Incorrect usage.")
            }
        }

        if (command === 'wheatnametag') {
            var _n = args[0];
            var name = '';
            switch (_n) {
                case '0':
                    name = '';
                    break;
                case '1':
                    name = "Hep Cat";
                    break;
                case '2':
                    name = "Bongo";
                    break;
                case '3':
                    name = "Allie";
                    break;
                case '4':
                    name = "Charlie";
                    break;
                case '5':
                    name = "Death"
                    break;
                default:
                    name = "???";
            }
            $gameMessage.setNametag(name);
        }

        if (command === 'wheattest') {
            console.log("Not testing anything right now.");
        }
    }
}

// ==============================================================
/************ Nametag Window *************/
var lh = Window_Base.prototype.lineHeight() * 2;

function Nametag_Window() {
    this.initialize.apply(this,arguments);
}

Nametag_Window.prototype = Object.create(Window_Base.prototype);
Nametag_Window.prototype.constructor = Nametag_Window;

Nametag_Window.prototype.initialize = function() {
    this._name = '';
    this.wheat_height = lh;
    //var width = 170; // Window_Base.faceWidth()
    this.wheat_width = 170;//this.drawTextEx(this._name, 0, this.contents.height) + this.textPadding() * 2;
    this.wheat_x = 0;
    this.wheat_y = ($gameMessage.positionType() * (Graphics.boxHeight - this.fittingHeight(4)) / 2) - this.wheat_height;
    Window_Base.prototype.initialize.call(this, this.wheat_x, this.wheat_y, this.wheat_width, this.wheat_height);
    this.openness = 0;
    this.refresh();
};
Nametag_Window.prototype.open = function() {
    this.refresh();
    Window_Base.prototype.open.call(this);
};

Nametag_Window.prototype.refresh = function() {
    this.contents.clear();
    this._name = $gameMessage.nametag();
    this.drawText(this._name, 0, 0);
};

Nametag_Window.prototype.terminateMessage = function() {
    if ($gameMessage.nametag() === '') {
        this.close();
    }
};

/****************************************/
var gMess_init = Game_Message.prototype.initialize;
Game_Message.prototype.initialize = function() {
    gMess_init.apply(this, arguments);
    this._nametag = '';
};

Game_Message.prototype.nametag = function() {
    return this._nametag;
};

Game_Message.prototype.setNametag = function(newNametag) {
    this._nametag = newNametag;
};

var winMess_subWinds = Window_Message.prototype.createSubWindows;
Window_Message.prototype.createSubWindows = function() {
    winMess_subWinds.apply(this, arguments);
    this._nametagWindow = new Nametag_Window();

};

Window_Message.prototype.subWindows = function() {
    return [this._goldWindow, this._choiceWindow,
            this._numberWindow, this._itemWindow, this._nametagWindow];
};

Window_Message.prototype.open = function() {
    Window_Base.prototype.open.call(this);
    if ($gameMessage.nametag() !== '') {
        this._nametagWindow.open();
    }
};

var winMess_newPage = Window_Message.prototype.newPage;
Window_Message.prototype.newPage = function(textState) {
    winMess_newPage.apply(this, arguments);
};

var winMess_terminate = Window_Message.prototype.terminateMessage;
Window_Message.prototype.terminateMessage = function() {
    winMess_terminate.apply(this,arguments);
    this._nametagWindow.terminateMessage();
};

var winMess_onEndofText = Window_Message.prototype.onEndOfText;
Window_Message.prototype.onEndOfText = function() {
    $gameMessage.setNametag('');
    winMess_onEndofText.apply(this, arguments);
};
// ============================================================

var bgm_preview = function(_i) {
    // stop current bgm
    AudioManager.stopBgm();
    
    // play corresponding bgm
    const prefix = $gameMessage._choices[_i];
    if (prefix === null) {
        return;
    }

    var file_str = prefix + "ChoiceSample"; // FILE NAMES ARE IMPORTANT!!!!! CHANGE WHEN CROPPED MUSIC IS IN
    var _bgm = {
        name: file_str,
        volume: AudioEffectManager.MASTER_VOLUME,
        pitch: 100,
        pan: 0
    };
    console.log(_bgm)
    if (_bgm){
        AudioManager.playBgm(_bgm);
    }
};

/** Allan injecting particles here XD */
ConfigManager.PERFORMANCE_LIMIT = 750; // if the score goes above 750, by default, kill particles

ConfigManager._particles = false;

Object.defineProperty(ConfigManager, 'particles', {
    get: function () {
        return ConfigManager._particles;
    },
    set: function (value) {
        ConfigManager._particles = value;
    },
    configurable: true
});

Object.defineProperties(TextManager, {
    particles: TextManager.getter('message', 'particles')
});

/** Key Mapping */

ConfigManager._controlKeys = false;
Object.defineProperty(ConfigManager, 'controlKeys', {
    get: function() {
        return ConfigManager._controlKeys;
    },
    set: function(value) {
        ConfigManager._controlKeys = value;
 
        if (value) {
            // CASE: WASD movement
            Input.keyMapper[87] = "up";     //W
            Input.keyMapper[83] = "down";   //S
            Input.keyMapper[65] = "left";   //A
            Input.keyMapper[68] = "right";  //D
            // and disable arrow movement
            Input.keyMapper[37] = '';
            Input.keyMapper[38] = '';
            Input.keyMapper[39] = '';
            Input.keyMapper[40] = '';
            
            console.log("ctrl key set to true");
        } else {
            
            // CASE: Arrow keys movement
            Input.keyMapper[37] = 'left';   // left arrow
            Input.keyMapper[38] = 'up';     // up arrow
            Input.keyMapper[39] = 'right';  // right arrow
            Input.keyMapper[40] = 'down';   // down arrow
            // and disable WASD movement
            Input.keyMapper[87] = "";
            Input.keyMapper[83] = "";
            Input.keyMapper[65] = "";
            Input.keyMapper[68] = "";
            
           console.log("ctrl key set to false");
        }
    },
    configurable: true
});

Object.defineProperties(TextManager,{
    controlKeys : TextManager.getter('message', 'controlKeys')
});

// adding the command to ConfigManager. For saving configs
ConfigManager.makeData = function() {
    var config = {};
    config.controlKeys = this.controlKeys;
    config.particles = this.particles;
    config.alwaysDash = this.alwaysDash;
    config.commandRemember = this.commandRemember;
    config.bgmVolume = this.bgmVolume;
    config.bgsVolume = this.bgsVolume;
    config.meVolume = this.meVolume;
    config.seVolume = this.seVolume;
    config.wasPerformanceMeasured = this.wasPerformanceMeasured;
    return config;
};

// for loading configs
var applyData_orig = ConfigManager.applyData;
ConfigManager.applyData = function(config) {
    applyData_orig.apply(this, arguments);
    ConfigManager.controlKeys = this.readFlag(config, 'controlKeys');
    ConfigManager.particles = this.readFlag(config, 'particles');

    // todo: find a way to save a one way thing
    if (false) {
        var arr = [];

        while (arr.length < 15) {
            const score = Daikon.testMachinePerformance();
            if (score > 0) {
                arr.push(arr);
            }
        }

        arr.sort();

        const median = arr[6];

        ConfigManager._particles = (median < ConfigManager.PERFORMANCE_LIMIT);

        console.log("Particle option successfully decided");
        ConfigManager.wasPerformanceMeasured = true;
    }
}

// text-ifying the values of the option
Window_Options.prototype.controlKeysStatusText = function(value) {
    return value ? 'WASD' : 'ARROWS';
};

// Check if controlKey option
Window_Options.prototype.isControlKeysSymbol = function(symbol) {
    return symbol.contains('Keys');
};

// displays value as text
Window_Options.prototype.statusText = function(index) {
    var symbol = this.commandSymbol(index);
    var value = this.getConfigValue(symbol);
    if (this.isVolumeSymbol(symbol)) {
        return this.volumeStatusText(value);
    } else if (this.isControlKeysSymbol(symbol)) {
        return this.controlKeysStatusText(value);
    } else {
        return this.booleanStatusText(value);
    }
};

// draw the commands
var makeCommandList_orig = Window_Options.prototype.makeCommandList;
Window_Options.prototype.makeCommandList = function () {
    this.addCommand(TextManager.controlKeys, 'controlKeys');
    this.addCommand("Particles", 'particles');
    makeCommandList_orig.apply(this,arguments);
};

/** Disable clicks */
TouchInput.initialize = function() {
    this.clear();
    //this._setupEventHandlers();
};

/******************************/
// Playing Piano

class PianoPlayer {
    constructor() {
        this.orig_keymap = {};
    }

    static initialize() {
        //this.orig_keymap = Input.keyMapper;
        this.orig_keymap = Object.assign({}, Input.keyMapper);
        this._stopPlaying = false;
        // map keys here Input.keyMapper[code] = 'string'
        Input.keyMapper[65] = 'a';
        Input.keyMapper[83] = 's';
        Input.keyMapper[68] = 'd';
        Input.keyMapper[70] = 'f';
        Input.keyMapper[71] = 'g';
        Input.keyMapper[72] = 'h';
        Input.keyMapper[74] = 'j';
        Input.keyMapper[75] = 'k';
        Input.keyMapper[76] = 'l';
    }

    static clear() {
        //Input.keyMapper = this.orig_keymap;
        Input.keyMapper = Object.assign({},this.orig_keymap);
    }
}

/*************************************************/
// Intro Video before title

// override boot
var _Scene_Boot_start = Scene_Boot.prototype.start;
Scene_Boot.prototype.start = function() {
    if (!DataManager.isBattleTest() && !DataManager.isEventTest()) {
        SceneManager.goto(Scene_IntroVid);
    } else {
        _Scene_Boot_start.call(this);
    }
};

// new scene class
function Scene_IntroVid() {
    this.initialize.apply(this,arguments);
}

Scene_IntroVid.prototype = Object.create(Scene_Base.prototype);
Scene_IntroVid.prototype.constructor = Scene_IntroVid;

Scene_IntroVid.prototype.initialize = function() {
    Scene_Base.prototype.initialize.call(this);
    this._vidname = 'Cutscene_Prologue_WITH_AUDIO.webm'; // should be constant right?
    //this.fadeScreen = new PictureCorn("BlackScreen");
    //this.logoStartPic = new PictureCorn("LogoStart");
    //this.logoEndPic = new PictureCorn("LogoEnd");
    // this.fadeScreen = new Game_Picture();
    // this.logoStartPic = new Game_Picture();
    // this.logoEndPic = new Game_Picture();

    // bools
    this._vidHasStarted = false;
    this._isDonePlayingVid = false;
    this._logoHasStarted = false;
    this._isDonePlayingLogo = false;
};

// play video 
Scene_IntroVid.prototype.start = function() {
    Scene_Base.prototype.start.call(this);
    SceneManager.clearStack();
    
    if (PluginManager.parameters("WheatFlour")["Show Intro Logo"].toLowerCase() === "false") {
        this._isDonePlayingLogo = true;
    }
    if (PluginManager.parameters("WheatFlour")["Show Intro Video"].toLowerCase() === "false") {
        this._isDonePlayingVid = true;
    }
}

// update, if not video is playing, exit
Scene_IntroVid.prototype.update = function() {
    if      (!this.updateLogo()     ) {} 
    else if (!this.updateIntroVid() ) {}  
    else {
        //end here
        console.log("end of intros");
        this.gotoTitleOrTest();
    }

    Scene_Base.prototype.update.call(this);
}

// on exit, go to next scene
// default load into title
Scene_IntroVid.prototype.gotoTitleOrTest = function() {
    Scene_Base.prototype.start.call(this);
    SoundManager.preloadImportantSounds();
    if (DataManager.isBattleTest()) {
        DataManager.setupBattleTest();
        SceneManager.goto(Scene_Battle);
    } else if (DataManager.isEventTest()) {
        DataManager.setupEventTest();
        SceneManager.goto(Scene_Map);
    } else {
        this.checkPlayerLocation();
        DataManager.setupNewGame();
        SceneManager.goto(Scene_Title);
        Window_TitleCommand.initCommandPosition();
    }
    this.updateDocumentTitle();
};

Scene_IntroVid.prototype.updateDocumentTitle = function() {
    document.title = $dataSystem.gameTitle;
};

Scene_IntroVid.prototype.checkPlayerLocation = function() {
    if ($dataSystem.startMapId === 0) {
        throw new Error('Player\'s starting position is not set');
    }
};

// Should return true if done showing
Scene_IntroVid.prototype.updateLogo = function() {
    if (!this._isDonePlayingLogo) {
        if (!this._logoHasStarted) {
            this.showIntroLogo();
            this._logoHasStarted = true;
        }

        if (!Graphics.isVideoPlaying()) { // checks for done
            this._isDonePlayingLogo = true;
            console.log("Done showing logo");
        }
    }
    return this._isDonePlayingLogo;
};

// Should return true if done showing
Scene_IntroVid.prototype.updateIntroVid = function() {
    // copied off of DMY_SkipVideo.js 09-Apr-2020
    if (Graphics.isVideoPlaying() && (Input.isTriggered('menu')
                                        || Input.isTriggered('escape')
                                        || Input.isTriggered('cancel')
                                        || Input.isTriggered('ok')
                                        || TouchInput.isCancelled())) {
      if ('_video' in Graphics && 'pause' in Graphics._video
                              && typeof Graphics._video.pause === 'function') {
        Graphics._video.pause();
        Graphics._onVideoEnd();
      }
    }

    if (!this._isDonePlayingVid) {
        if (!this._vidHasStarted) { // starts the video
            this.showIntroVid();
            this._vidHasStarted = true;
        }
        if (!Graphics.isVideoPlaying()) { // checks for done
            this._isDonePlayingVid = true;
            console.log("Done showing vid");
        }
    }
    return this._isDonePlayingVid;
};

Scene_IntroVid.prototype.showIntroLogo = function() {
    // this.fadeScreen.show(0,0,0,85,85,255,0);
    // this.logoStartPic.show(0,0,0,85,85,255,0);
    // this.fadeScreen.show("BlackScreen",0,0,0,85,85,255,0);
    // this.logoStartPic.show("LogoStart",0,0,0,85,85,255,0);
    
    var fn = PluginManager.parameters("WheatFlour")["Intro Logo Name"];
    Graphics.playVideo("movies/" + fn + ".webm");
    this.startFadeIn(24, false); // idk why this thing not working
}

Scene_IntroVid.prototype.showIntroVid = function() {
    // var _bgm = {
    //     name: "Prologue",
    //     volume: AudioEffectManager.MASTER_VOLUME,
    //     pitch: 100,
    //     pan: 0
    // };
    // AudioManager.playBgm(_bgm);
    Graphics.playVideo("movies/" + this._vidname);
};

// Title Menu Repositioning
Window_TitleCommand.prototype.updatePlacement = function() {
    this.x = (Graphics.boxWidth - this.width) / 2 + 30;
    this.y = Graphics.boxHeight - this.height - 125;

    this.setBackgroundType(2);
};

Window_TitleCommand.prototype.windowWidth = function() {
    return 190;
};

/******************************************** */
// Credits

Scene_Gameover.prototype.initialize = function() {
    Scene_Base.prototype.initialize.call(this);
    this.credVidStarted = false;
};

Scene_Gameover.prototype.create = function() {
    Scene_Base.prototype.create.call(this);
    // this.playGameoverMusic();
    // this.createBackground();
};

Scene_Gameover.prototype.update = function() {
    // check if vid still playing
    var isPlaying = true; // credits unskippable
    if (this.credVidStarted) {
        if (!Graphics.isVideoPlaying()) {
            var isPlaying = false; 
        }
    }

    if (this.isActive() && !this.isBusy() && !isPlaying) {
        this.gotoTitle();
    }
    Scene_Base.prototype.update.call(this);
};

Scene_Gameover.prototype.start = function() {
    Scene_Base.prototype.start.call(this);
    this.startFadeIn(this.slowFadeSpeed(), false);
    // play video
    AudioManager.stopAll();
    Graphics.playVideo("movies/HepCat_Credits.webm");
    this.credVidStarted = true;
};