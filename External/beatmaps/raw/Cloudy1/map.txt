osu file format v14

[General]
AudioFilename: audio.mp3
AudioLeadIn: 0
PreviewTime: -1
Countdown: 0
SampleSet: Normal
StackLeniency: 0.7
Mode: 3
LetterboxInBreaks: 0
SpecialStyle: 0
WidescreenStoryboard: 0

[Editor]
DistanceSpacing: 1.2
BeatDivisor: 4
GridSize: 32
TimelineZoom: 0.9

[Metadata]
Title:Cloudy1
TitleUnicode:Cloudy1
Artist:Adriana Jacobs
ArtistUnicode:Adriana Jacobs
Creator:PourToi
Version:
Source:
Tags:
BeatmapID:0
BeatmapSetID:-1

[Difficulty]
HPDrainRate:5
CircleSize:4
OverallDifficulty:5
ApproachRate:5
SliderMultiplier:1.4
SliderTickRate:1

[Events]
//Background and Video events
//Break Periods
//Storyboard Layer 0 (Background)
//Storyboard Layer 1 (Fail)
//Storyboard Layer 2 (Pass)
//Storyboard Layer 3 (Foreground)
//Storyboard Layer 4 (Overlay)
//Storyboard Sound Samples

[TimingPoints]
0,666.666666666667,4,1,1,100,1,0


[HitObjects]
192,192,4666,5,0,0:0:0:0:
64,192,7333,1,0,0:0:0:0:
320,192,9666,1,0,0:0:0:0:
320,192,10000,1,2,0:0:0:0:
64,192,12666,1,0,0:0:0:0:
192,192,15000,1,0,0:0:0:0:
192,192,15333,1,2,0:0:0:0:
192,192,18000,128,0,18666:0:0:0:0:
320,192,20333,1,0,0:0:0:0:
64,192,20666,128,2,21333:0:0:0:0:
64,192,23333,1,0,0:0:0:0:
448,192,24666,1,2,0:0:0:0:
320,192,26000,1,0,0:0:0:0:
448,192,27333,1,2,0:0:0:0:
192,192,28666,128,0,29333:0:0:0:0:
192,192,29833,1,2,0:0:0:0:
64,192,31000,1,0,0:0:0:0:
320,192,31333,1,2,0:0:0:0:
192,192,32666,1,0,0:0:0:0:
64,192,33666,1,0,0:0:0:0:
320,192,34000,1,2,0:0:0:0:
192,192,35333,1,0,0:0:0:0:
192,192,36333,1,0,0:0:0:0:
192,192,36666,1,2,0:0:0:0:
320,192,38000,128,0,38666:0:0:0:0:
64,192,39000,1,2,0:0:0:0:
64,192,39333,1,2,0:0:0:0:
320,192,40000,1,0,0:0:0:0:
320,192,40333,1,0,0:0:0:0:
64,192,40666,128,2,41333:0:0:0:0:
448,192,42000,1,0,0:0:0:0:
448,192,44666,1,0,0:0:0:0:
64,192,47333,1,0,0:0:0:0:
64,192,50000,1,0,0:0:0:0:
64,192,50666,1,0,0:0:0:0:
64,192,51166,128,2,53500:0:0:0:0:
320,192,55333,1,0,0:0:0:0:
320,192,57666,1,0,0:0:0:0:
320,192,58000,1,0,0:0:0:0:
192,192,60666,1,2,0:0:0:0:
