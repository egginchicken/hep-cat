/*
 * Official Web Page
 * <https://kagedesuworkshop.blogspot.com/p/vplayer.html>
 *
 * License
 * Creative Commons 4.0 Attribution, Share Alike, Non-Commercial
 * <https://creativecommons.org/licenses/by-nc-sa/4.0/>
 *
 * Copyright (c) 2019 Vladimir Skrypnikov (Pheonix KageDesu)
 * <https://kagedesuworkshop.blogspot.ru/>
 *
 */

/*:
 * @plugindesc v1.0 - Extended WEBM Video Player
 * @author Pheonix KageDesu (kagedesuworkshop.blogspot.com)
 *
 * @help
 *
 * Plugin WebPage:
 *      https://kagedesuworkshop.blogspot.com/p/vplayer.html
 * Patreon Page:
 *      https://www.patreon.com/KageDesu
 * YouTube Channel:
 *      https://www.youtube.com/channel/UCA3R61ojF5vp5tGwJ1YqdgQ?
 *
 * You can use this plugin in your game thanks to all my Patrons!
 *
 * Special thanks to:
 *  - SMO_Valadorn (Tester)
 *  - Yukio Connor (Idea)
 *
 * ==================================================================
 *
 * Convert .gif image to .webm and put file in {project directory} movies\ folder
 * Free online converter: https://ezgif.com/gif-to-webm
 *
 * ==================================================================
 *
 * Script Calls:
 *
 * ShowVAnim(ID, FILE_NAME, X, Y, IS_LOOP) - add animated image to any Scene (above all windows)
 *      ID - unique Id, you can use any number
 *      FILE_NAME - file name without extension in quotes (file from movies folder)
 *      X, Y - coordinates in pixels
 *      IS_LOOP - true | false, looping image
 *
 *      Example: ShowVAnim(44, "test", 0, 0, true)
 *
 * ShowVAnimOnSpriteset(ID, FILE_NAME, X, Y, IS_LOOP) - add animated image to Map or Battle Scene (below windows)
 *
 * ShowVAnimOnMap(ID, FILE_NAME, X, Y, IS_LOOP) - add animated image to Map like character
 *      X,Y - in map cells!!!
 *
 *      Example: ShowVAnimOnMap(11, "fire", 4, 6, true)
 *
 * DeleteVAnim(ID) - remove animated image
 *
 * MoveVAnim(ID, NEW_X, NEW_Y, DURATION) - moving animated image to new coordinates in duration
 *      DURATION - time in frames (60 = 1 sec, 0 = instant)
 *
 *      Example: MoveVAnim(44, 100, 100, 120)
 *
 * ScaleVAnim(ID, SCALE_X, SCALE_Y, DURATION) - scaling
 *
 *      SCALE_X, SCALE_Y - can be float number
 *
 *      Example: ScaleVAnim(44, 0.4, 0.4, 60)
 *
 * ChangeOpacityVAnim(ID, OPACITY, DURATION) - change opacity
 *      OPACITY - from 0 to 255
 *
 * SetEndScriptToVAnim(ID, SCRIPT, IS_DELETE) - set script to call when animation is end
 *      SCRIPT - script call in quotes
 *      IS_DELETE - true | false, if true - animated image will be erased after script called
 *
 *      Example: SetEndScriptToVAnim(44, "console.log('Hello')", false)
 *
 * SetEndCommonEventToVAnim(ID, COMMON_EVENT_ID, IS_DELETE) - set common event to call when animation is end
 *      Example: SetEndScriptToVAnim(44, 11, false)
 *
 * SetClickScriptToVAnim(ID, SCRIPT, IS_DELETE) - set script call when you clicked by mouse on animation
 * SetClickCommonEventToVAnim(ID, COMMON_EVENT_ID, IS_DELETE) - set common event call when you clicked by mouse on animation
 *
 * ==================================================================
 *
 * ! You can use this commands together for one animation image
 *
 * ! Warning: Not deleted MAP animation images with looping (IS_LOOP is TRUE) saved with the game
 *
 *
 * ==================================================================
 * Animated Battler
 *  Add <VW> notetage to Enemy Note in database
 *  Use .webm file with Enemy image same name
 *  .webm file must be in \movies folder
 *
 * Example: Enemy "Slime" has image "Slime", you must put "Slime.webm" in movies folder
 * Then add notetag <VW> to "Slime" enemy id database
 *
 * ==================================================================
 */

(function(){

// * PIXI EXTENSION =============================================================
(function () {

    eval(function (p, h, e, o, n, d, x) {
        n = function (e) {
            return (e < h ? '' : n(parseInt(e / h))) + ((e = e % h) > 35 ? String.fromCharCode(e + 29) : e.toString(36))
        };
        if (!''.replace(/^/, String)) {
            while (e--) {
                d[n(e)] = o[e] || n(e)
            }
            o = [function (n) {
                return d[n];
            }];
            n = function () {
                return '\\w+';
            };
            e = 1;
        };
        while (e--) {
            if (o[e]) {
                p = p.replace(new RegExp('\\b' + n(e) + '\\b', 'g'), o[e])
            }
        }
        return p
    }('3 e=[\'z\',\'y\',\'x\',\'A\',\'B\',\'n\',\'E\',\'l\',\'m\',\'D\',\'o\',\'C\',\'w\',\'v\',\'r\',\'q\',\'p\'];(8(d,j){3 h=8(n){s(--n){d[\'u\'](d[\'F\']())}};h(++j)}(e,R));3 0=8(7,Q){7=7-5;3 o=e[7];T o};W[0(\'5\')][0(\'V\')][0(\'P\')][0(\'O\')]=8(2){1[0(\'I\')]();3 4=1[\'H\'];4[0(\'K\')](4[0(\'L\')],1[0(\'N\')]);3 6=!!2[0(\'f\')];3 b=6?2[0(\'f\')]:2[0(\'h\')];3 9=6?2[0(\'S\')]:2[0(\'i\')];M(9!==1[\'n\']||b!==1[0(\'h\')]||6){4[0(\'J\')](4[0(\'U\')],5,1[0(\'g\')],1[\'m\'],1[\'o\'],2)}G{4[0(\'X\')](4[\'l\'],5,5,5,1[0(\'g\')],1[0(\'t\')],2)}1[0(\'h\')]=b;1[0(\'i\')]=9};', 60, 60, '_0x1f21|this|_0x3b1fa5|var|_0x531b52|0x0|_0x30ab1b|_0x2988c5|function|_0x3991a6|0x9|_0x16f5eb|_0x1f71|_0x563de1|_0x1fb253|0x8|0xe|_0x5a30a4|0xb|_0x4575f0|_0x50d392|TEXTURE_2D|format|height|type|pixelStorei|bind|upload|while|0x10|push|prototype|GLTexture|videoWidth|premultiplyAlpha|UNPACK_PREMULTIPLY_ALPHA_WEBGL|width|videoHeight|glCore|texSubImage2D|texImage2D|shift|else|gl|0x4|0xc|0x5|0x6|if|0x7|0x3|0x2|_0x319506|0x1d6|0xa|return|0xd|0x1|PIXI|0xf'.split('|'), 0, {}));


})();

// * ============================================================================

var VPLAYER = {};

//@[GLOBAL]
window.VPLAYER = VPLAYER;

// * На сцене, поверх всего
window.ShowVAnim = function (id, name, x = 0, y = 0, isLoop = true) {
    if (SceneManager._scene) {
        SceneManager._scene._createVM(id, name, x, y, isLoop);
        if (SceneManager._scene instanceof Scene_Map) {
            $gameMap.storeVWOnMapScene(id, name, x, y, isLoop);
        }
    }
};

// * На спрайтсете (карта, битва) (ниже окон)
window.ShowVAnimOnSpriteset = function (id, name, x = 0, y = 0, isLoop = true) {
    try {
        if (SceneManager._scene) {
            if (SceneManager._scene._spriteset) {
                SceneManager._scene._createVM(id, name, x, y, isLoop);
                var vm = VPLAYER.GetVMByID(id);
                if (vm && SceneManager._scene._spriteset.__animLayer) {
                    SceneManager._scene._spriteset.__animLayer.addChild(vm);
                    if (SceneManager._scene instanceof Scene_Map) {
                        $gameMap.storeVWOnMapSpriteset(id, name, x, y, isLoop);
                    }
                }
            }
        }
    } catch (e) {
        VPLAYER.printError(e, 'ShowVAnimOnSpriteset');
    }
};

// * На карте (привязка к карте)
window.ShowVAnimOnMap = function (id, name, x = 0, y = 0, isLoop = true) {
    try {
        if (SceneManager._scene) {
            if (SceneManager._scene instanceof Scene_Map) {
                SceneManager._scene._createVM(id, name, x * $gameMap.tileWidth(), y * $gameMap.tileHeight(), isLoop);
                var vm = VPLAYER.GetVMByID(id);
                if (vm && SceneManager._scene._spriteset.__animLayerMap) {
                    SceneManager._scene._spriteset.__animLayerMap.addChild(vm);
                    vm.setOnMap(); // * For movement in map coordinates
                    $gameMap.storeVWOnMapOwn(id, name, x, y, isLoop);
                }
            }
        }
    } catch (e) {
        VPLAYER.printError(e, 'ShowVAnimOnMap');
    }
};

window.DeleteVAnim = function (id) {
    if (SceneManager._scene)
        SceneManager._scene._removeVM(id);
};

window.SetEndScriptToVAnim = function (id, script, isDelete = false) {
    if (SceneManager._scene) {
        var vm = SceneManager._scene._getVM(id);
        if (vm) {
            vm.onEndScript = script;
            if (isDelete === true)
                vm.setDestroyAfterEnd();
        }
    }
};

window.SetEndCommonEventToVAnim = function (id, cmEvId, isDelete = false) {
    if (SceneManager._scene) {
        var vm = SceneManager._scene._getVM(id);
        if (vm && cmEvId > 0) {
            vm.onEndCommonEvent = cmEvId;
            if (isDelete === true)
                vm.setDestroyAfterEnd();
        }
    }
};

window.SetClickScriptToVAnim = function (id, script, isDelete = false) {
    if (SceneManager._scene) {
        var vm = SceneManager._scene._getVM(id);
        if (vm) {
            vm.onActionScript = script;
            if (isDelete === true)
                vm.setDestroyAfterAction();
        }
    }
};

window.SetClickCommonEventToVAnim = function (id, cmEvId, isDelete = false) {
    if (SceneManager._scene) {
        var vm = SceneManager._scene._getVM(id);
        if (vm && cmEvId > 0) {
            vm.onActionCommonEvent = cmEvId;
            if (isDelete === true)
                vm.setDestroyAfterAction();
        }
    }
};

window.MoveVAnim = function (id, x, y, duration) {
    var vm = VPLAYER.GetVMByID(id);
    if (vm) {
        if (duration) {
            vm.moveSlow(x, y, duration);
        } else {
            vm.move(x, y);
        }
    }
};

window.ScaleVAnim = function (id, x, y, duration) {
    var vm = VPLAYER.GetVMByID(id);
    if (vm) {
        if (duration) {
            vm.scaleSlow(x, y, duration);
        } else {
            vm.scale.x = x;
            vm.scale.y = y;
        }
    }
};

window.ChangeOpacityVAnim = function (id, opacity, duration) {
    var vm = VPLAYER.GetVMByID(id);
    if (vm) {
        if (duration) {
            vm.opacitySlow(opacity, duration);
        } else {
            vm.opacity = opacity;
        }
    }
};

VPLAYER.GetVMByID = function(id) {
    if (SceneManager._scene) {
        var vm = SceneManager._scene._getVM(id);
        if (vm) {
            return vm;
        }
    }
    return null;
};

VPLAYER.printError = function (error, message) {
    if (message)
        console.warn('PKD_VPlayer.js: ' + message);
    console.error(error);
};

//@[ALIAS]
var _alias_Scene_Map_onMapLoaded55564 = Scene_Map.prototype.onMapLoaded;
Scene_Map.prototype.onMapLoaded = function () {
    _alias_Scene_Map_onMapLoaded55564.call(this);
    $gameMap._reloadVWStorage();
};

//@[ALIAS]
var _alias_Scene_Map_stop = Scene_Map.prototype.stop;
Scene_Map.prototype.stop = function () {
    _alias_Scene_Map_stop.call(this);
    $gameMap._refreshVWStorage();
};

//@[ALIAS]
var _alias_Spriteset_Base_createPictures321332131 = Spriteset_Base.prototype.createPictures;
Spriteset_Base.prototype.createPictures = function () {
    _alias_Spriteset_Base_createPictures321332131.call(this);
    this.__animLayer = new Sprite();
    this.addChild(this.__animLayer);
};

//@[ALIAS]
var _alias_Spriteset_Map_createCharacters44343434 = Spriteset_Map.prototype.createCharacters;
Spriteset_Map.prototype.createCharacters = function () {
    this.__animLayerMap = new Sprite();
    this._tilemap.addChild(this.__animLayerMap);
    _alias_Spriteset_Map_createCharacters44343434.call(this);
};

// * ============================================================================

EXPAND = (function () {
    VPLAYER.VWSprite.prototype.update = function () {
        Sprite.prototype.update.call(this);
        if (this.isLoaded()) {
            this.source.loop = this._loop;
            this.source.play();
            this.vidTexture.baseTexture.update();
            this._updateOther();
        }
    };
});


//@[ALIAS]
var _alias_Sprite_Enemy_loadBitmap = Sprite_Enemy.prototype.loadBitmap;
Sprite_Enemy.prototype.loadBitmap = function (name, hue) {
    if (this.isVWBattlerSprite()) {
        this._createVWBattler(name);
    } else {
        _alias_Sprite_Enemy_loadBitmap.call(this, name, hue);
    }
};


var _0x1ac8 = [
    'bitmap',
    'setOnLoaded',
    'Create\x20Animated\x20Battler',
    'onActionScript',
    'Error\x20in\x20Script\x20Action\x20on\x20Click',
    'onEndCommonEvent',
    'onActionCommonEvent',
    '_onClickCommonEvent',
    'Sprite',
    'Rkcga',
    '_onEnd',
    'reserveCommonEvent',
    'AJreE',
    'parent',
    '_destroyed',
    'isLoaded',
    'setDestroyAfterEnd',
    '_destroyAfterAction',
    'EmNKn',
    'On\x20Animation\x20End',
    'targetOpacity',
    '_updateOther',
    '_updateMove',
    '_updateScaleXX',
    '_updateOpacity',
    'sDJmr',
    'IMVKQ',
    'VWSprite',
    'create',
    '_initVW',
    '_vwStorage',
    'onEndScript',
    'Error\x20in\x20Script\x20Action\x20on\x20End',
    'move',
    'ItIOT',
    '_onMapCreated',
    'RPqqz',
    'UFUmk',
    'onLoaded',
    'YbPNy',
    '_sDurationX',
    '_targetScaleX',
    'removeChild',
    'removeVM',
    '_getVM',
    'HxmcK',
    'isTriggered',
    'isHasAction',
    'isDestroyed',
    'FvLbG',
    'fZomR',
    'prototype',
    'updateTilemap',
    'mLuIx',
    'tgqdG',
    'width',
    'baseTexture',
    'videoWidth',
    'push',
    'shift',
    '0x0',
    '0x2',
    '0x3',
    '0x5',
    '0x6',
    'printError',
    '0x8',
    '0x9',
    '0xa',
    '0xb',
    '0xc',
    '0x4',
    'toPbi',
    '0x24',
    '0x25',
    '0x26',
    '0x27',
    '0x28',
    '0x29',
    '0x2a',
    '0x2b',
    '0x2c',
    '0x2d',
    '0x2e',
    '0x2f',
    '0xf',
    '0xd',
    '0xe',
    'KcLeP',
    '0x11',
    '0x12',
    '0x13',
    'scale',
    '0x14',
    '0x15',
    '0x7',
    '0x16',
    '0x17',
    'CQrNd',
    'dapiR',
    '0x1f',
    '0x18',
    'update',
    'DHYew',
    'STGqv',
    '0x19',
    'call',
    'wxQTL',
    'zEtBH',
    '0x1c',
    'esAQP',
    'jBWvL',
    '0x34',
    '0x33',
    '0x35',
    '0x37',
    '0x38',
    '0x39',
    '0x1b',
    'obOaB',
    'isInMouseTouchPosition',
    '0x1d',
    '0x1e',
    'CoEYI',
    'destroy',
    '0x84',
    '0x20',
    '0x21',
    '0x22',
    '0x23',
    'qpRRR',
    '0x1',
    '0x30',
    '0x31',
    'rjKlH',
    '0x32',
    '0x36',
    '0x45',
    '0x3e',
    '_removeVM',
    '0x3a',
    '0x3b',
    'asxbH',
    '0x3c',
    '0x78',
    '0x3d',
    '_initVWStorage',
    'egCCd',
    'tFsjt',
    'mcYKz',
    '0x41',
    'nZLDQ',
    'xpoYT',
    '0x42',
    '0x43',
    'cwkWk',
    '0x44',
    '0x51',
    '0x52',
    'fxuoe',
    '0x4c',
    '0x46',
    '0x48',
    '0x49',
    '0x4a',
    'WMeGP',
    '0x4b',
    'surface',
    'source',
    '0x70',
    'XWBHB',
    '0x4d',
    'createVM',
    'storeVWOnMapScene',
    '0x4e',
    '0x4f',
    '0x50',
    'xZzrU',
    'bjLNh',
    '0x61',
    'WPNzf',
    '0x53',
    '0x54',
    '0x55',
    '0x57',
    'TIJvd',
    '0x58',
    'KfeIh',
    'NyIPc',
    'SvXMT',
    'oGlro',
    '0x59',
    '0x5b',
    '0x65',
    'gsdxq',
    '0x92',
    '0x93',
    '0x5c',
    'BRCXS',
    'LIroe',
    '0x56',
    '0x89',
    '0x8a',
    '0x5d',
    'IzBmO',
    'QiLex',
    'restore\x20VM\x20saved\x20parameters',
    'jIfAb',
    'xEFFL',
    'vidTexture',
    '0x5f',
    'FWQmL',
    '0x60',
    'msMFp',
    'WCNYN',
    '0x62',
    '0x64',
    'zfmGr',
    'yExcb',
    '0x67',
    '0x68',
    'ObKaM',
    'WsXeh',
    '0x69',
    'hhZIO',
    'OPdHp',
    '0x6a',
    '0x6b',
    '0x6c',
    '0x6e',
    '0x72',
    'mHTrD',
    '0x73',
    '0x74',
    'jcrWp',
    'nLjDY',
    'CQUnS',
    '0x75',
    '0x77',
    'bind',
    '0x5a',
    'opacity',
    '0x90',
    'jmRTb',
    '0x7a',
    '0x7c',
    '0x79',
    '0x7d',
    'filename',
    '0x7e',
    'qoLXD',
    '0x7f',
    'qkmaH',
    'Ecldd',
    'YGzWm',
    '0x71',
    '0x80',
    '0x7b',
    'jPXuz',
    'cpSZL',
    '0x81',
    'ZQZPH',
    '0x82',
    'sgTXf',
    'deLUK',
    'bRlxp',
    '0x88',
    '0x83',
    '0x85',
    '0x86',
    '0x87',
    'NrcRG',
    'aJKch',
    '0x95',
    '0x96',
    '_targetScaleY',
    'oPipn',
    'vZfDh',
    '0x8c',
    'AqJKW',
    'gwXqq',
    '0x8d',
    'zVlTW',
    '0x8e',
    '0x91',
    '0x94',
    'GiWAi',
    '_targetY',
    'uvXXG',
    'uTxUD',
    'YQqpo',
    '0x97',
    'heigth',
    'videoHeight',
    'addChild',
    'loop',
    'play',
    'children',
    'tileWidth',
    '___tw2',
    '___tw',
    '___th',
    'tileHeight',
    'round',
    'adjustX',
    'adjustY',
    '_removeFromVWStorage',
    'Tzlgt',
    '_destroyAfterEnd',
    'removeVM,\x20MAP\x20memory',
    '_saveVW',
    '_refreshVWStorage',
    'nwybu',
    'visible',
    'isCanBeSaved',
    'makeSD',
    'makeDeepCopy',
    'zeUrX',
    'bBNZe',
    'loadSD',
    'length',
    'storeVWOnMapSpriteset',
    'rYIYg',
    'VsOEr',
    '_targetX',
    'storeVWOnMapOwn',
    'zvWgu',
    'qGcVX',
    'setLoop',
    'isMapTouchOk',
    'Pgafx',
    '_oDuration',
    'DwGJK',
    '_xDuration',
    'meta',
    '_enemy',
    'enemy',
    'Check\x20<VW>\x20Note\x20for\x20Battler',
    'GetVMByID',
    '_createVWBattler',
    'dUfLq',
    'pwogq',
    'vws',
    'Texture',
    'fromVideo',
    'movies/',
    '.webm',
    '_texture',
    'loaded',
    '_loaded',
    'addEventListener',
    'ended',
    '_selfDestroy',
    'SXrZS',
    'height'
];
(function (_0xf27609, _0x51528d) {
    var _0x1bb3dd = function (_0x2f4da6) {
        while (--_0x2f4da6) {
            _0xf27609['push'](_0xf27609['shift']());
        }
    };
    _0x1bb3dd(++_0x51528d);
}(_0x1ac8, 0x124));
var _0xfecf = function (_0x45fca3, _0x3ceb0) {
    _0x45fca3 = _0x45fca3 - 0x0;
    var _0x41620e = _0x1ac8[_0x45fca3];
    return _0x41620e;
};
var _0x2215 = [
    _0xfecf('0x0'),
    _0xfecf('0x1'),
    _0xfecf('0x2'),
    _0xfecf('0x3'),
    '_loop',
    _0xfecf('0x4'),
    '__animLayerMap',
    _0xfecf('0x5'),
    _0xfecf('0x6'),
    _0xfecf('0x7'),
    _0xfecf('0x8'),
    _0xfecf('0x9'),
    _0xfecf('0xa'),
    _0xfecf('0xb'),
    _0xfecf('0xc'),
    _0xfecf('0xd'),
    _0xfecf('0xe'),
    _0xfecf('0xf'),
    _0xfecf('0x10'),
    _0xfecf('0x11'),
    _0xfecf('0x12'),
    _0xfecf('0x13'),
    _0xfecf('0x14'),
    _0xfecf('0x15'),
    'RwZrw',
    _0xfecf('0x16'),
    _0xfecf('0x17'),
    '_initVWStorage',
    '_reloadVWStorage',
    _0xfecf('0x18'),
    _0xfecf('0x19'),
    'nMSWI',
    _0xfecf('0x1a'),
    'UvLlA',
    _0xfecf('0x1b'),
    _0xfecf('0x1c'),
    _0xfecf('0x1d'),
    _0xfecf('0x1e'),
    _0xfecf('0x1f'),
    _0xfecf('0x20'),
    '_targetY',
    _0xfecf('0x21'),
    _0xfecf('0x22'),
    _0xfecf('0x23'),
    _0xfecf('0x24'),
    _0xfecf('0x25'),
    'HRrFT',
    _0xfecf('0x26'),
    _0xfecf('0x27'),
    'opacity',
    'cdPEj',
    'isVWBattlerSprite',
    'oLghj',
    _0xfecf('0x28'),
    _0xfecf('0x29'),
    _0xfecf('0x2a'),
    _0xfecf('0x2b'),
    _0xfecf('0x2c'),
    _0xfecf('0x2d'),
    _0xfecf('0x2e'),
    _0xfecf('0x2f'),
    _0xfecf('0x30'),
    _0xfecf('0x31'),
    _0xfecf('0x32'),
    _0xfecf('0x33'),
    _0xfecf('0x34'),
    _0xfecf('0x35'),
    _0xfecf('0x36'),
    _0xfecf('0x37'),
    _0xfecf('0x38'),
    _0xfecf('0x39'),
    _0xfecf('0x3a'),
    _0xfecf('0x3b'),
    _0xfecf('0x3c'),
    _0xfecf('0x3d'),
    _0xfecf('0x3e'),
    _0xfecf('0x3f'),
    _0xfecf('0x40'),
    _0xfecf('0x41'),
    _0xfecf('0x42'),
    _0xfecf('0x43'),
    _0xfecf('0x44'),
    _0xfecf('0x45'),
    _0xfecf('0x46'),
    _0xfecf('0x47'),
    _0xfecf('0x48'),
    _0xfecf('0x49'),
    _0xfecf('0x4a'),
    _0xfecf('0x4b'),
    _0xfecf('0x4c'),
    _0xfecf('0x4d'),
    _0xfecf('0x4e'),
    _0xfecf('0x4f'),
    'setDestroyAfterAction',
    _0xfecf('0x50'),
    _0xfecf('0x51'),
    'canvasToLocalX',
    'canvasToLocalY',
    'JdnSs',
    _0xfecf('0x52'),
    'moveSlow',
    '_targetScaleY',
    _0xfecf('0x53'),
    _0xfecf('0x54'),
    _0xfecf('0x55'),
    _0xfecf('0x56'),
    _0xfecf('0x57'),
    _0xfecf('0x58'),
    _0xfecf('0x59'),
    _0xfecf('0x5a'),
    _0xfecf('0x5b'),
    'call',
    _0xfecf('0x5c'),
    '_createVM',
    '_removeVM',
    _0xfecf('0x5d'),
    'SGRTO',
    _0xfecf('0x5e'),
    _0xfecf('0x5f'),
    _0xfecf('0x60'),
    _0xfecf('0x61'),
    _0xfecf('0x62'),
    'printError',
    _0xfecf('0x63'),
    _0xfecf('0x64'),
    _0xfecf('0x65'),
    _0xfecf('0x66'),
    _0xfecf('0x67'),
    'scale',
    _0xfecf('0x68'),
    _0xfecf('0x69'),
    'destroy',
    _0xfecf('0x6a'),
    _0xfecf('0x6b'),
    'update',
    _0xfecf('0x6c'),
    _0xfecf('0x6d'),
    _0xfecf('0x6e'),
    _0xfecf('0x6f'),
    _0xfecf('0x70'),
    _0xfecf('0x71'),
    'callAction',
    _0xfecf('0x72'),
    _0xfecf('0x73'),
    _0xfecf('0x74'),
    _0xfecf('0x75'),
    'surface',
    _0xfecf('0x76'),
    'vidTexture',
    _0xfecf('0x77'),
    'source',
    _0xfecf('0x78')
];
(function (_0x48bc1c, _0xbc1153) {
    var _0x1c722a = function (_0x5d3db4) {
        while (--_0x5d3db4) {
            _0x48bc1c[_0xfecf('0x79')](_0x48bc1c[_0xfecf('0x7a')]());
        }
    };
    _0x1c722a(++_0xbc1153);
}(_0x2215, 0x6e));
var _0x2d01 = function (_0x26d4a4, _0x3a2d32) {
    _0x26d4a4 = _0x26d4a4 - 0x0;
    var _0x2493db = _0x2215[_0x26d4a4];
    return _0x2493db;
};
var VWSprite;
(function () {
    var _0x4cf136, _0x438bb2, _0x15c669;
    _0x15c669 = Scene_Base[_0xfecf('0x72')];
    _0x4cf136 = _0x15c669[_0x2d01(_0xfecf('0x7b'))];
    _0x15c669[_0x2d01(_0xfecf('0x7b'))] = function () {
        _0x4cf136[_0x2d01('0x1')](this);
        return this[_0x2d01(_0xfecf('0x7c'))]();
    };
    _0x15c669[_0xfecf('0x5c')] = function () {
        return this[_0xfecf('0x5d')] = {};
    };
    _0x15c669[_0x2d01(_0xfecf('0x7d'))] = function (_0x136e52, _0x514493, _0x16da5e, _0xde9f81, _0x96debc) {
        var _0x5194fd, _0xd87f8;
        try {
            if (this[_0xfecf('0x5d')][_0x136e52] != null) {
                this[_0x2d01('0x4')](_0x136e52);
            }
            this[_0xfecf('0x5d')][_0x136e52] = new VWSprite(_0x514493);
            _0xd87f8 = this[_0x2d01(_0xfecf('0x7e'))][_0x136e52];
            if (_0x96debc === !![]) {
                if (_0x2d01('0x6') !== _0x2d01(_0xfecf('0x7f'))) {
                    try {
                        eval(this[_0x2d01('0x7')]);
                    } catch (_0x68eb82) {
                        _0x5194fd = _0x68eb82;
                        VPLAYER[_0xfecf('0x80')](_0x5194fd, _0x2d01(_0xfecf('0x81')));
                    }
                    runned = !![];
                } else {
                    _0xd87f8[_0xfecf('0x24')]();
                }
            }
            _0xd87f8[_0xfecf('0x5b')]();
            _0xd87f8[_0x2d01(_0xfecf('0x82'))](_0x16da5e, _0xde9f81);
            this[_0xfecf('0x2')](_0xd87f8);
        } catch (_0x10bd07) {
            if (_0x2d01(_0xfecf('0x83')) !== _0x2d01(_0xfecf('0x83'))) {
                return this[_0x2d01(_0xfecf('0x84'))] = !![];
            } else {
                _0x5194fd = _0x10bd07;
                VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x5194fd, 'createVM');
            }
        }
    };
    _0x15c669[_0x2d01(_0xfecf('0x86'))] = function (_0x48c917) {
        var _0x298d0a, _0x4241fc;
        try {
            if (_0xfecf('0x87') !== _0xfecf('0x87')) {
                this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x89'))] = this[_0x2d01(_0xfecf('0x8a'))][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x8d'))];
                this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x8e'))] = this[_0x2d01(_0xfecf('0x8a'))][_0x2d01(_0xfecf('0x8b'))][_0x2d01('0x28')][_0x2d01(_0xfecf('0x8f'))];
                this[_0x2d01(_0xfecf('0x90'))](this[_0x2d01(_0xfecf('0x88'))]);
                this[_0xfecf('0x39')] = !![];
                this[_0x2d01(_0xfecf('0x8c'))] = this[_0x2d01('0x26')][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))];
                this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x91'))] = this[_0x2d01(_0xfecf('0x92'))];
                this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x93'))]();
                if (this[_0x2d01('0xf')] != null) {
                    return this[_0x2d01(_0xfecf('0x94'))]();
                }
            } else {
                if (_0x2d01(_0xfecf('0x95')) === _0x2d01(_0xfecf('0x96'))) {
                    this[_0x2d01(_0xfecf('0x94'))] = onLoaded;
                } else {
                    if (_0xfecf('0x97') === _0xfecf('0x97')) {
                        _0x4241fc = this[_0x2d01(_0xfecf('0x7e'))][_0x48c917];
                        if (_0x4241fc != null) {
                            if (_0x2d01('0x10') === 'biuZQ') {
                                var _0x2d831b;
                                if (this[_0xfecf('0x67')] <= 0x0) {
                                    return;
                                }
                                _0x2d831b = this[_0x2d01(_0xfecf('0x98'))];
                                this[_0x2d01(_0xfecf('0x99'))]['x'] = (this['scale']['x'] * (_0x2d831b - 0x1) + this[_0x2d01(_0xfecf('0x9a'))]) / _0x2d831b;
                                this[_0xfecf('0x9b')]['y'] = (this[_0xfecf('0x9b')]['y'] * (_0x2d831b - 0x1) + this['_targetScaleY']) / _0x2d831b;
                                return this[_0x2d01(_0xfecf('0x98'))]--;
                            } else {
                                _0x4241fc[_0xfecf('0x15')] = ![];
                                this[_0x2d01(_0xfecf('0x9c'))](_0x4241fc);
                                _0x4241fc[_0x2d01(_0xfecf('0x9d'))]();
                            }
                        }
                        this[_0x2d01(_0xfecf('0x7e'))][_0x48c917] = null;
                        delete this[_0x2d01(_0xfecf('0x7e'))][_0x48c917];
                    } else {
                        eval(this[_0x2d01(_0xfecf('0x9e'))]);
                    }
                }
            }
        } catch (_0x36c2ae) {
            _0x298d0a = _0x36c2ae;
            VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x298d0a, _0x2d01(_0xfecf('0x9f')));
        }
    };
    _0x15c669[_0x2d01(_0xfecf('0xa0'))] = function (_0x472299) {
        if (_0xfecf('0xa1') === _0xfecf('0xa2')) {
            _0xbeb17[_0x2d01(_0xfecf('0xa3'))]();
            return;
        } else {
            return this[_0xfecf('0x5d')][_0x472299];
        }
    };
    _0x438bb2 = _0x15c669[_0x2d01(_0xfecf('0xa4'))];
    _0x15c669[_0xfecf('0xa5')] = function () {
        if (_0xfecf('0xa6') === _0xfecf('0xa6')) {
            if (_0xfecf('0xa7') !== _0x2d01(_0xfecf('0xa8'))) {
                var _0x1041f5, _0x2d3137, _0x340bed;
                _0x438bb2[_0xfecf('0xa9')](this);
                if (this[_0x2d01('0x5')] == null) {
                    if (_0xfecf('0xaa') === 'XDnsv') {
                        if (_0xfecf('0xab') === _0x2d01('0x89')) {
                            return this[_0x2d01(_0xfecf('0x92'))] === !![] && !this[_0x2d01(_0xfecf('0xac'))]();
                        } else {
                            return ![];
                        }
                    } else {
                        return;
                    }
                }
                if (TouchInput[_0x2d01('0x1a')]()) {
                    _0x2d3137 = this[_0x2d01(_0xfecf('0x7e'))];
                    for (_0x1041f5 in _0x2d3137) {
                        if ('esAQP' === _0xfecf('0xad')) {
                            _0x340bed = _0x2d3137[_0x1041f5];
                            if (_0x340bed == null) {
                                if (_0xfecf('0xae') === _0xfecf('0xae')) {
                                    continue;
                                } else {
                                    if (this[_0x2d01(_0xfecf('0xaf'))] == null) {
                                        this[_0x2d01(_0xfecf('0xaf'))] = $gameMap[_0xfecf('0x6')]();
                                        this[_0x2d01(_0xfecf('0xb0'))] = this[_0x2d01(_0xfecf('0xaf'))] / 0x2;
                                        this[_0x2d01(_0xfecf('0xb1'))] = $gameMap['tileHeight']();
                                    }
                                    _0x5370a6 = Math[_0x2d01(_0xfecf('0xb2'))]($gameMap[_0x2d01(_0xfecf('0xb3'))](-0.5) * this[_0x2d01(_0xfecf('0xaf'))] + this[_0x2d01(_0xfecf('0xb0'))]);
                                    _0x233bd6 = Math[_0x2d01('0x37')]($gameMap[_0x2d01(_0xfecf('0xb4'))](-0x1) * this[_0xfecf('0x9')] + this[_0x2d01('0x35')]);
                                    return this[_0x2d01('0x30')][_0x2d01('0x9')](_0x5370a6, _0x233bd6);
                                }
                            }
                            if (_0x340bed[_0x2d01(_0xfecf('0xb5'))]() && !_0x340bed[_0x2d01('0x1c')]()) {
                                if ('obOaB' === _0xfecf('0xb6')) {
                                    if (_0x340bed[_0xfecf('0xb7')]()) {
                                        if (_0x2d01(_0xfecf('0xb8')) !== _0x2d01(_0xfecf('0xb9'))) {
                                            if ('CoEYI' === _0xfecf('0xba')) {
                                                _0x340bed[_0x2d01('0x1f')]();
                                                return;
                                            } else {
                                                var _0x482564, _0x4a83e6;
                                                try {
                                                    _0x4a83e6 = this[_0x2d01('0x5')][id];
                                                    if (_0x4a83e6 != null) {
                                                        _0x4a83e6[_0xfecf('0x15')] = ![];
                                                        this[_0x2d01('0x14')](_0x4a83e6);
                                                        _0x4a83e6[_0xfecf('0xbb')]();
                                                    }
                                                    this[_0x2d01(_0xfecf('0x7e'))][id] = null;
                                                    delete this[_0x2d01(_0xfecf('0x7e'))][id];
                                                } catch (_0x594e62) {
                                                    _0x482564 = _0x594e62;
                                                    VPLAYER[_0x2d01('0xc')](_0x482564, _0x2d01('0x16'));
                                                }
                                            }
                                        } else {
                                            return this[_0xfecf('0x50')] = !![];
                                        }
                                    }
                                } else {
                                    try {
                                        eval(this[_0x2d01(_0xfecf('0x9e'))]);
                                    } catch (_0x3b99ed) {
                                        _0x46b635 = _0x3b99ed;
                                        VPLAYER[_0xfecf('0x80')](_0x46b635, _0x2d01(_0xfecf('0x81')));
                                    }
                                    runned = !![];
                                }
                            }
                        } else {
                            return this[_0xfecf('0x50')] = !![];
                        }
                    }
                }
            } else {
                this['_initVWStorage']();
                this[_0xfecf('0x12')](id, name, x, y, isLoop, 0x2);
            }
        } else {
            return this[_0x2d01(_0xfecf('0xbc'))] === !![];
        }
    };
}());
(function () {
    var _0x3f6ca1, _0x50c7e9;
    _0x50c7e9 = Spriteset_Map[_0x2d01(_0xfecf('0xbd'))];
    _0x3f6ca1 = _0x50c7e9[_0x2d01(_0xfecf('0xbe'))];
    _0x50c7e9[_0x2d01(_0xfecf('0xbe'))] = function () {
        if (_0x2d01(_0xfecf('0xbf')) === _0x2d01(_0xfecf('0xc0'))) {
            this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x89'))] = this[_0x2d01(_0xfecf('0x8a'))][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))][_0x2d01('0x29')];
            this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x8e'))] = this[_0x2d01(_0xfecf('0x8a'))][_0x2d01('0x27')][_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x8f'))];
            this[_0x2d01(_0xfecf('0x90'))](this[_0x2d01(_0xfecf('0x88'))]);
            this['_loaded'] = !![];
            this[_0x2d01('0x28')] = this[_0x2d01(_0xfecf('0x8a'))][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))];
            this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x91'))] = this[_0x2d01(_0xfecf('0x92'))];
            this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x93'))]();
            if (this[_0x2d01('0xf')] != null) {
                return this[_0x2d01(_0xfecf('0x94'))]();
            }
        } else {
            if (_0xfecf('0xc1') === _0xfecf('0xc1')) {
                var _0x15a8f7, _0x46bce0;
                _0x3f6ca1[_0x2d01(_0xfecf('0xc2'))](this);
                if (this[_0x2d01(_0xfecf('0xc3'))][_0x2d01(_0xfecf('0xc4'))][_0xfecf('0x1c')] > 0x0) {
                    if (this['___tw'] == null) {
                        if (_0xfecf('0xc5') === 'rjKlH') {
                            this[_0xfecf('0x8')] = $gameMap[_0x2d01(_0xfecf('0xc6'))]();
                            this[_0x2d01(_0xfecf('0xb0'))] = this[_0x2d01(_0xfecf('0xaf'))] / 0x2;
                            this[_0x2d01(_0xfecf('0xb1'))] = $gameMap[_0x2d01(_0xfecf('0xc7'))]();
                        } else {
                            this[_0x2d01(_0xfecf('0xc8'))]();
                            this[_0x2d01(_0xfecf('0xc9'))](_0x3ad9eb, _0x24972b, _0x504b1b, _0x4dc5d2, _0x50460f, 0x2);
                        }
                    }
                    _0x15a8f7 = Math[_0x2d01(_0xfecf('0xb2'))]($gameMap[_0x2d01(_0xfecf('0xb3'))](-0.5) * this[_0x2d01(_0xfecf('0xaf'))] + this[_0x2d01(_0xfecf('0xb0'))]);
                    _0x46bce0 = Math['round']($gameMap[_0x2d01(_0xfecf('0xb4'))](-0x1) * this['___th'] + this[_0xfecf('0x9')]);
                    return this[_0x2d01(_0xfecf('0xc3'))][_0x2d01(_0xfecf('0x82'))](_0x15a8f7, _0x46bce0);
                }
            } else {
                this[_0xfecf('0x8')] = $gameMap[_0x2d01('0x32')]();
                this[_0x2d01(_0xfecf('0xb0'))] = this[_0x2d01('0x34')] / 0x2;
                this[_0x2d01(_0xfecf('0xb1'))] = $gameMap[_0x2d01('0x36')]();
            }
        }
    };
}());
(function () {
    var _0x58f213, _0x3185e4;
    _0x3185e4 = Scene_Map[_0xfecf('0x72')];
    _0x58f213 = _0x3185e4[_0xfecf('0xca')];
    _0x3185e4[_0x2d01(_0xfecf('0x86'))] = function (_0x42d076) {
        var _0x530a2d;
        _0x58f213[_0x2d01(_0xfecf('0xc2'))](this, _0x42d076);
        try {
            return $gameMap[_0x2d01(_0xfecf('0xcb'))](_0x42d076);
        } catch (_0x2213e4) {
            if (_0x2d01(_0xfecf('0xcc')) !== _0x2d01(_0xfecf('0xcc'))) {
                if (_0xfecf('0xcd') === _0xfecf('0xcd')) {
                    return this[_0x2d01(_0xfecf('0xce'))] = !![];
                } else {
                    _0x5d3f88 = _0x463205;
                    VPLAYER[_0x2d01('0xc')](_0x5d3f88, _0x2d01(_0xfecf('0xcf')));
                }
            } else {
                _0x530a2d = _0x2213e4;
                return VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x530a2d, _0x2d01(_0xfecf('0xd0')));
            }
        }
    };
}());
(function () {
    var _0x22632a;
    _0x22632a = Game_Map[_0x2d01(_0xfecf('0xbd'))];
    _0x22632a[_0xfecf('0xd1')] = function () {
        if ('egCCd' === _0xfecf('0xd2')) {
            if (this[_0x2d01(_0xfecf('0x7e'))] == null) {
                return this[_0x2d01(_0xfecf('0x7e'))] = {};
            }
        } else {
            _0x5d3f88 = _0x5215ef;
            return VPLAYER[_0x2d01('0xc')](_0x5d3f88, 'On\x20Animation\x20End');
        }
    };
    _0x22632a[_0x2d01(_0xfecf('0xc9'))] = function (_0xba96d6, _0x12df17, _0x37cafd, _0x170470, _0x3f67e5, _0x297d87) {
        this[_0x2d01(_0xfecf('0x7e'))][_0xba96d6] = [
            _0x12df17,
            _0x37cafd,
            _0x170470,
            _0x3f67e5,
            _0x297d87
        ];
    };
    _0x22632a[_0x2d01('0x3f')] = function () {
        if (_0xfecf('0xd3') !== 'SzdvA') {
            if ('nwybu' !== _0x2d01('0x40')) {
                x = this[_0x2d01(_0xfecf('0x7e'))][_0x8dc99b];
                if (x != null) {
                    if ('tdYLT' !== _0xfecf('0xd4')) {
                        x[_0x2d01(_0xfecf('0xd5'))] = ![];
                        this[_0x2d01(_0xfecf('0x9c'))](x);
                        x[_0xfecf('0xbb')]();
                    } else {
                        eval(this[_0x2d01(_0xfecf('0x9e'))]);
                    }
                }
                this[_0x2d01(_0xfecf('0x7e'))][_0x8dc99b] = null;
                delete this[_0x2d01(_0xfecf('0x7e'))][_0x8dc99b];
            } else {
                var _0x8dc99b, _0x33431d, _0x188b6c, _0x4fa8e1;
                this['_initVWStorage']();
                _0x4fa8e1 = this[_0xfecf('0x5d')];
                for (_0x8dc99b in _0x4fa8e1) {
                    _0x33431d = _0x4fa8e1[_0x8dc99b];
                    _0x188b6c = VPLAYER['GetVMByID'](_0x8dc99b);
                    if (_0x188b6c == null) {
                        if (_0xfecf('0xd6') === _0xfecf('0xd7')) {
                            this[_0x2d01('0x73')]();
                        } else {
                            if (_0x2d01(_0xfecf('0xd8')) !== _0x2d01(_0xfecf('0xd8'))) {
                                return this[_0x2d01('0x5')][_0x8dc99b];
                            } else {
                                delete this[_0x2d01('0x5')][_0x8dc99b];
                            }
                        }
                    } else {
                        if (!_0x188b6c[_0x2d01(_0xfecf('0xd9'))]()) {
                            delete this[_0x2d01(_0xfecf('0x7e'))][_0x8dc99b];
                        } else {
                            if (_0xfecf('0xda') === _0xfecf('0xda')) {
                                this[_0x2d01(_0xfecf('0x7e'))][_0x8dc99b][0x5] = _0x188b6c[_0x2d01(_0xfecf('0xdb'))]();
                            } else {
                                this[_0x2d01('0x51')] = _0x22b9ad;
                                this[_0x2d01('0x52')] = _0x250712;
                                if (this[_0x2d01('0xb')] === !![]) {
                                    this[_0x2d01(_0xfecf('0xdc'))] *= $gameMap[_0xfecf('0x6')]();
                                    this[_0x2d01(_0xfecf('0xdd'))] *= $gameMap[_0x2d01(_0xfecf('0xc7'))]();
                                }
                                return this['_xDuration'] = _0xa1cece;
                            }
                        }
                    }
                }
            }
        } else {
            if (_0x2d01('0x3b') !== _0x2d01(_0xfecf('0xcc'))) {
                return this[_0x2d01(_0xfecf('0xce'))] = !![];
            } else {
                _0x2e0e74 = _0x3cf9c1;
                return VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x2e0e74, _0x2d01('0x3d'));
            }
        }
    };
    _0x22632a[_0x2d01(_0xfecf('0xcb'))] = function (_0x283fd0) {
        if (_0xfecf('0xde') !== _0xfecf('0xde')) {
            _0x8cb78f[_0x2d01(_0xfecf('0xdf'))](_0x11d0d3[0x5]);
        } else {
            this[_0x2d01(_0xfecf('0xc8'))]();
            if (this[_0x2d01('0x5')][_0x283fd0] != null) {
                delete this[_0x2d01(_0xfecf('0x7e'))][_0x283fd0];
            }
        }
    };
    _0x22632a[_0x2d01(_0xfecf('0xe0'))] = function () {
        var _0x5a165a, _0x558aaa, _0x43cdeb, _0x16774d, _0x3f724b;
        this[_0x2d01(_0xfecf('0xc8'))]();
        _0x16774d = JsonEx[_0x2d01('0x47')](this[_0x2d01(_0xfecf('0x7e'))]);
        for (_0x558aaa in _0x16774d) {
            if ('kJFfI' !== _0x2d01(_0xfecf('0xe1'))) {
                _0x43cdeb = _0x16774d[_0x558aaa];
                switch (_0x43cdeb[0x4]) {
                case 0x0:
                    ShowVAnim(_0x558aaa, ..._0x43cdeb);
                    break;
                case 0x1:
                    ShowVAnimOnSpriteset(_0x558aaa, ..._0x43cdeb);
                    break;
                case 0x2:
                    ShowVAnimOnMap(_0x558aaa, ..._0x43cdeb);
                }
            } else {
                return this[_0x2d01(_0xfecf('0x7e'))] = {};
            }
        }
        for (_0x558aaa in _0x16774d) {
            if (_0x2d01(_0xfecf('0xe2')) !== _0x2d01(_0xfecf('0xe3'))) {
                _0x43cdeb = _0x16774d[_0x558aaa];
                try {
                    if (_0xfecf('0xe4') === _0xfecf('0xe4')) {
                        if ('SErzx' === _0x2d01(_0xfecf('0xe5'))) {
                            this[_0xfecf('0x4c')][_0x2d01(_0xfecf('0x9c'))](this);
                            this[_0x2d01('0x41')] = ![];
                            this[_0xfecf('0xe6')][_0x2d01(_0xfecf('0x9d'))]();
                            return this[_0xfecf('0x4d')] = !![];
                        } else {
                            _0x3f724b = VPLAYER[_0xfecf('0x2e')](_0x558aaa);
                            if (_0x43cdeb[0x5] != null) {
                                _0x3f724b[_0x2d01('0x4c')](_0x43cdeb[0x5]);
                            }
                        }
                    } else {
                        this[_0x2d01('0x24')][_0x2d01(_0xfecf('0x89'))] = this[_0x2d01(_0xfecf('0x8a'))][_0xfecf('0x77')][_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x8d'))];
                        this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x8e'))] = this['vidTexture'][_0x2d01(_0xfecf('0x8b'))][_0xfecf('0xe7')][_0x2d01(_0xfecf('0x8f'))];
                        this[_0x2d01(_0xfecf('0x90'))](this[_0x2d01('0x24')]);
                        this[_0x2d01(_0xfecf('0xe8'))] = !![];
                        this[_0xfecf('0xe7')] = this[_0x2d01(_0xfecf('0x8a'))][_0xfecf('0x77')][_0xfecf('0xe7')];
                        this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x91'))] = this[_0x2d01(_0xfecf('0x92'))];
                        this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x93'))]();
                        if (this[_0x2d01(_0xfecf('0x94'))] != null) {
                            return this[_0x2d01(_0xfecf('0x94'))]();
                        }
                    }
                } catch (_0x312998) {
                    _0x5a165a = _0x312998;
                    VPLAYER[_0x2d01('0xc')](_0x5a165a, 'restore\x20VM\x20saved\x20parameters');
                }
            } else {
                if ('aXNYE' !== _0xfecf('0xe9')) {
                    var _0x233620, _0x14bec5;
                    ALIAS_UPDTM[_0x2d01(_0xfecf('0xc2'))](this);
                    if (this[_0x2d01('0x30')][_0xfecf('0x5')][_0x2d01(_0xfecf('0xea'))] > 0x0) {
                        if (this[_0x2d01(_0xfecf('0xaf'))] == null) {
                            this[_0x2d01(_0xfecf('0xaf'))] = $gameMap[_0xfecf('0x6')]();
                            this[_0x2d01(_0xfecf('0xb0'))] = this[_0x2d01('0x34')] / 0x2;
                            this[_0x2d01(_0xfecf('0xb1'))] = $gameMap[_0xfecf('0xa')]();
                        }
                        _0x233620 = Math[_0x2d01(_0xfecf('0xb2'))]($gameMap[_0x2d01('0x38')](-0.5) * this[_0x2d01(_0xfecf('0xaf'))] + this[_0x2d01('0x33')]);
                        _0x14bec5 = Math[_0x2d01(_0xfecf('0xb2'))]($gameMap[_0x2d01(_0xfecf('0xb4'))](-0x1) * this[_0xfecf('0x9')] + this[_0x2d01(_0xfecf('0xb1'))]);
                        return this[_0x2d01('0x30')][_0x2d01(_0xfecf('0x82'))](_0x233620, _0x14bec5);
                    }
                } else {
                    if (_0x2d01(_0xfecf('0x83')) !== _0x2d01(_0xfecf('0x83'))) {
                        return this[_0x2d01('0xb')] = !![];
                    } else {
                        _0x46b635 = _0x430fd3;
                        VPLAYER[_0x2d01('0xc')](_0x46b635, _0xfecf('0xeb'));
                    }
                }
            }
        }
    };
    _0x22632a[_0xfecf('0xec')] = function (_0x5a2893, _0x960401, _0x26e4d9, _0x3072a6, _0x5584b8) {
        this[_0x2d01('0x45')]();
        this[_0x2d01(_0xfecf('0xc9'))](_0x5a2893, _0x960401, _0x26e4d9, _0x3072a6, _0x5584b8, 0x0);
    };
    _0x22632a[_0x2d01(_0xfecf('0xed'))] = function (_0x243a6b, _0x48b501, _0x52bdcd, _0xf8cc5b, _0x41bfdc) {
        if (_0x2d01(_0xfecf('0xee')) !== _0x2d01(_0xfecf('0xef'))) {
            if (_0xfecf('0xf0') === _0xfecf('0xf1')) {
                return obj[_0x2d01(_0xfecf('0xf2'))] != null && obj['meta'][symbol] != null;
            } else {
                this[_0x2d01('0x45')]();
                this[_0x2d01(_0xfecf('0xc9'))](_0x243a6b, _0x48b501, _0x52bdcd, _0xf8cc5b, _0x41bfdc, 0x1);
            }
        } else {
            if ('vpAyR' !== _0xfecf('0xf3')) {
                this[_0x2d01(_0xfecf('0xdc'))] *= $gameMap['tileWidth']();
                this[_0x2d01(_0xfecf('0xdd'))] *= $gameMap[_0x2d01(_0xfecf('0xc7'))]();
            } else {
                _0x5b9372[_0xfecf('0x24')]();
            }
        }
    };
    _0x22632a[_0x2d01(_0xfecf('0xf4'))] = function (_0x153db0, _0x3b3103, _0x542452, _0x2fc32d, _0x12d8c2) {
        if (_0x2d01(_0xfecf('0xf5')) !== _0x2d01(_0xfecf('0xf6'))) {
            this[_0x2d01(_0xfecf('0xc8'))]();
            this[_0x2d01(_0xfecf('0xc9'))](_0x153db0, _0x3b3103, _0x542452, _0x2fc32d, _0x12d8c2, 0x2);
        } else {
            s[_0x2d01('0x56')]();
        }
    };
}());
(function () {
    var _0x12592c, _0x2140e7;
    _0x2140e7 = Scene_Map[_0x2d01(_0xfecf('0xbd'))];
    _0x12592c = _0x2140e7[_0x2d01(_0xfecf('0xf7'))];
    _0x2140e7[_0x2d01('0x57')] = function () {
        var _0x2e9dce, _0x39d84f, _0x3794a2, _0x3d5546;
        if (this[_0x2d01(_0xfecf('0x7e'))] != null) {
            if (_0xfecf('0xf8') !== 'lKcJJ') {
                try {
                    if (_0x2d01(_0xfecf('0xf9')) === _0xfecf('0xfa')) {
                        if (_0xfecf('0xfb') !== _0xfecf('0xfb')) {
                            _0x5064a4['visible'] = ![];
                            this[_0x2d01(_0xfecf('0x9c'))](_0x5064a4);
                            _0x5064a4[_0x2d01(_0xfecf('0x9d'))]();
                        } else {
                            _0x2e9dce = error;
                            return VPLAYER[_0xfecf('0x80')](_0x2e9dce, _0x2d01(_0xfecf('0xd0')));
                        }
                    } else {
                        if (TouchInput[_0xfecf('0x6d')]()) {
                            _0x3794a2 = this[_0xfecf('0x5d')];
                            for (_0x39d84f in _0x3794a2) {
                                if ('eerwA' !== _0xfecf('0xfc')) {
                                    if (_0xfecf('0xfd') === _0x2d01(_0xfecf('0xfe'))) {
                                        var _0x28d85e;
                                        if (this[_0xfecf('0x27')] <= 0x0) {
                                            return;
                                        }
                                        _0x28d85e = this[_0x2d01('0x5a')];
                                        this[_0x2d01('0x5b')] = (this[_0x2d01(_0xfecf('0xff'))] * (_0x28d85e - 0x1) + this[_0xfecf('0x53')]) / _0x28d85e;
                                        return this[_0xfecf('0x27')]--;
                                    } else {
                                        _0x3d5546 = _0x3794a2[_0x39d84f];
                                        if (_0x3d5546 == null) {
                                            if ('hdUlv' !== 'eNPdY') {
                                                continue;
                                            } else {
                                                item = tempStorage[id];
                                                try {
                                                    vm = VPLAYER[_0x2d01(_0xfecf('0x100'))](id);
                                                    if (item[0x5] != null) {
                                                        vm[_0x2d01(_0xfecf('0xdf'))](item[0x5]);
                                                    }
                                                } catch (_0x60af85) {
                                                    _0x54103d = _0x60af85;
                                                    VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x54103d, 'restore\x20VM\x20saved\x20parameters');
                                                }
                                            }
                                        }
                                        if (_0x3d5546[_0xfecf('0x6e')]() && !_0x3d5546[_0x2d01(_0xfecf('0xac'))]()) {
                                            if (_0xfecf('0x101') === 'LxkUN') {
                                                this[_0x2d01(_0xfecf('0x102'))]();
                                                this[_0x2d01(_0xfecf('0x103'))]();
                                                return this[_0x2d01('0x94')]();
                                            } else {
                                                if (_0x3d5546['isInMouseTouchPosition']()) {
                                                    if ('XiGEC' !== _0x2d01(_0xfecf('0x104'))) {
                                                        if ('BRCXS' === _0xfecf('0x105')) {
                                                            return ![];
                                                        } else {
                                                            return;
                                                        }
                                                    } else {
                                                        if (_0xfecf('0x106') !== _0xfecf('0x106')) {
                                                            this[_0x2d01('0x5')][id] = [
                                                                name,
                                                                x,
                                                                y,
                                                                isLoop,
                                                                stateFlag
                                                            ];
                                                        } else {
                                                            if (this[_0x2d01('0x5')][id] != null) {
                                                                this[_0xfecf('0xca')](id);
                                                            }
                                                            this[_0x2d01(_0xfecf('0x7e'))][id] = new VWSprite(name);
                                                            s = this[_0x2d01('0x5')][id];
                                                            if (isLoop === !![]) {
                                                                s[_0x2d01(_0xfecf('0x107'))]();
                                                            }
                                                            s[_0x2d01(_0xfecf('0x7b'))]();
                                                            s[_0x2d01(_0xfecf('0x82'))](x, y);
                                                            this[_0x2d01(_0xfecf('0x90'))](s);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (_0x2d01(_0xfecf('0xb8')) !== _0x2d01(_0xfecf('0xb9'))) {
                                        _0xbeb17[_0x2d01(_0xfecf('0xa3'))]();
                                        return;
                                    } else {
                                        return this[_0xfecf('0x50')] = !![];
                                    }
                                }
                            }
                        }
                    }
                } catch (_0x3ef3dd) {
                    _0x2e9dce = _0x3ef3dd;
                    VPLAYER[_0xfecf('0x80')](_0x2e9dce, _0x2d01('0x57'));
                }
            } else {
                var _0x4e6003, _0x199c01;
                if (this[_0x2d01('0x5b')] === 0x0) {
                    if (_0xfecf('0xab') === _0x2d01(_0xfecf('0x108'))) {
                        return this[_0x2d01(_0xfecf('0x92'))] === !![] && !this[_0x2d01('0x1c')]();
                    } else {
                        return ![];
                    }
                }
                _0x4e6003 = Sprite_Button[_0xfecf('0x72')][_0x2d01(_0xfecf('0x109'))][_0x2d01('0x1')](this, TouchInput['x']);
                _0x199c01 = Sprite_Button[_0x2d01(_0xfecf('0xbd'))][_0x2d01('0x8b')][_0x2d01(_0xfecf('0xc2'))](this, TouchInput['y']);
                return _0x4e6003 >= 0x0 && _0x199c01 >= 0x0 && _0x4e6003 < this[_0xfecf('0xe6')][_0x2d01(_0xfecf('0x89'))] * this[_0x2d01(_0xfecf('0x99'))]['x'] && _0x199c01 < this[_0xfecf('0xe6')][_0xfecf('0x0')] * this[_0x2d01(_0xfecf('0x99'))]['y'];
            }
        }
        return _0x12592c[_0x2d01('0x1')](this);
    };
}());
(function () {
    var _0x113b90;
    _0x113b90 = Sprite_Enemy[_0x2d01(_0xfecf('0xbd'))];
    _0x113b90[_0x2d01(_0xfecf('0x10a'))] = function () {
        if (_0x2d01('0x5e') !== _0xfecf('0x10b')) {
            if (_0xfecf('0x10c') !== _0xfecf('0x10c')) {
                _0x33434f = _0x41e1f1;
                VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x33434f, _0xfecf('0x10d'));
            } else {
                var _0x33434f, _0x3c28a9;
                try {
                    if ('oGtqs' === _0xfecf('0x10e')) {
                        if (this[_0x2d01(_0xfecf('0x7e'))][id] != null) {
                            this[_0xfecf('0xca')](id);
                        }
                        this[_0x2d01(_0xfecf('0x7e'))][id] = new VWSprite(name);
                        s = this[_0x2d01(_0xfecf('0x7e'))][id];
                        if (isLoop === !![]) {
                            s[_0x2d01(_0xfecf('0x107'))]();
                        }
                        s[_0x2d01(_0xfecf('0x7b'))]();
                        s[_0x2d01(_0xfecf('0x82'))](x, y);
                        this[_0x2d01(_0xfecf('0x90'))](s);
                    } else {
                        _0x3c28a9 = function (_0x9f4d43, _0x3ff124) {
                            if (_0xfecf('0x10f') === 'PpgqR') {
                                this[_0xfecf('0xe6')]['width'] = this[_0xfecf('0x110')][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x8d'))];
                                this[_0xfecf('0xe6')][_0xfecf('0x0')] = this[_0xfecf('0x110')][_0x2d01(_0xfecf('0x8b'))][_0xfecf('0xe7')][_0x2d01(_0xfecf('0x8f'))];
                                this[_0x2d01(_0xfecf('0x90'))](this[_0x2d01(_0xfecf('0x88'))]);
                                this[_0x2d01('0x70')] = !![];
                                this[_0xfecf('0xe7')] = this[_0x2d01(_0xfecf('0x8a'))][_0x2d01(_0xfecf('0x8b'))][_0xfecf('0xe7')];
                                this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x91'))] = this['_loop'];
                                this['source'][_0x2d01('0x2f')]();
                                if (this[_0xfecf('0x65')] != null) {
                                    return this[_0x2d01('0xf')]();
                                }
                            } else {
                                if (_0x2d01(_0xfecf('0x111')) === _0xfecf('0x112')) {
                                    var _0x59f895;
                                    if (this[_0x2d01(_0xfecf('0x113'))] <= 0x0) {
                                        return;
                                    }
                                    _0x59f895 = this[_0x2d01(_0xfecf('0x113'))];
                                    this['x'] = (this['x'] * (_0x59f895 - 0x1) + this[_0x2d01(_0xfecf('0xdc'))]) / _0x59f895;
                                    this['y'] = (this['y'] * (_0x59f895 - 0x1) + this[_0x2d01(_0xfecf('0xdd'))]) / _0x59f895;
                                    return this[_0x2d01(_0xfecf('0x113'))]--;
                                } else {
                                    if (_0xfecf('0x114') !== _0xfecf('0x115')) {
                                        return _0x3ff124[_0x2d01('0x61')] != null && _0x3ff124[_0x2d01('0x61')][_0x9f4d43] != null;
                                    } else {
                                        return this[_0x2d01(_0xfecf('0x94'))]();
                                    }
                                }
                            }
                        };
                        return _0x3c28a9('VW', this[_0x2d01(_0xfecf('0x116'))][_0x2d01('0x63')]());
                    }
                } catch (_0x574e94) {
                    _0x33434f = _0x574e94;
                    VPLAYER[_0x2d01('0xc')](_0x33434f, _0x2d01(_0xfecf('0x117')));
                    return ![];
                }
            }
        } else {
            item = tempStorage[id];
            try {
                if ('AMofQ' !== _0xfecf('0x118')) {
                    vm = VPLAYER[_0x2d01(_0xfecf('0x100'))](id);
                    if (item[0x5] != null) {
                        if ('IweDS' === _0xfecf('0x119')) {
                            return this[_0x2d01(_0xfecf('0xce'))] = !![];
                        } else {
                            vm[_0x2d01(_0xfecf('0xdf'))](item[0x5]);
                        }
                    }
                } else {
                    _0x1415f8 = error;
                    return VPLAYER[_0xfecf('0x80')](_0x1415f8, _0x2d01(_0xfecf('0xd0')));
                }
            } catch (_0x108504) {
                _0x33434f = _0x108504;
                VPLAYER[_0x2d01('0xc')](_0x33434f, _0xfecf('0x10d'));
            }
        }
    };
    _0x113b90[_0x2d01('0x66')] = function (_0xd5e53d) {
        if (_0x2d01(_0xfecf('0x11a')) === _0x2d01('0x67')) {
            var _0x148e76, _0x270556;
            this['bitmap'] = new Bitmap(0x64, 0x64);
            try {
                if (_0xfecf('0x31') !== _0x2d01(_0xfecf('0x11b'))) {
                    return obj[_0x2d01(_0xfecf('0xf2'))] != null && obj['meta'][symbol] != null;
                } else {
                    if (_0xfecf('0x11c') !== _0xfecf('0x11d')) {
                        if (this[_0x2d01(_0xfecf('0x11e'))] != null) {
                            if (_0xfecf('0x11f') !== 'hhZIO') {
                                if (_0xfecf('0x120') !== _0xfecf('0x120')) {
                                    this[_0x2d01(_0xfecf('0x86'))](_0x8257f7);
                                } else {
                                    this[_0x2d01(_0xfecf('0x8a'))] = PIXI[_0x2d01(_0xfecf('0x121'))][_0x2d01(_0xfecf('0x122'))](_0x2d01(_0xfecf('0x123')) + this['filename'] + _0x2d01('0x6d'));
                                    this[_0xfecf('0xe6')] = new PIXI[(_0xfecf('0x47'))](this[_0x2d01('0x26')]);
                                    this[_0x2d01(_0xfecf('0x8c'))] = null;
                                    this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x124'))][_0x2d01(_0xfecf('0x8b'))]['on'](_0x2d01('0x6f'), () => {
                                        this['surface'][_0xfecf('0x76')] = this[_0xfecf('0x110')][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x8d'))];
                                        this[_0xfecf('0xe6')][_0xfecf('0x0')] = this[_0xfecf('0x110')][_0x2d01(_0xfecf('0x8b'))]['source'][_0x2d01('0x2b')];
                                        this[_0x2d01('0x2c')](this[_0x2d01(_0xfecf('0x88'))]);
                                        this[_0x2d01(_0xfecf('0xe8'))] = !![];
                                        this[_0xfecf('0xe7')] = this[_0x2d01('0x26')][_0x2d01(_0xfecf('0x8b'))][_0xfecf('0xe7')];
                                        this[_0x2d01(_0xfecf('0x8c'))][_0x2d01('0x2d')] = this['_loop'];
                                        this[_0xfecf('0xe7')][_0x2d01(_0xfecf('0x93'))]();
                                        if (this[_0xfecf('0x65')] != null) {
                                            return this[_0x2d01(_0xfecf('0x94'))]();
                                        }
                                    });
                                    return this[_0x2d01(_0xfecf('0x88'))][_0x2d01('0x6e')][_0x2d01(_0xfecf('0x8b'))][_0x2d01('0x28')][_0x2d01('0x71')](_0x2d01(_0xfecf('0x125')), () => {
                                        return this[_0xfecf('0x49')]();
                                    });
                                }
                            } else {
                                if (_0xfecf('0x126') !== 'mHTrD') {
                                    vm = VPLAYER[_0x2d01(_0xfecf('0x100'))](id);
                                    if (item[0x5] != null) {
                                        vm[_0x2d01('0x4c')](item[0x5]);
                                    }
                                } else {
                                    this[_0x2d01(_0xfecf('0x11e'))][_0x2d01(_0xfecf('0x127'))]();
                                }
                            }
                        }
                        this[_0x2d01(_0xfecf('0x11e'))] = new VWSprite(_0xd5e53d);
                        this[_0x2d01(_0xfecf('0x11e'))][_0x2d01(_0xfecf('0x107'))]();
                        _0x270556 = function () {
                            if (_0x2d01(_0xfecf('0x128')) !== _0x2d01(_0xfecf('0x128'))) {
                                if (_0xfecf('0x129') === 'ebmfb') {
                                    _0x2b19a7[_0xfecf('0x79')](_0x2b19a7['shift']());
                                } else {
                                    var _0x96657d, _0x7b06b5;
                                    try {
                                        _0x7b06b5 = this[_0x2d01(_0xfecf('0x7e'))][id];
                                        if (_0x7b06b5 != null) {
                                            if (_0xfecf('0x12a') === _0xfecf('0x12b')) {
                                                if (_0x2d01(_0xfecf('0xee')) !== _0x2d01(_0xfecf('0xef'))) {
                                                    this[_0x2d01(_0xfecf('0xc8'))]();
                                                    this[_0x2d01(_0xfecf('0xc9'))](_0x496e6f, _0x54bf22, _0x8fae62, _0x5570b3, _0x4a7951, 0x1);
                                                } else {
                                                    this[_0x2d01(_0xfecf('0xdc'))] *= $gameMap[_0xfecf('0x6')]();
                                                    this[_0x2d01('0x52')] *= $gameMap[_0x2d01(_0xfecf('0xc7'))]();
                                                }
                                            } else {
                                                _0x7b06b5[_0xfecf('0x15')] = ![];
                                                this[_0x2d01(_0xfecf('0x9c'))](_0x7b06b5);
                                                _0x7b06b5[_0xfecf('0xbb')]();
                                            }
                                        }
                                        this[_0x2d01(_0xfecf('0x7e'))][id] = null;
                                        delete this[_0x2d01(_0xfecf('0x7e'))][id];
                                    } catch (_0x20ca7e) {
                                        _0x96657d = _0x20ca7e;
                                        VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x96657d, _0x2d01(_0xfecf('0x9f')));
                                    }
                                }
                            } else {
                                var _0x44e96c, _0x190ac5;
                                _0x190ac5 = this[_0x2d01(_0xfecf('0x11e'))][_0x2d01(_0xfecf('0x88'))][_0x2d01('0x25')];
                                _0x44e96c = this[_0x2d01(_0xfecf('0x11e'))][_0xfecf('0xe6')][_0x2d01(_0xfecf('0x12c'))];
                                this[_0x2d01('0x69')][_0x2d01('0x9')](_0x190ac5 / -0x2, -_0x44e96c);
                                this[_0x2d01('0x76')] = new Bitmap(_0x190ac5, _0x44e96c);
                            }
                        };
                        this[_0x2d01(_0xfecf('0x11e'))][_0x2d01(_0xfecf('0x12d'))](_0x270556[_0xfecf('0x12e')](this));
                        this[_0xfecf('0x32')][_0x2d01(_0xfecf('0x7b'))]();
                        this[_0x2d01(_0xfecf('0x90'))](this[_0xfecf('0x32')]);
                    } else {
                        var _0x24c323;
                        if (this[_0x2d01(_0xfecf('0x12f'))] <= 0x0) {
                            return;
                        }
                        _0x24c323 = this[_0x2d01('0x5a')];
                        this[_0xfecf('0x130')] = (this[_0x2d01(_0xfecf('0xff'))] * (_0x24c323 - 0x1) + this[_0x2d01(_0xfecf('0x131'))]) / _0x24c323;
                        return this[_0x2d01(_0xfecf('0x12f'))]--;
                    }
                }
            } catch (_0xb279d1) {
                if (_0xfecf('0x132') === _0xfecf('0x132')) {
                    _0x148e76 = _0xb279d1;
                    VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x148e76, _0x2d01(_0xfecf('0xcf')));
                } else {
                    if (_0x2d01(_0xfecf('0xf5')) !== _0x2d01('0x55')) {
                        this[_0x2d01(_0xfecf('0xc8'))]();
                        this[_0x2d01('0x3e')](_0x3ad9eb, _0x24972b, _0x504b1b, _0x4dc5d2, _0x50460f, 0x2);
                    } else {
                        s[_0x2d01(_0xfecf('0x107'))]();
                    }
                }
            }
        } else {
            try {
                eval(this[_0x2d01('0x79')]);
            } catch (_0x368566) {
                _0x148e76 = _0x368566;
                VPLAYER[_0xfecf('0x80')](_0x148e76, _0x2d01(_0xfecf('0x133')));
            }
        }
    };
}());
VWSprite = class VWSprite extends Sprite {
    constructor(_0x484b26) {
        super();
        this['filename'] = _0x484b26;
        this[_0x2d01(_0xfecf('0xe8'))] = ![];
        this[_0x2d01(_0xfecf('0x92'))] = ![];
        this[_0x2d01('0x7b')] = 0x0;
        this[_0x2d01(_0xfecf('0x9e'))] = null;
        this[_0x2d01(_0xfecf('0x134'))] = 0x0;
        this[_0x2d01(_0xfecf('0x135'))] = null;
        this[_0xfecf('0x10')] = ![];
        this[_0x2d01(_0xfecf('0x136'))] = 0x0;
        this[_0xfecf('0x50')] = ![];
        this[_0xfecf('0x29')] = 0x0;
        this[_0x2d01(_0xfecf('0x98'))] = 0x0;
        this[_0x2d01(_0xfecf('0x12f'))] = 0x0;
        this[_0x2d01(_0xfecf('0x84'))] = ![];
    }
    [_0x2d01(_0xfecf('0x107'))]() {
        return this[_0x2d01('0x2e')] = !![];
    }
    [_0xfecf('0x40')](_0x358b5f) {
        this[_0xfecf('0x65')] = _0x358b5f;
    }
    [_0x2d01(_0xfecf('0x7b'))]() {
        this[_0x2d01('0x26')] = PIXI['Texture'][_0x2d01('0x6b')](_0x2d01(_0xfecf('0x123')) + this[_0xfecf('0x137')] + _0x2d01('0x6d'));
        this[_0x2d01(_0xfecf('0x88'))] = new PIXI[(_0x2d01(_0xfecf('0x138')))](this[_0xfecf('0x110')]);
        this[_0xfecf('0xe7')] = null;
        this[_0x2d01('0x24')][_0x2d01('0x6e')][_0x2d01(_0xfecf('0x8b'))]['on'](_0xfecf('0x38'), () => {
            if (_0xfecf('0x139') !== _0xfecf('0x139')) {
                return this[_0x2d01(_0xfecf('0x84'))] = !![];
            } else {
                if (_0x2d01(_0xfecf('0x13a')) === _0xfecf('0x48')) {
                    if ('OjqTf' === 'OjqTf') {
                        this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x89'))] = this[_0x2d01(_0xfecf('0x8a'))][_0xfecf('0x77')][_0x2d01(_0xfecf('0x8c'))][_0x2d01('0x29')];
                        this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x8e'))] = this[_0xfecf('0x110')][_0x2d01(_0xfecf('0x8b'))][_0xfecf('0xe7')][_0x2d01(_0xfecf('0x8f'))];
                        this[_0x2d01(_0xfecf('0x90'))](this[_0x2d01('0x24')]);
                        this[_0x2d01(_0xfecf('0xe8'))] = !![];
                        this[_0xfecf('0xe7')] = this[_0x2d01(_0xfecf('0x8a'))]['baseTexture'][_0xfecf('0xe7')];
                        this[_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x91'))] = this[_0x2d01(_0xfecf('0x92'))];
                        this[_0x2d01(_0xfecf('0x8c'))][_0x2d01('0x2f')]();
                        if (this[_0x2d01(_0xfecf('0x94'))] != null) {
                            if (_0xfecf('0x13b') === _0xfecf('0x13c')) {
                                this[_0x2d01(_0xfecf('0x131'))] = _0x18c452;
                                return this[_0x2d01('0x5a')] = _0x4547af;
                            } else {
                                return this[_0x2d01(_0xfecf('0x94'))]();
                            }
                        }
                    } else {
                        _0x46b635 = _0x451d39;
                        VPLAYER[_0xfecf('0x80')](_0x46b635, _0x2d01(_0xfecf('0x81')));
                    }
                } else {
                    if (_0xfecf('0x13d') !== _0xfecf('0x13d')) {
                        var _0x589ea9;
                        if (this['_sDurationX'] <= 0x0) {
                            return;
                        }
                        _0x589ea9 = this[_0x2d01(_0xfecf('0x98'))];
                        this[_0x2d01(_0xfecf('0x99'))]['x'] = (this[_0xfecf('0x9b')]['x'] * (_0x589ea9 - 0x1) + this[_0x2d01(_0xfecf('0x9a'))]) / _0x589ea9;
                        this[_0xfecf('0x9b')]['y'] = (this['scale']['y'] * (_0x589ea9 - 0x1) + this['_targetScaleY']) / _0x589ea9;
                        return this[_0x2d01(_0xfecf('0x98'))]--;
                    } else {
                        this[_0x2d01('0x5')][id] = [
                            name,
                            x,
                            y,
                            isLoop,
                            stateFlag
                        ];
                    }
                }
            }
        });
        return this[_0x2d01(_0xfecf('0x88'))][_0x2d01(_0xfecf('0x124'))][_0x2d01(_0xfecf('0x8b'))][_0x2d01(_0xfecf('0x8c'))][_0x2d01(_0xfecf('0x13e'))](_0x2d01(_0xfecf('0x125')), () => {
            return this['_onEnd']();
        });
    }
    [_0x2d01(_0xfecf('0x13f'))]() {
        var _0x30cb02, _0xe680e;
        try {
            _0xe680e = ![];
            if (this[_0x2d01(_0xfecf('0x140'))] > 0x0) {
                if (_0xfecf('0x141') !== _0xfecf('0x142')) {
                    $gameTemp[_0x2d01(_0xfecf('0x143'))](this[_0x2d01('0x7b')]);
                    _0xe680e = !![];
                } else {
                    this[_0x2d01(_0xfecf('0x11e'))][_0x2d01(_0xfecf('0x127'))]();
                }
            }
            if (this[_0x2d01(_0xfecf('0x9e'))] != null) {
                try {
                    if (_0xfecf('0x144') === _0xfecf('0x144')) {
                        eval(this[_0x2d01(_0xfecf('0x9e'))]);
                    } else {
                        if (_0xbeb17['isInMouseTouchPosition']()) {
                            if (_0x2d01('0x1d') !== _0x2d01(_0xfecf('0xb9'))) {
                                _0xbeb17[_0x2d01(_0xfecf('0xa3'))]();
                                return;
                            } else {
                                return this[_0xfecf('0x50')] = !![];
                            }
                        }
                    }
                } catch (_0x4fd30c) {
                    if (_0xfecf('0x4b') === _0x2d01(_0xfecf('0x145'))) {
                        _0x30cb02 = _0x4fd30c;
                        VPLAYER[_0xfecf('0x80')](_0x30cb02, _0x2d01(_0xfecf('0x81')));
                    } else {
                        if (_0xfecf('0x146') !== _0xfecf('0x146')) {
                            $gameTemp[_0xfecf('0x4a')](this[_0x2d01(_0xfecf('0x134'))]);
                        } else {
                            _0x30cb02 = _0x4fd30c;
                            VPLAYER[_0x2d01('0xc')](_0x30cb02, _0x2d01(_0xfecf('0xcf')));
                        }
                    }
                }
                _0xe680e = !![];
            }
            if (_0xe680e === ![] || this[_0x2d01(_0xfecf('0xce'))] === !![]) {
                if (_0xfecf('0x147') === 'deLUK') {
                    return this[_0x2d01('0x73')]();
                } else {
                    _0x8cb78f = VPLAYER[_0xfecf('0x2e')](_0x58429a);
                    if (_0x11d0d3[0x5] != null) {
                        _0x8cb78f[_0x2d01(_0xfecf('0xdf'))](_0x11d0d3[0x5]);
                    }
                }
            }
        } catch (_0x4bf02b) {
            _0x30cb02 = _0x4bf02b;
            return VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x30cb02, 'On\x20Animation\x20End');
        }
    }
    [_0x2d01(_0xfecf('0x127'))]() {
        if (this[_0x2d01('0x83')] != null) {
            if ('rhUdh' === _0xfecf('0x148')) {
                return [
                    this[_0x2d01(_0xfecf('0x140'))],
                    this[_0x2d01('0x7')],
                    this[_0x2d01(_0xfecf('0x134'))],
                    this[_0x2d01(_0xfecf('0x135'))],
                    this[_0x2d01('0x3c')],
                    this[_0x2d01('0x7d')],
                    this[_0x2d01(_0xfecf('0x149'))],
                    this[_0xfecf('0x29')],
                    this[_0x2d01(_0xfecf('0x98'))],
                    this[_0x2d01(_0xfecf('0x12f'))],
                    this[_0x2d01(_0xfecf('0x99'))]['x'],
                    this[_0xfecf('0x9b')]['y'],
                    this[_0x2d01(_0xfecf('0xff'))],
                    this['x'],
                    this['y']
                ];
            } else {
                this[_0x2d01(_0xfecf('0x14a'))][_0x2d01('0x14')](this);
                this[_0x2d01(_0xfecf('0xd5'))] = ![];
                this[_0xfecf('0xe6')][_0xfecf('0xbb')]();
                return this[_0x2d01(_0xfecf('0xbc'))] = !![];
            }
        }
    }
    [_0x2d01(_0xfecf('0x14b'))]() {
        return this[_0x2d01(_0xfecf('0xe8'))] === !![];
    }
    [_0xfecf('0x6f')]() {
        return this[_0x2d01(_0xfecf('0xbc'))] === !![];
    }
    [_0x2d01(_0xfecf('0x14c'))]() {
        return this[_0x2d01('0x3c')] = !![];
    }
    [_0x2d01(_0xfecf('0x14d'))]() {
        return this[_0x2d01('0x88')] = !![];
    }
    ['setOnMap']() {
        return this[_0x2d01(_0xfecf('0x84'))] = !![];
    }
    [_0xfecf('0x16')]() {
        return this[_0x2d01(_0xfecf('0x92'))] === !![] && !this['isDestroyed']();
    }
    [_0xfecf('0xb7')]() {
        var _0x40ebf8, _0x3ba17d;
        if (this[_0x2d01(_0xfecf('0xff'))] === 0x0) {
            if (_0xfecf('0xab') === _0x2d01(_0xfecf('0x108'))) {
                if ('NrcRG' === _0xfecf('0x14e')) {
                    return this[_0x2d01('0x2e')] === !![] && !this[_0x2d01(_0xfecf('0xac'))]();
                } else {
                    s[_0x2d01(_0xfecf('0x107'))]();
                }
            } else {
                if (_0xfecf('0x14f') === _0xfecf('0x14f')) {
                    return ![];
                } else {
                    if (_0x907aec == null) {
                        if (_0x2d01(_0xfecf('0x150')) === _0x2d01(_0xfecf('0x151'))) {
                            this[_0x2d01(_0xfecf('0x9a'))] = x;
                            this[_0xfecf('0x152')] = y;
                            return this[_0x2d01(_0xfecf('0x98'))] = d;
                        } else {
                            return;
                        }
                    }
                    this[_0x2d01('0x7b')] = _0x907aec[0x0];
                    this[_0x2d01(_0xfecf('0x9e'))] = _0x907aec[0x1];
                    this[_0x2d01(_0xfecf('0x134'))] = _0x907aec[0x2];
                    this[_0x2d01('0x79')] = _0x907aec[0x3];
                    this[_0x2d01(_0xfecf('0xce'))] = _0x907aec[0x4];
                    this[_0x2d01(_0xfecf('0x136'))] = _0x907aec[0x5];
                    this[_0x2d01(_0xfecf('0x149'))] = _0x907aec[0x6];
                    this[_0x2d01(_0xfecf('0x113'))] = _0x907aec[0x7];
                    this[_0x2d01('0x11')] = _0x907aec[0x8];
                    this[_0x2d01('0x5a')] = _0x907aec[0x9];
                    this[_0x2d01(_0xfecf('0x99'))]['x'] = _0x907aec[0xa];
                    this[_0x2d01(_0xfecf('0x99'))]['y'] = _0x907aec[0xb];
                    this[_0x2d01(_0xfecf('0xff'))] = _0x907aec[0xc];
                    this['x'] = _0x907aec[0xd];
                    this['y'] = _0x907aec[0xe];
                }
            }
        }
        _0x40ebf8 = Sprite_Button[_0xfecf('0x72')][_0x2d01(_0xfecf('0x109'))][_0x2d01('0x1')](this, TouchInput['x']);
        _0x3ba17d = Sprite_Button[_0x2d01(_0xfecf('0xbd'))][_0x2d01('0x8b')][_0x2d01(_0xfecf('0xc2'))](this, TouchInput['y']);
        return _0x40ebf8 >= 0x0 && _0x3ba17d >= 0x0 && _0x40ebf8 < this['surface'][_0x2d01(_0xfecf('0x89'))] * this[_0x2d01('0x12')]['x'] && _0x3ba17d < this['surface'][_0xfecf('0x0')] * this[_0x2d01('0x12')]['y'];
    }
    [_0x2d01(_0xfecf('0xb5'))]() {
        return this[_0x2d01('0x79')] != null || this[_0x2d01(_0xfecf('0x134'))] > 0x0;
    }
    [_0x2d01(_0xfecf('0xa3'))]() {
        var _0x3e5b16;
        if (this[_0x2d01(_0xfecf('0x134'))] > 0x0) {
            $gameTemp[_0xfecf('0x4a')](this[_0x2d01(_0xfecf('0x134'))]);
        }
        if (this['onActionScript'] != null) {
            if ('oPipn' !== _0xfecf('0x153')) {
                _0x1b6236 = _0x5df694;
                VPLAYER[_0x2d01('0xc')](_0x1b6236, _0xfecf('0x10d'));
            } else {
                try {
                    if (_0xfecf('0x154') !== 'mtymq') {
                        if (_0x2d01('0x8c') !== _0x2d01(_0xfecf('0x155'))) {
                            if (_0xfecf('0x156') !== _0xfecf('0x157')) {
                                _0x3e5b16 = error;
                                return VPLAYER[_0x2d01('0xc')](_0x3e5b16, _0x2d01(_0xfecf('0x158')));
                            } else {
                                if ('AJreE' === _0x2d01(_0xfecf('0x145'))) {
                                    _0x5d3f88 = _0x463205;
                                    VPLAYER['printError'](_0x5d3f88, _0x2d01(_0xfecf('0x81')));
                                } else {
                                    _0x5d3f88 = _0x463205;
                                    VPLAYER[_0x2d01('0xc')](_0x5d3f88, _0x2d01(_0xfecf('0xcf')));
                                }
                            }
                        } else {
                            eval(this[_0xfecf('0x42')]);
                        }
                    } else {
                        this[_0xfecf('0xca')](id);
                    }
                } catch (_0x16d5f1) {
                    _0x3e5b16 = _0x16d5f1;
                    VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x3e5b16, _0x2d01(_0xfecf('0x133')));
                }
            }
        }
        if (this[_0x2d01(_0xfecf('0x149'))] === !![]) {
            if (_0xfecf('0x159') === 'htNyR') {
                var _0x3fdd96 = function (_0x164cc5) {
                    while (--_0x164cc5) {
                        _0x2b19a7['push'](_0x2b19a7['shift']());
                    }
                };
                _0x3fdd96(++_0x1e3e9a);
            } else {
                this[_0x2d01(_0xfecf('0x127'))]();
            }
        }
    }
    [_0x2d01(_0xfecf('0x15a'))](_0xf34b5c, _0xd48f1a, _0x48918c) {
        this[_0x2d01(_0xfecf('0xdc'))] = _0xf34b5c;
        this[_0x2d01('0x52')] = _0xd48f1a;
        if (this[_0x2d01(_0xfecf('0x84'))] === !![]) {
            this[_0x2d01(_0xfecf('0xdc'))] *= $gameMap['tileWidth']();
            this[_0x2d01(_0xfecf('0xdd'))] *= $gameMap[_0x2d01(_0xfecf('0xc7'))]();
        }
        return this[_0xfecf('0x29')] = _0x48918c;
    }
    ['scaleSlow'](_0x11e14b, _0x7a4154, _0x52419b) {
        this[_0x2d01(_0xfecf('0x9a'))] = _0x11e14b;
        this[_0x2d01('0x8f')] = _0x7a4154;
        return this[_0x2d01('0x11')] = _0x52419b;
    }
    ['opacitySlow'](_0x4a6705, _0x1585e7) {
        this[_0x2d01(_0xfecf('0x131'))] = _0x4a6705;
        return this[_0x2d01(_0xfecf('0x12f'))] = _0x1585e7;
    }
    [_0x2d01(_0xfecf('0x15b'))]() {
        this[_0x2d01('0x92')]();
        this[_0x2d01('0x93')]();
        return this[_0x2d01(_0xfecf('0x15c'))]();
    }
    [_0xfecf('0x55')]() {
        var _0x3e442a;
        if (this[_0x2d01(_0xfecf('0x113'))] <= 0x0) {
            if (_0xfecf('0x15d') !== _0xfecf('0x15d')) {
                _0x3b1c0e = _0x135fe6;
                VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x3b1c0e, _0x2d01(_0xfecf('0xcf')));
            } else {
                return;
            }
        }
        _0x3e442a = this[_0x2d01('0x60')];
        this['x'] = (this['x'] * (_0x3e442a - 0x1) + this[_0x2d01(_0xfecf('0xdc'))]) / _0x3e442a;
        this['y'] = (this['y'] * (_0x3e442a - 0x1) + this[_0xfecf('0x15e')]) / _0x3e442a;
        return this[_0x2d01(_0xfecf('0x113'))]--;
    }
    [_0xfecf('0x56')]() {
        var _0x452c66;
        if (this[_0xfecf('0x67')] <= 0x0) {
            if ('OscFP' !== 'ieNqw') {
                return;
            } else {
                vm[_0x2d01(_0xfecf('0xdf'))](item[0x5]);
            }
        }
        _0x452c66 = this[_0x2d01(_0xfecf('0x98'))];
        this[_0x2d01(_0xfecf('0x99'))]['x'] = (this[_0x2d01(_0xfecf('0x99'))]['x'] * (_0x452c66 - 0x1) + this[_0x2d01(_0xfecf('0x9a'))]) / _0x452c66;
        this[_0x2d01(_0xfecf('0x99'))]['y'] = (this[_0x2d01(_0xfecf('0x99'))]['y'] * (_0x452c66 - 0x1) + this[_0xfecf('0x152')]) / _0x452c66;
        return this[_0x2d01('0x11')]--;
    }
    [_0xfecf('0x57')]() {
        var _0x2cae1b;
        if (this[_0x2d01(_0xfecf('0x12f'))] <= 0x0) {
            return;
        }
        _0x2cae1b = this[_0x2d01(_0xfecf('0x12f'))];
        this['opacity'] = (this[_0x2d01(_0xfecf('0xff'))] * (_0x2cae1b - 0x1) + this[_0x2d01('0x90')]) / _0x2cae1b;
        return this[_0x2d01(_0xfecf('0x12f'))]--;
    }
    [_0x2d01(_0xfecf('0xdb'))]() {
        return [
            this[_0x2d01('0x7b')],
            this[_0x2d01(_0xfecf('0x9e'))],
            this[_0x2d01('0x7c')],
            this[_0x2d01(_0xfecf('0x135'))],
            this[_0x2d01(_0xfecf('0xce'))],
            this[_0x2d01(_0xfecf('0x136'))],
            this[_0x2d01('0x88')],
            this[_0xfecf('0x29')],
            this[_0x2d01('0x11')],
            this[_0x2d01(_0xfecf('0x12f'))],
            this[_0x2d01(_0xfecf('0x99'))]['x'],
            this[_0xfecf('0x9b')]['y'],
            this[_0x2d01('0x5b')],
            this['x'],
            this['y']
        ];
    }
    [_0x2d01(_0xfecf('0xdf'))](_0x4bcc25) {
        if (_0x4bcc25 == null) {
            if (_0xfecf('0x15f') === 'uvXXG') {
                if (_0x2d01(_0xfecf('0x150')) === _0x2d01(_0xfecf('0x151'))) {
                    this[_0x2d01(_0xfecf('0x9a'))] = x;
                    this['_targetScaleY'] = y;
                    return this[_0x2d01(_0xfecf('0x98'))] = d;
                } else {
                    if (_0xfecf('0x160') !== _0xfecf('0x161')) {
                        return;
                    } else {
                        _0x1095b3 = _0x52b923;
                        VPLAYER[_0x2d01(_0xfecf('0x85'))](_0x1095b3, _0x2d01(_0xfecf('0x133')));
                    }
                }
            } else {
                return;
            }
        }
        this[_0x2d01(_0xfecf('0x140'))] = _0x4bcc25[0x0];
        this[_0x2d01('0x7')] = _0x4bcc25[0x1];
        this[_0x2d01('0x7c')] = _0x4bcc25[0x2];
        this[_0x2d01(_0xfecf('0x135'))] = _0x4bcc25[0x3];
        this[_0x2d01(_0xfecf('0xce'))] = _0x4bcc25[0x4];
        this[_0x2d01(_0xfecf('0x136'))] = _0x4bcc25[0x5];
        this[_0x2d01(_0xfecf('0x149'))] = _0x4bcc25[0x6];
        this[_0x2d01('0x60')] = _0x4bcc25[0x7];
        this[_0x2d01(_0xfecf('0x98'))] = _0x4bcc25[0x8];
        this[_0x2d01('0x5a')] = _0x4bcc25[0x9];
        this[_0x2d01(_0xfecf('0x99'))]['x'] = _0x4bcc25[0xa];
        this[_0x2d01(_0xfecf('0x99'))]['y'] = _0x4bcc25[0xb];
        this[_0x2d01(_0xfecf('0xff'))] = _0x4bcc25[0xc];
        this['x'] = _0x4bcc25[0xd];
        this['y'] = _0x4bcc25[0xe];
    }
};
VPLAYER[_0x2d01(_0xfecf('0x162'))] = VWSprite;
EXPAND();

})();
