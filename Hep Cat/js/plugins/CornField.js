/*:
* @plugindesc For scripts that the game needs specifically
* @author Allan Manuba
*
* @help This plugin does not provide plugin commands.
*/

var $cornField = null;

const RedState = {
    NONE : 0,
    APPEARING : 1,
    APPEARED : 2,
    DISAPPEARING : 3
}

const ReactionState = {
    BAD: 0,
    NEUTRAL: 1,
    GOOD: 2,
    TO_BAD: 3,
    TO_NEUTRAL: 4,
    TO_GOOD: 5
}

class CornField {
    constructor() {
        this.initialize();
    }

    initialize() {
        $pictureManagerCorn.initialize();

        this.reactionState = ReactionState.NEUTRAL;
        this.reaction = new PictureCorn(this.get_picNeut());
        this.reaction.setZ(-60);
        this.reaction.show(0, 0, 0, 85, 85, 255, 0);

        this.base = new PictureCorn("00_Rhythm_Game_Base");
        this.base.setZ(-30);
        this.base.show(0, -16, 0, 85, 85, 255, 0);

        var mult = this.hp_lowerLimit() / 16;
        var lv_height = 198 * 0.85;
        var lv_yScale = 85 * mult;
        var lv_yPos = 600 - (lv_height * mult);
        this.leveler = new PictureCorn("00_Rhythm_Game_Leveler");
        this.leveler.setZ(-25);
        this.leveler.show(0, 14, lv_yPos, 85, lv_yScale, 255, 0);

        // Red FPS thingy
        this.redState = RedState.NONE;
        this.redOpacity = 0;

        this._redthingy = new PictureCorn("redstuff");
        this._redthingy.setZ(0);
        this._redthingy.show(0, 0, 0, 85, 85, 0, 0);

        this.setUpGameBase();
    }

    setUpProgressBar() {
        this._progressBarBottom = new PictureCorn("Rhythm-Game_Progress-Bar_BOTTOM");
        this._progressBarBottom.setZ(0)
        this._progressBarBottom.show(0, 0, 373, 85, 50, 255, BLEND_MODE.NORMAL);

        this._progressBarTop = new PictureCorn("Rhythm-Game_Progress-Bar_TOP");
        this._progressBarTop.setZ(10)
        this._progressBarTop.show(0, 0, 373, 85, 50, 255, BLEND_MODE.NORMAL);
    }

    setUpGameBase() {
        this.base = new PictureCorn("00_Rhythm_Game_Base");
        this.base.setZ(-30);
        this.base.show(0, -16, 0, 85, 85, 255, 0);

        this.top = new PictureCorn("00_Rhythm_Game_Top");
        this.top.setZ(-28);
        this.top.show(0, -16, 0, 85, 85, 255, 0);

        this.leveler = new PictureCorn("00_Rhythm_Game_Leveler");
        this.leveler.setZ(-29);
        this.leveler.show(0, -16, CornField.leveler_lowest, 85, 85, 255, 0);
    }

    updatePicture() {
        const val = $tscManager._scoreManager.get_mistakeTracker();
        const deltaTime = $timerCorn.getDeltaTime();
        const time = $timerCorn.getTime();

        if (val < this.hp_lowerLimit()) {

            if (this.redState !== RedState.APPEARED) {
                this.redState = RedState.APPEARING;
            }
            if (this.reactionState !== ReactionState.BAD
                && this.reactionState !== ReactionState.TO_BAD) {
                this.reactionState = ReactionState.TO_BAD;
                this.animStartTime = time;
            }

            // this.reaction.setPictureName(this.get_picBad());
        } else if (val < this.hp_upperLimit()) {

            if (this.redState !== RedState.NONE) {
                this.redState = RedState.DISAPPEARING;
            }
            if (this.reactionState !== ReactionState.NEUTRAL
                && this.reactionState !== ReactionState.TO_NEUTRAL) {
                this.reactionState = ReactionState.TO_NEUTRAL;
                this.animStartTime = time;
            }

            //this.reaction.setPictureName(this.get_picNeut());
        } else {

            if (this.reactionState !== ReactionState.GOOD
                && this.reactionState !== ReactionState.TO_GOOD) {
                this.reactionState = ReactionState.TO_GOOD;
                this.animStartTime = time;
            }
            // this.reaction.setPictureName(this.get_picGood());
        }

        switch (this.redState) {
            case RedState.DISAPPEARING:
                this.redOpacity -= CornField.OPACITY_STEP * deltaTime;
                if (this.redOpacity <= 0) {
                    this.redOpacity = 0;
                }
                this._redthingy.setOpacity(this.redOpacity);
                break;
            case RedState.APPEARING:
                this.redOpacity += CornField.OPACITY_STEP * deltaTime;
                if (this.redOpacity >= CornField.RED_MAX_OPACITY) {
                    this.redOpacity = CornField.RED_MAX_OPACITY;
                }
                this._redthingy.setOpacity(this.redOpacity);
                break;
            case RedState.APPEARED:
            case RedState.NONE:
                // do nothing
                break;
            default:
                console.warn("Unknown red state: " + this.redState);
                break;
        }

        const elapsedTime = time - this.animStartTime;
        var scaleY = 85;
        var x = 0;

        switch (this.reactionState) {
            case ReactionState.TO_BAD:
                x = Math.abs(45 * Math.sin(elapsedTime * CornField.OPT_SHAKE_FACTOR));

                if (elapsedTime < 150) {

                    // 0 - 300 squish
                    scaleY = CornField.BAD_OFFSET + (150 - elapsedTime) * CornField.OPT_BAD_COMP;

                } else if (elapsedTime < 250) {

                    // 300 - 500 expand
                    const newElapsedTime = elapsedTime - 150;
                    this.reaction.setPictureName(this.get_picBad());
                    scaleY = CornField.BAD_OFFSET + (newElapsedTime * CornField.OPT_AFTER_SQUISH);

                } else {

                    x = 0;
                    this.reactionState = ReactionState.BAD;
                    this.reaction.setPictureName(this.get_picBad());

                }

                break;

            case ReactionState.TO_NEUTRAL:
                if (elapsedTime < 250) {
                    const toneVal = (elapsedTime / 5) * (2 - (elapsedTime / 125));
                    this.reaction._tone = [toneVal, toneVal, toneVal, toneVal];
                } else {
                    this.reactionState = ReactionState.NEUTRAL;
                    this.reaction._tone = [0, 0, 0, 0];
                }

                if (elapsedTime > 125) {
                    this.reaction.setPictureName(this.get_picNeut());
                }
                break;

            case ReactionState.TO_GOOD:
                if (elapsedTime < 150) {

                    // 0 - 300 squish
                    scaleY = 85 - CornField.GOOD_DIFF + (150 - elapsedTime) * CornField.OPT_GOOD_FACTOR1;

                } else if (elapsedTime < 250) {

                    // 300 - 500 expand
                    const newElapsedTime = elapsedTime - 150;
                    this.reaction.setPictureName(this.get_picGood());
                    scaleY = 85 - CornField.GOOD_DIFF + (newElapsedTime * CornField.OPT_GOOD_FACTOR2);

                } else {

                    this.reactionState = ReactionState.GOOD;
                    this.reaction.setPictureName(this.get_picGood());

                }
                break;

            case ReactionState.BAD:
            case ReactionState.NEUTRAL:
            case ReactionState.GOOD:
                // do nothing
                break;

            default:
                console.warn("Unknown reaction state: " + this.reactionState);
                break;
        }

        this.reaction.setScaleSpec(85, scaleY);
        this.reaction.setY(740 * (85 - scaleY) / 150);
        this.reaction.setX(x);
    }

    

    get_picBad() {
        return "Battleback_Tutorial_Bad";
    }

    get_picNeut() {
        return "Battleback_Tutorial_Neutral";
    }

    get_picGood() {
        return "Battleback_Tutorial_Good";
    }

    hp_lowerLimit() {
        return 3;
    }
    
    hp_upperLimit() {
        return 12;
    }

    /**
     * 
     * @param {float} factor is a float value from 0.0 to 1.0
     * indicating the difficulty
     */
    adjustDifficulty(factor) {
        if (factor < 0) {
            factor = 0;
        } else if (factor > 1) {
            factor = 1;
        }

        var multiplier = 1 - factor;
        this.leveler.setY(multiplier * CornField.leveler_lowest);
    }
}

CornField.leveler_lowest = 168;

CornField.OPACITY_STEP = 0.4;
CornField.RED_MAX_OPACITY = 200;

CornField.GOOD_DIFF = 5;
CornField.OPT_GOOD_FACTOR1 = CornField.GOOD_DIFF / 150;
CornField.OPT_GOOD_FACTOR2 = CornField.GOOD_DIFF / 100;

CornField.BAD_DIFF = 5;
CornField.BAD_OFFSET = 85 + CornField.BAD_DIFF;
CornField.OPT_SHAKE_FACTOR = Math.PI / 83;
CornField.OPT_AFTER_SQUISH = CornField.BAD_DIFF / 100;
CornField.OPT_BAD_COMP = CornField.BAD_DIFF / 150;

class CornFieldBackroom extends CornField {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        const blackScreen = new PictureCorn("BlackScreen");
        this.background = new PictureCorn("Battleback_Tutorial_Background")

        blackScreen.setZ(1000);
        blackScreen.show(0, 0, 0, 100, 100, 255, 0);

        this.background.setZ(-100);
        this.background.show(0, 0, 0, 85, 85, 255, 0);

        this.setUpProgressBar();

        const startTime = (new Date()).getTime();
        const duration = 45 / 60 * 1000;
        const intervalId = setInterval(function () {
            const elapsedTime = (new Date()).getTime() - startTime;
            const newVal = 255 - (255 * (elapsedTime / duration));
            blackScreen.setOpacity(newVal);
        }, 1000 / 60);

        setTimeout(function () {
            clearInterval(intervalId);
            blackScreen.erase();
        }, duration);
    }

    get_picBad() {
        return "Battleback_Tutorial_Bad";
    }

    get_picNeut() {
        return "Battleback_Tutorial_Neutral";
    }

    get_picGood() {
        return "Battleback_Tutorial_Good";
    }

    static setup() {
        $cornField = new CornFieldBackroom();
    }

    hp_lowerLimit() {
        return 3;
    }
    
    hp_upperLimit() {
        return 12;
    }
}

class CornFieldSpeakeasy extends CornField {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        this.background = new PictureCorn("02_Battleback_Speakeasy_Background");
        const blackScreen = new PictureCorn("BlackScreen");

        blackScreen.setZ(1000);
        blackScreen.show(0, 0, 0, 100, 100, 255, 0);

        this.background.setZ(-100);
        this.background.show(0, 0, 0, 85, 85, 255, 0);

        this.setUpProgressBar();

        const startTime = (new Date()).getTime();
        const duration = 45 / 60 * 1000;
        const intervalId = setInterval(function () {
            const elapsedTime = (new Date()).getTime() - startTime;
            const newVal = 255 - (255 * (elapsedTime / duration));
            blackScreen.setOpacity(newVal);
        }, 1000 / 60);

        setTimeout(function () {
            clearInterval(intervalId);
            blackScreen.erase();
        }, duration);
    }

    get_picBad() {
        return "02_Battleback_Speakeasy_Bad";
    }

    get_picNeut() {
        return "02_Battleback_Speakeasy_Neutral";
    }

    get_picGood() {
        return "02_Battleback_Speakeasy_Good";
    }

    hp_lowerLimit() {
        return 5;
    }
    
    hp_upperLimit() {
        return 13;
    }

    static setup() {
        $cornField = new CornFieldSpeakeasy();
    }
}

class CornFieldGreenroom extends CornField {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        this.background = new PictureCorn("03_Battleback_Greenroom_Background");
        const blackScreen = new PictureCorn("BlackScreen");

        blackScreen.setZ(1000);
        blackScreen.show(0, 0, 0, 100, 100, 255, 0);

        this.background.setZ(-100);
        this.background.show(0, 0, 0, 85, 85, 255, 0);

        this.setUpProgressBar();

        const startTime = (new Date()).getTime();
        const duration = 45 / 60 * 1000;
        const intervalId = setInterval(function () {
            const elapsedTime = (new Date()).getTime() - startTime;
            const newVal = 255 - (255 * (elapsedTime / duration));
            blackScreen.setOpacity(newVal);
        }, 1000 / 60);

        setTimeout(function () {
            clearInterval(intervalId);
            blackScreen.erase();
        }, duration);
    }

    get_picBad() {
        return "03_Battleback_Greenroom_Bad";
    }

    get_picNeut() {
        return "03_Battleback_Greenroom_Neutral";
    }

    get_picGood() {
        return "03_Battleback_Greenroom_Good";
    }

    hp_lowerLimit() {
        return 9;
    }
    
    hp_upperLimit() {
        return 14;
    }

    static setup() {
        $cornField = new CornFieldGreenroom();
    }
}

class CornFieldStage extends CornField {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        this.background = new PictureCorn("04_Battleback_Stage_Background");
        const blackScreen = new PictureCorn("BlackScreen");

        blackScreen.setZ(1000);
        blackScreen.show(0, 0, 0, 100, 100, 255, 0);

        this.background.setZ(-100);
        this.background.show(0, 0, 0, 85, 85, 255, 0);

        this.setUpProgressBar();

        const startTime = (new Date()).getTime();
        const duration = 45 / 60 * 1000;
        const intervalId = setInterval(function () {
            const elapsedTime = (new Date()).getTime() - startTime;
            const newVal = 255 - (255 * (elapsedTime / duration));
            blackScreen.setOpacity(newVal);
        }, 1000 / 60);

        setTimeout(function () {
            clearInterval(intervalId);
            blackScreen.erase();
        }, duration);
    }

    get_picBad() {
        return "04_Battleback_Stage_Bad";
    }

    get_picNeut() {
        return "04_Battleback_Stage_Neutral";
    }

    get_picGood() {
        return "04_Battleback_Stage_Good";
    }

    hp_lowerLimit() {
        return 11;
    }
    
    hp_upperLimit() {
        return 16;
    }

    static setup() {
        $cornField = new CornFieldStage();
    }
}