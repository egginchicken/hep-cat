/*:
 *@plugindesc GreenCheesecake is a better audio manager lol
 * @author Allan Manuba
 *
 * @help This plugin does not provide plugin commands.
 * todo
 */

class GlobalCheesecake {
    constructor() {
        this.initialize();
    }

    initialize() {
        this.cheesecakeList = [];
    }

    stopAll() {
        this.cheesecakeList.forEach(item => {
            item.stop();
        });
    }

    addCheesecake(item) {
        this.cheesecakeList.push(item);
    }

    erase(cheesecake) {
        cheesecake.stop();
        ArrayUtil.removeItem(this.cheesecakeList, cheesecake);
    }

    eraseAll() {
        this.stopAll();
        this.initialize();
    }
}

$globalCheesecake = new GlobalCheesecake();

/**
 * Other functions that are in parent:
 * stop()
 * fadeIn(duration)
 * fadeOut(duration)
 * seek() // gets the position
 * play(loop, offset)
 * isPlaying()
 * */
class GreenCheesecake extends WebAudio {
    constructor(url) {
        super(url);

        if (url.startsWith("audio/bgm/")) {
            this.configVolume = AudioManager._bgmVolume / 100;
        } else if (url.startsWith("audio/bgs/")) {
            this.configVolume = AudioManager._bgsVolume / 100;
        } else if (url.startsWith("audio/me/")) {
            this.configVolume = AudioManager._meVolume / 100;
        } else if (url.startsWith("audio/se/")) {
            this.configVolume = AudioManager._seVolume / 100;
        } else {
            this.configVolume = 0.5;
            console.error("Invalid folder: might have occured beforehand: " + url);
        }

        this._autoPlay = false;
        this.loop = false;
        this.offset = 0;
        $globalCheesecake.addCheesecake(this);
    }

    // #region setters and getters
    setLoop(shouldLoop) {
        this.loop = shouldLoop;
    }

    setOffset(offset) {
        this.offset = offset;
    }

    setVolume(volume) {
        this.volume = this.configVolume * volume;
    }
    // #endregion setters and getters

    addListener(listener) {
        if (typeof listener.informLoadComplete === "function") {
            if (this.isReady()) {
                listener.informLoadComplete();
            } else if (WebAudio._context) {
                this.addLoadListener(function () {
                    listener.informLoadComplete();
                }.bind(this));
            }
        } else {
            console.error("listener does not have informLoadComplete function");
        }
    }

    static audioFileExt() {
        if (WebAudio.canPlayOgg() && !Utils.isMobileDevice()) {
            return ".ogg";
        } else {
            return ".m4a";
        }
    }

    startPlaying() {
        if (this.isReady()) {
            this.hasEndedVal = false;
            this.play(this.loop, this.offset);
        }
    }

    hasEnded() {
        return this.hasEndedVal;
    }

    erase() {
        $globalCheesecake.erase(this);
    }

    static encode(songName, folder) {
        const ext = GreenCheesecake.audioFileExt();
        return GreenCheesecake.path + folder + "/" + encodeURIComponent(songName) + ext;
    }
}

GreenCheesecake.prototype._createEndTimer = function () {
    if (this._sourceNode && !this._sourceNode.loop) {
        var endTime = this._startTime + this._totalTime / this._pitch;
        var delay = endTime - WebAudio._context.currentTime;
        this._endTimer = setTimeout(function () {
            this.stop();
            this.hasEndedVal = true;
        }.bind(this), delay * 1000);
    }
};

GreenCheesecake.path = "audio/";

/**
 * Synchronizes the two song for beatmap and full
 * */
class KitchenSync {
    constructor() {
        this.initialize();
    }

    initialize() {
        this.listenerList = [];
        this.bufferList = [];
        this.isLoadingBuffer = false;
        this.bufferCompleteCount = 0;
    }

    addListener(listener) {
        if (typeof listener.informLoadComplete === "function") {
            this.listenerList.push(listener);
        } else {
            console.error("Listener has no informLoadComplete: " + listener);
        }
    }

    addSong(songName, folder) {
        if (!this.isLoadingBuffer) {
            const url = GreenCheesecake.encode(songName, folder);
            const buffer = new GreenCheesecake(url);
            buffer.addListener(this);
            this.bufferList.push(buffer);
            return buffer;
        } else {
            console.error("Cannot add song after starting songs");
            return null;
        }
    }

    loadBufferList() {
        this.isLoadingBuffer = true;
        if (this.isReady()) {
            this.informAllListeners();
        }
    }

    informLoadComplete() {
        this.bufferCompleteCount += 1;

        if (this.isLoadingBuffer && this.isReady()) {
            this.informAllListeners();
        }
    }

    informAllListeners() {
        this.listenerList.forEach(listener => {
            listener.informLoadComplete();
        });

        this.listenerList = [];
    }

    isReady() {
        return this.isLoadingBuffer && this.bufferList.length === this.bufferCompleteCount;
    }

    startPlaying() {
        if (this.isReady()) {
            this.bufferList.forEach(buffer => {
                buffer.startPlaying();
            });
        } else {
            console.error("Songs not yet ready");
        }
    }

    clear() {
        this.bufferList.forEach(buffer => {
            buffer.erase();
        });
        this.initialize();
    }
}