/*:
 *@plugindesc Debugs all Corn related plugins
 * @author Allan Manuba
 *
 * @help This plugin does not provide plugin commands.
 * HOW TO USE THIS:
 * Run CornPesticide.run();
 */

class CornPesticide {
    static run() {
        if (true) { MusicTimerTester.run(); }
    }
}

class MusicTimerTester {
    static run() {
        var tester = new MusicTimerTester();
        tester.testGetBgmBuffer();
        tester.testIsBgmPrepared();
        tester.testGetTime();

        setTimeout(function () {
            MusicTimerTester.run2(tester);
        }, 7000);
    }

    static run2(tester) {
        setTimeout(tester.testPause, 1000);
        setTimeout(tester.testResume, 2000);
        setTimeout(tester.testEnding, 3000);
    }

    static testEndingStatic() {
        var mTimer = new MusicTimerTester();
        mTimer.testEnding();
    }

    testGetBgmBuffer() {
        console.log(MusicTimerHelper.getBgmBuffer());
    }

    testIsBgmPrepared() {
        console.log(MusicTimerHelper.isBgmPrepared());

        const bgm = {
            name: "Dreamy",
            volume: AudioEffectManager.MASTER_VOLUME,
            pitch: 100,
            pan: 0
        };
        MusicTimerHelper.playBgm(bgm);
        console.log(MusicTimerHelper.getBgmBuffer());

        console.log(`Start time: ${(new Date).getTime()}`);

        while (MusicTimerHelper.isBgmPrepared()) {
            setTimeout(function () { }, 1);
        }

        console.log(`End time: ${(new Date).getTime()}`);
    }

    testGetTime() {
        const maxCount = 5;
        var count = 1;
        var id = setInterval(function () {
            console.log(`Song time: ${MusicTimerHelper.seek()}`);
            count++;
            if (count > maxCount) {
                clearInterval(id);
            }
        }, 1000);
    }

    testPause() {
        MusicTimerHelper.pauseBgm();
        console.log("Pause");
        console.log(`Status: ${MusicTimerHelper.isBgmPrepared()}`);
    }

    testResume() {
        MusicTimerHelper.resumeBgm();
        console.log(`Resume at ${MusicTimerHelper.seek()}`);
    }

    testEnding() {
        console.log(`Entering ending test: !`);
        const bgm = {
            name: "Dreamy",
            volume: AudioEffectManager.MASTER_VOLUME,
            pitch: 100,
            pan: 0
        };
        //MusicTimer.pauseBgm();
        MusicTimerHelper.playBgm(bgm, 65);
        console.log(MusicTimerHelper.getBgmBuffer());

        setTimeout(function () {
            const id = setInterval(function () {
                var flag = MusicTimerHelper.hasSongEnded();
                if (flag) {
                    console.log("Success");
                    clearInterval(id);
                }
            }, 1000);
        }, 2000);
    }
}

class GreenCheesecakeTester {
    constructor() {
        this.cheesecake = null;
    }

    static run() {
        var cheesecake = new GreenCheesecake("Cloudy1Full");
        var tester = new GreenCheesecakeTester();
        tester.setCheesecake(cheesecake);
        cheesecake.addListener(tester);
    }

    setCheesecake(cheesecake) {
        this.cheesecake = cheesecake;
    }

    informLoadComplete() {
        this.cheesecake.startPlaying();
    }
}

class KitchenSyncTester {
    constructor() {
        this.kitchenSync = null;
        this.full = null;
    }

    static run() {
        console.log("Confirm wazokwski");
        var tester = new KitchenSyncTester();
        var kitchenSync = new KitchenSync();
        tester.setKitchenSync(kitchenSync);
        tester.full = kitchenSync.addSong("Cloudy3Background");
        kitchenSync.addSong("Cloudy3Beatmap");
        kitchenSync.loadBufferList();
    }

    setKitchenSync(kitchenSync) {
        this.kitchenSync = kitchenSync;
        this.kitchenSync.addListener(this);
    }

    informLoadComplete() {
        this.kitchenSync.startPlaying();
        var fullSong = this.full;
    }
}