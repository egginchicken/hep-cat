/*:
 * @plugindesc Helpful utility tools.
 * @author Allan Manuba
 *
 * @help This plugin does not provide plugin commands.
 *
 */

 /**
  * Pool pictures for a certain scene???
  */
 
class Daikon {
    constructor() {
        throw "Static class. Don't initialize. Use Corn.function() instead.";
    }
}

/**
 * Returns a shuffled array.
 * CAUTION: It also shuffles the original array
 * Example:
 * var tmpArr = [0, 1, 2, 3, 4];
 * Daikon.shuffle(tmpArr);
 */
Daikon.shuffle = function() {
    // from https://stackoverflow.com/a/2450976
    // by CoolAJ86
    var currentIndex = array.length;
    var temporaryValue;
    var randomIndex;
    while (0 !== currentIndex) { 
        randomIndex = Math.floor(Math.random() * currentIndex); 
        currentIndex -= 1; temporaryValue = array[currentIndex]; 
        array[currentIndex] = array[randomIndex]; 
        array[randomIndex] = temporaryValue;
    }
    
    return array;
}

Daikon.randomInt = function (lowerBound, higherBound) {
    const diff = higherBound - lowerBound + 1;
    const newOffset = Math.floor(Math.random() * diff);
    return lowerBound + newOffset;
}

Daikon.randomFloat = function (lowerBound, higherBound) {
    const diff = higherBound - lowerBound;
    const newOffset = Math.random() * diff;
    return lowerBound + newOffset;
}

/**
 * returns true if the player is within the npc's line of sight
 * Example:
 * Daikon.isPlayerVisible(this._eventId);
 */
Daikon.isPlayerVisible = function(eventNPC) {
    var npc = $gameMap._events[eventNPC._eventId];
    var dir = npc.direction();
    var nx = npc._x;
    var ny = npc._y;
    var px = $gamePlayer._x;
    var py = $gamePlayer._y;
    if (
        (dir == directionsCorn.UP && nx == px && py < ny) ||
        (dir == directionsCorn.DOWN && nx == px && py > ny) ||
        (dir == directionsCorn.LEFT && ny == py && px < nx) ||
        (dir == directionsCorn.RIGHT && ny == py && px > nx)
    )
    {
        return true;
    }
    else
    {
        return false;
    }
}

const $DAIKON_EPSILON = 0.00001;

Daikon.floatCompare = function (f1, f2) {
    return Math.abs(f1 - f2) < $DAIKON_EPSILON;
}

/**
 * Returns the time it takes to compute cos with random values
 * iterated 10 000 times. This value is multiplied by 1000. */
Daikon.testMachinePerformance = function () {
    const startMarker = performance.now();

    var i;
    var x;
    for (i = 0; i < 10001; i++) {
        x = Math.cos(Math.random() * 1000);
    }

    const endMarker = performance.now();

    return 1000 * (endMarker - startMarker);
}

// #region Utilities

/**
 * JSONUtilCorn
 *
 * Based on https://www.w3schools.com/nodejs/nodejs_filesystem.asp
 */
const CORN_DATA_TYPE = {
    MAP: 0,
    ANIMATION: 1,
    GODOT_TREE: 2
}

class JSONUtilDaikon {
    constructor() {
        throw new Error('This is a static class');
    }

    static readData(name, dataType, receiver) {
        var path;

        switch (dataType) {
            case CORN_DATA_TYPE.MAP:
                path = "cornflour/maps/";
                path += encodeURIComponent(name) + ".json";
                break;
            case CORN_DATA_TYPE.ANIMATION:
                path = "cornflour/animations/";
                path += encodeURIComponent(name) + ".json";
                break;
            case CORN_DATA_TYPE.GODOT_TREE:
                path = "cornflour/godot_tree/";
                path += encodeURIComponent(name) + ".tscn";
                break;
            default:
                throw "Reading unknown type";
                break;
        }
        JSONUtilDaikon.readJSON(path, receiver);
    }

    static readMap(mapName, receiver) {
        // todo: change
        JSONUtilDaikon.readData(mapName, CORN_DATA_TYPE.MAP, receiver);
    }

    static readGodotTree(godotTreeName, receiver) {
        JSONUtilDaikon.readData(godotTreeName, CORN_DATA_TYPE.GODOT_TREE, receiver);
    }

    /**
     * readMap
     * @param receiver  an object that should have receiveData(data) function
     *                  and is aware how to handle this JSON data
     */
    static readJSON(path, receiver) {
        if (typeof receiver.receiveData !== "function") {
            console.error(receiver);
            throw "Receiver does not have receiveData";
        }

        var xhr = new XMLHttpRequest();
        xhr.open('GET', path);
        xhr.overrideMimeType('application/json');
        xhr.onload = function () {
            if (xhr.status < 400) {
                receiver.receiveData(xhr.responseText);
            }
        };
        xhr.onerror = this._mapLoader || function () {
            console.error("Error loading file: " + path);
        };
        xhr.send();
    }

    // todo: fix this; it won't work on deploy
    static writeJSON(filename, obj) {
        // from https://www.w3schools.com/nodejs/nodejs_filesystem.asp
        var str = JSON.stringify(beatData);

        var fs = require('fs');
        var path = "cornflour/data/" + encodeURIComponent(filename) + ".json";
        fs.writeFile(path, beatData, function (err) {
            if (err) {
                console.error(err);
            }
        });
    }
}

class ArrayUtil {
    constructor() {
        throw "This is a static class";
    }

    static removeItem(array, item) {
        const ind = array.indexOf(item);

        if (ind !== -1) {
            array.splice(ind, 1);
        } else {
            console.warn("Item not found in array");
        }
    }
}

class VectorUtil {
    static interpolate(x1, x2, x3, y1, y3) {
        return ((x2 - x1) * (y3 - y1) / (x3 - x1)) + y1;
    }

    /**
     * Takes absolute value of vectorArrStep
     * @param {any} vectorArrCurrent
     * @param {any} vectorArrTarget
     * @param {any} vectorArrStep
     */
    static moveTowards(vectorArrCurrent, vectorArrTarget, vectorArrStep) {
        var idx;
        for (idx = 0; idx < 2; idx++) {
            if (Daikon.floatCompare(vectorArrCurrent[idx], vectorArrTarget[idx])) {
                // if equal don't do anything
            }
            else if (vectorArrTarget[idx] > vectorArrCurrent[idx]) {
                // if target is far ahead, move forward with the step
                // if exceed value, set value to equal
                vectorArrCurrent[idx] += Math.abs(vectorArrStep[idx]);

                if (vectorArrCurrent[idx] >= vectorArrTarget[idx]) {
                    vectorArrCurrent[idx] = vectorArrTarget[idx];
                }
            }
            else
            {
                // if target is far behind, move backward with the step
                // if exceed value, set value to equal
                vectorArrCurrent[idx] -= Math.abs(vectorArrStep[idx]);

                if (vectorArrCurrent[idx] <= vectorArrTarget[idx]) {
                    vectorArrCurrent[idx] = vectorArrTarget[idx];
                }
            }
        }
        return vectorArrCurrent;
    }
}

var $cornKeyCodeDown = 0;
var $cornKeyCodeUp = 0;

window.addEventListener("keydown", function (event) {
    $cornKeyCodeDown = event.keyCode;
    $cornKeyCodeUp = 0;
});

window.addEventListener("keyup", function (event) {
    $cornKeyCodeDown = 0;
    $cornKeyCodeUp = event.keyCode;
});
// #endregion Utilities