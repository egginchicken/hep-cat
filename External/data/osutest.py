import math
import json
import jsonpickle

# change this variables:
filename_in = "Gurenge.txt"
filename_out = "map_demo2.json"

class Beat:
    def __init__(self, line):
        # default values for now
        self._type = 0
        self._tolerance = [
            200,
            500,
            1000
        ]
        self._overdueTime = 1200
        self._tolerableDiff = 1000
        self._pictureName = "BeatCorn"
        self._specialMessage = ""

        # parse
        arr = line.split(",")
        self._time = arr[(2)]

        yNum = math.floor(int(arr[1])/80)

        if (yNum < 0):
            yNum = 0
        elif (yNum > 4):
            yNum = 4

        self._modifiers = {}
        self._modifiers["HITMARK_Y_INDEX"] = yNum

class Map:
    def __init__(self, f):
        # default values
        self._firstBeatOffset = 0
        self._queue = []
        self._difficulty = 0
        self._passRate = [
                0
            ]
        self._screenTime = 5000
        self._hitmarkX = 96

        beat_flag = False
        
        for line in f:
            if beat_flag and line.strip() != "":
                self._queue.append(Beat(line))
            elif "AudioFilename:" in line:
                self._audioFilename = line.split()[1]
            elif "Title:" in line:
                self._title = ":".join(line.split(":")[1:])
            elif "Artist:" in line:
                self._artist = ":".join(line.split(":")[1:])

            if "[HitObjects]" in line:
                beat_flag = True

class OldCode:
    def test():
        with open("magicalmixer.txt", "r") as f:
            flag = False
            for line in f:
                if flag and line.strip() != "":
                    infoList.append(Info(line))

                if "[HitObjects]" in line:
                    flag = True

        with open("demo1.json", "w") as f:
            f.write("[\n")
            isFirst = True
            for info in infoList:
                if isFirst:
                    isFirst = False
                else:
                    f.write(",\n")
        
                f.write("{{\"_pictureName\":\"BeatCorn\",\"_type\":0,\"_time\":{0},\"_tolerance\":[200,500,1000],\"_overdueTime\":5200,\"_toleableDiff\":1000,\"_modifier\":[{1}],\"_picture\":null,\"_message\":0}}".format(info.time, info.yNum))

            f.write("]")

def parse(filename_in, filename_out):
    map = None

    with open(filename_in, "r") as f:
        map = Map(f)

    with open(filename_out, "w") as f:
        f.write(jsonpickle.encode(map))

parse(filename_in, filename_out)