/*:
* @plugindesc For rhythm system. Requires PrettyCorn.js
* @author Allan Manuba
*
* @help This plugin does not provide plugin commands.
* HOW TO USE THIS:
* 1. Create an event that triggers the rhythm system
* 2. Add Script with "CornFlour.initialize("name of json without .json");"
* 3. Add Script that adds your listeners. Make sure they have receiveMessage(beat) as a function inside.
*    You can do it like this:
*    // in the script:
*    ...
*    class UniqueClassName {
*          ...
*          receiveMessage(beat) {
*              // returns Excellent, Good, Okay, etc...
*              var message = beat.getMessage();
*
*              // returns a special key
*              // if it does not exist, it returns null
*              // console should warn you
*              var specialMessage = beat.getSpecialMessage("KEY");
*
*              ... // do stuff here like
*
*              if (message == messaegeCorn.EXCELLENT) {
*                  makeExcellentSounds();
*              }
*
*              ...
*          }
*    }
*    To add them, call CornFlour.registerLsitener(yourClassInstance);
* 4. Wait for data to load. You can use a loop with a "Wait : 1 frame" and 
*    an "If : Script : CornFlour.isPrepared(); Break Loop"
* 5. Start the game with "CornFlour.start()"
* 6. Create a loop in the event
* 7. In the loop, call "Script : CornFlour.update();" AND put "Wait : 1 frame"
* 8. Break loop by checking "CornFlour.isGameOver();"
* 9. When exiting the game mode do cleanup using CornFlour.clear();"
*
* COMMON ERRORS:
* 1. Spelling CornFlour as Cornflour
* 2. Putting an extension file such as ".json" on most functions that require a filename input
*    this applies to CornFlour.initialize("filenamehere")
*
* CHECK OUT:
* 1. CornFlour class is mostly what you'll be interacting with
* 2. Check out static ListenerSample() function in CornFlour class if you want to see a sample code of how
*    how to interact with CornFlour // todo
* 3. TSCManager if you want to see how to interact with the game more directly
* 4. TimedSystemCorn if you REALLY want to see how the interactions happen
*
* todo:
 * fix hold animation
 * check if on time
*/

// check if PrettyCorn plugin is present
'use strict';
var pluginInd;
var isPluginFound = false;
for (pluginInd = 0; pluginInd < $plugins.length; pluginInd++) {
    if ($plugins[pluginInd].name == "PrettyCorn") {
        isPluginFound = true;
        break;
    }
}

if (!isPluginFound) {
    throw "CornFlour.js plugin requires PrettyCorn.js plugin to work properly.\nYou might have to restart RPGMakerMV to reload plugins."
}

// BUG: possible concurrency issue in map between TSCVisualizer and TimedSystemCorn ://
// let's find out XD
// concurrency issue with the usage of PictureManager too ://

//=============================================================================
// CornFlour.js
//=============================================================================

//-----------------------------------------------------------------------------
// Constants
//
// Constants go here, all should be suffixed with corn to avoid conflicts

var DEF_SCREEN_TIME = 5000; // 5 seconds

class ScreenCorn {
    constructor() {
        throw new Error('This is a static class');
    }
}

ScreenCorn.SCREEN_WIDTH = 816;
ScreenCorn.SCREEN_HEIGHT = 624;
ScreenCorn.OFFSCREEN_OFFSET = 64;

const BEAT_CORN_MAP_ID = 6; // may change...

/**
 * CornFlour
 * This is the class you interact with outside of this script
 * */
class CornFlour {
    constructor() {
        throw "Static class: Don't instantiate"
    }

    static setAutoPlay(shouldAutoPlay) {
        CornFlour.shouldAutoPlay = shouldAutoPlay;
    }

    static initialize(filename) {
        CornFlour.pass = false;
        CornFlour.lightingPref = ConfigManager.TxLighting;
        ConfigManager.TxLighting = false;

        // init
        $tscManager = new TSCManager();

        // other modifiers
        CornFlour.setAutoPlay($gameSwitches.value(39));

        if (CornFlour.shouldAutoPlay) {
            CornFlour.autoPlayer = new BeatAutoPlayer();
        }

        $tscManager.loadMap(filename);
    }

    static start() {
        if ($tscManager != null) {
            $tscManager.start();
        } else {
            console.warn("You did not initialize CornFlour");
        }

        $backgroundBuffer = null;
        $preferredOffset = 0;
        $timerCorn = new MusicTimerCorn();
        $kitchenSync = new KitchenSync();
    }

    /**
     * Returns true if manager can now start playing everything
     * */
    static isPrepared() {
        return $tscManager.isPrepared();
    }

    static clear() {
        CornFlour.setAutoPlay(false);

        if ($tscManager != null) {
            CornFlour.previousScore = CornFlour.getScoreRate();
            $tscManager.clear();
            $tscManager = null;
        }

        CornFlour.autoPlayer = null;

        if ($kitchenSync != null) {
            $kitchenSync.clear();
        }

        if ($pictureManagerCorn != null) {
            $pictureManagerCorn.clear();
        }

        /** Enable WASD */
        // Input.keyMapper[87] = "up";
        // Input.keyMapper[83] = "down";
        // Input.keyMapper[65] = "left";
        // Input.keyMapper[68] = "right";
    }

    static update() {
        var movement = null;

        if (Input.isTriggered(BeatCorn.DIRECTION.left)) {
            movement = 37;
        } else if (Input.isTriggered(BeatCorn.DIRECTION.up)) {
            movement = 38;
        } else if (Input.isTriggered(BeatCorn.DIRECTION.right)) {
            movement = 39;
        } else if (Input.isTriggered(BeatCorn.DIRECTION.down)) {
            movement = 40;
        }

        if (CornFlour.autoPlayer != null) {
            movement = CornFlour.autoPlayer.evaluate();
        }

        $tscManager.update(movement);
    }

    static isGameOver() {
        if ($tscManager != null) {
            const flag = $tscManager.isDone();

            if (flag) {
                ConfigManager.TxLighting = CornFlour.lightingPref;
                CornFlour.pass = $tscManager.didWePass();
            }

            return flag;
        } else {
            console.warn("Cannot check game over if you haven't initialized CornFlour");
            return false;
        }
    }

    static registerListener(listener) {
        if ($tscManager != null) {
            $tscManager.registerChild(listener);
        } else {
            console.warn("Cannot register listener if you haven't initialized CornFlour");
        }
    }

    static getScoreRate() {
        return $tscManager.getScoreRate();
    }

    static didWePass() {
        return CornFlour.pass;
    }

    static sampleListener() {
        throw "This does not work but the structure works on RPGMaker MV"
        // Checkout Map001's event to see this in action

        // Do step 1:  Create an event that triggers the rhythm system
        // How would you trigger this in RPGMaker MV?

        // This is step 2:
        // Add Script with "CornFlour.play("name of json without .json");"
        // Don't do CornFlour.initialize("map_demo2.json");
        CornFlour.initialize("map_demo2");

        // Step 3: Add Script that adds your listeners. 
        // Make sure they have receiveMessage(beat) as a function inside.
        var yourClassInstance = new BarSampleClass();
        CornFlour.registerListener(yourClassInstance);

        // Step 4: Wait for data to load. You can use a loop with a "Wait : 1 frame" and 
        // an "If : Script : CornFlour.isPrepared(); Break Loop"
        // 1000 / 60 = delay for each frame to make 60 frames per second
        // an alternative is to use the loop, wait, and if statement in RPG Maker MV

        var frameDelay = 1000 / 60;
        while (!CornFlour.isPrepared()) {
            setTimeout(function () { }, frameDelay);
        }

        // Step 5: Start the game with "CornFlour.start()"
        CornFlour.start();

        // Step 6: Create a loop in the event
        // This is how the loop if RPG Maker MV works
        while (true) {
            // Step 7: In the loop, call "Script : CornFlour.update();" AND put "Wait : 1 frame"
            CornFlour.update();
            // setTimeout(function () { }, frameDelay);

            // Step 8: Break loop by checking "CornFlour.isGameOver();"
            if (CornFlour.isGameOver()) {
                break;
            }

            // I put the frame delay or wait over here
            setTimeout(function () { }, frameDelay);
        }

        // Step 9: When exiting the game mode do cleanup using CornFlour.clear();"
        CornFlour.clear();
    }
}

CornFlour.autoPlayer = null;
CornFlour.previousScore = 0;
CornFlour.shouldAutoPlay = false;

// #region Samples

/**
 * BarSampleClass
 * Shows how to make a class that properly interacts with CornFlour
 * Emulates an HPBar
 * */
class BarSampleClass {
    constructor(map) {
        this._maxScore = 0;
        this._currentScore = 0;
    }

    receiveMessage(beat) {
        var message = beat.getMessage();

        switch (message) {
            case messageCorn.START:
                // do nothing
                // initialize everything here
                // you have to add specialMessage somewhere in the code
                this._maxScore = beat.getSpecialMessage("QUEUE_LENGTH");
                break;
            case messageCorn.EXCELLENT:
                this._currentScore = this._currentScore + 3;
                break;
            case messageCorn.GOOD:
                this._currentScore = this._currentScore + 2;
                break;
            case messageCorn.OKAY:
                this._currentScore = this._currentScore + 1;
                break;
            case messageCorn.BAD:
                // maybe put something when something bad happens
                break;
            case messageCorn.MISS:
                // react to a  miss
                break;
            case messageCorn.WAITING:
                // do nothing or maybe something you want?
                break;
            case messageCorn.END:
                // maybe release all assets or show the final score?
                break;
            default:
                break;
        }
    }
}

// #endregion Samples

// #region Cores
/**
 * Timer that every class looks at as reference
 * */
class TimerCorn {
    constructor() {
        // just to make it non-null
        this._deltaTime = 1000 / 60;
        this.offset = -500;
        this.startTime();
        this.updateTime();
    }

    startTime() {
        this._startTime = (new Date()).getTime();
        this._deltaTime = 1000 / 60;
        this._previousTime = 0;
        this._currentTime = 0;
        this.updateTime();
    }

    updateTime() {
        this._previousTime = this._currentTime;
        this._currentTime = (new Date()).getTime();
        this._deltaTime = this._currentTime - this._previousTime;
    }

    getTime() {
        return this._currentTime - this._startTime + this.offset + $preferredOffset;
    }

    getDeltaTime() {
        if (this._deltaTime === 0) {
            this._deltaTime = 1;
        }
        return this._deltaTime;
    }
}

const messageCorn = {
    WAITING: 0,
    EXCELLENT: 1,
    GOOD: 2,
    OKAY: 3,
    MISS: 4,
    BAD: 5,
    START: 6,
    END: 7,
    HOLD_START: 8
}

const beatCornType = {
    REGULAR: 0,
    HOLD: 1
}

/**
 * BeatCorn
 * Holds the information for every beat and how a player should behave
 * The first modifier is where the beat corn is placed on the staff
 */
class BeatCorn {
    constructor() {
        this._pictureName = "";

        this._time = 0;
        this._endTime = 0;

        // todo: change type based on endTime == 0
        this._type = 0;

        this._tolerance = [200, 500, 1000];
        this._overdueTime = 200; // this._tolerance[0];
        this._tolerableDiff = 1000;
        this._modifiers = []; // array of things you might need in the future
        this._picture = null;
        this._message = messageCorn.WAITING;
        this._specialMessage = {};
        this.focusState = FocusState.START;
    }

    static cast(data) {
        const beatCorn = new BeatCorn();

        beatCorn._pictureName = data._pictureName;

        beatCorn._time = data._time;
        beatCorn._endTime = data._endTime;

        // todo: change type based on endTime == 0
        beatCorn._type = data._type;
        beatCorn._tolerance = data._tolerance;

        beatCorn._overdueTime = beatCorn.getTime() + beatCorn._tolerance[beatCorn._tolerance.length - 1];// + beatCorn._tolerance[0];
        beatCorn._tolerableDiff = beatCorn._tolerance[beatCorn._tolerance.length - 1];
        beatCorn._modifiers = data._modifiers; // array of things you might need in the future
        beatCorn.setPicture(beatCorn._pictureName, 10);
        beatCorn._picture.setZ(10);
        beatCorn._message = messageCorn.WAITING;
        beatCorn._specialMessage = {};

        return beatCorn;
    }

    get startTime() {
        return this._startTime;
    }

    multiplyTolerance(factor) {
        var i;
        for (i = 0; i < this._tolerance.length; i++) {
            this._tolerance[i] = this._tolerance[i] * factor;
        }

        this._overdueTime = this.getTime() + this._tolerance[2];
        this._tolerableDiff = this._tolerance[2];
    }

    setPicture(pictureName, z) {
        if (pictureName === "BeatCorn") {

            switch (this.getModifiers(BeatCorn.HITMARK_Y_INDEX)) {
                case 0:
                    this._pictureName = pictureName + "Red";
                    break;
                case 1:
                    this._pictureName = pictureName + "Yellow";
                    break;
                case 2:
                    this._pictureName = pictureName + "Green";
                    break;
                case 3:
                    this._pictureName = pictureName + "Blue";
                    break;
                default:
                    console.warn("Unknown beat index: " + this.getModifiers(BeatCorn.HITMARK_Y_INDEX));
                    this._pictureName = pictureName + "Red";
                    break;
            }
        } else {
            this._pictureName = pictureName;
        }
        this._picture = new PictureCorn(this._pictureName);
        this._picture.setZ(z);
    }

    // todo: remove timer
    react(timer, movement, offset) {
        var diff = this.getTimeDiff($timerCorn, offset);

        if ($timerCorn.getTime() > this._overdueTime) {
            this._message = messageCorn.MISS;
            return this._message;
        } else if (diff > this._tolerableDiff) {
            return messageCorn.WAITING;
        }

        if (movement !== null) {
            if (movement != (37 + this.getModifiers(BeatCorn.HITMARK_Y_INDEX))) {
                this._message = messageCorn.BAD;
            }
            else if (diff < this._tolerance[0]) {
                this._message = messageCorn.EXCELLENT;
            }
            else if (diff < this._tolerance[1]) {
                this._message = messageCorn.GOOD;
            }
            else if (diff < this._tolerance[2]) {
                this._message = messageCorn.OKAY;
            }
            else {
                this._message = messageCorn.WAITING;
            }
        } else {
            this._message = messageCorn.WAITING;
        }

        return this._message;
    }

    // todo: remove timer
    holdReact(timer, offset) {
        var isHold;
        var isRelease;

        if (Input.keyMapper[37] === "") { // wasd
            switch (this.getModifiers(BeatCorn.HITMARK_Y_INDEX)) {
                case 0: // left
                    isHold = $cornKeyCodeDown === 65;
                    isRelease = $cornKeyCodeUp === 65;
                    break;
                case 1: // up
                    isHold = $cornKeyCodeDown === 87;
                    isRelease = $cornKeyCodeUp === 87;
                    break;
                case 2: // right
                    isHold = $cornKeyCodeDown === 68;
                    isRelease = $cornKeyCodeUp === 68;
                    break;
                case 3: // down
                    isHold = $cornKeyCodeDown === 83;
                    isRelease = $cornKeyCodeUp === 83;
                    break;
                default:
                    console.warn("Unknown");
                    break;
            }
        } else {
            // arrow keys
            isHold = $cornKeyCodeDown === (this.getModifiers(BeatCorn.HITMARK_Y_INDEX) + 37);
            isRelease = $cornKeyCodeUp === (this.getModifiers(BeatCorn.HITMARK_Y_INDEX) + 37);
        }

        var diff = this.getTimeDiff($timerCorn, offset);

        if (isRelease) {
            // releasing or pressing the next button
            if (diff < this._tolerance[0]) {
                this._message = messageCorn.EXCELLENT;
            }
            else if (diff < this._tolerance[1]) {
                this._message = messageCorn.GOOD;
            }
            else if (diff < this._tolerance[2]) {
                this._message = messageCorn.OKAY;
            }
            else {
                this._message = messageCorn.MISS;
            }
        }
        else if (isHold) {
            // holding the right button
            if ($timerCorn.getTime() > this._overdueTime) {
                this._message = messageCorn.MISS;
            } else {
                this._message = messageCorn.WAITING;
            }
        } else {
            //console.warn(`Unhandled case: isRelease(${isRelease}) isHold(${isHold})`);
            //console.warn(`Unhandled case: $cornKeyCodeDown(${$cornKeyCodeDown}) $cornKeyCodeUp(${$cornKeyCodeUp})`);
        }

        return this._message;
    }

    getTime() {
        return this._time; // todo: consider beatmap offset
    }

    // todo: remove timer
    getTimeDiff(timer, offset) {
        return Math.abs(this.getTime() - ($timerCorn.getTime() + offset));
    }

    shouldDraw(time, offset, screenTime) {
        return this.getTimeDrawDiff(time, offset, screenTime) >= 0;
    }

    getType() {
        return this._type;
    }

    showPicture() {
        if (!this._picture.isVisible()) {
            var y = CornVisualizerManager.STAFF_START_Y + (this.getModifiers(BeatCorn.HITMARK_Y_INDEX) * CornVisualizerManager.BEAT_SEPARATION);
            this._picture.setZ(50);
            this._picture.show(ORIGIN.CENTER, 1000, y, 85, 85, 255, BLEND_MODE.NORMAL);
            this._picture.setAngle((this.getModifiers(BeatCorn.HITMARK_Y_INDEX) * 90) - 90);
        }
    }

    setAngle(angle) {
        if (this._picture != null) {
            this._angle = angle;
            this._picture.setAngle(this._angle);
        } else {
            console.error("No picture for beat:\n " + beat);
        }
    }

    getTimeDrawDiff(time, offset, screenTime) {
        return (time + screenTime + offset) - this.getTime();
    }

    destroySelf() {
        if (this._picture.isVisible()) {
            this._picture.erase();
        }
    }

    // todo: remove timer
    isOverdue(timer) {
        return $timerCorn.getTime() > this._overdueTime;
    }

    isInMotion() {
        return this._picture._x != this._picture._targetX || this._picture._y != this._picture._targetY;
    }

    getModifiers(key) {
        return this._modifiers[key];
    }

    getMessage() {
        return this._message;
    }

    getSpecialMessage(key) {
        console.warn("Key " + key + " does not exist.");
        return this._specialMessage[key];
    }

    setSpecialMessage(key, value) {
        return this._specialMessage[key] = value;
    }

    setX(x) {
        this._picture.setX(x);
    }

    getX() {
        return this._picture._x;
    }

    getY() {
        return this._picture._y;
    }

    setVectorArr(vectorArr) {
        this._picture._x = vectorArr[0];
        this._picture._y = vectorArr[1];
    }

    getVectorArr() {
        return [this._picture._x, this._picture._y];
    }

    setScale(scale) {
        this._picture.setScale(scale);
    }

    setOpacity(opacity) {
        this._picture.setOpacity(opacity);
    }

    adjustTolerance(tolerance) {
        this._tolerance = tolerance.slice();
        this._overdueTime = this.getTime() + tolerance[2];
        this._tolerableDiff = tolerance[2];
    }

    static CreateEmpty() {
        return BeatCorn.cast(BeatCorn.FAKE_BEAT);
    }

    // hacky message wrappers lmao XD
    static createEndingBeat() {
        var b = BeatCorn.CreateEmpty();
        b._message = messageCorn.END;
        return b;
    }

    static createStartingBeat() {
        var b = BeatCorn.CreateEmpty();
        b._message = messageCorn.START;
        return b;
    }

    static createWaitingBeat() {
        var b = BeatCorn.CreateEmpty();
        b._message = messageCorn.WAITING;
        return b;
    }
}

BeatCorn.BEAT_KEY = "BEAT_KEY";
BeatCorn.BEAT_KEYCODE = 32;
BeatCorn.HOLD_FACTOR = 1.25;
BeatCorn.RELEASE_FACTOR = 1.5;

BeatCorn.DIRECTION_CODE = {
    "left": 37,
    "up": 38,
    "right": 39,
    "down": 40
};

BeatCorn.DIRECTION = {
    "left": "left",
    "up": "up",
    "right": "right",
    "down": "down"
};

BeatCorn.FAKE_BEAT = {
    "_modifiers": {
        "HITMARK_Y_INDEX": 1
    },
    "_overdueTime": 1200,
    "_pictureName": "BeatCorn",
    "_specialMessage": "",
    "_time": "1378",
    "_tolerableDiff": 1000,
    "_tolerance": [
        200,
        500,
        1000
    ],
    "_type": 0,
    "py/object": "__main__.Beat"
};

BeatCorn.HITMARK_Y_INDEX = "HITMARK_Y_INDEX";

const FocusState = {
    START: 0,
    END: 1,
    RELEASE: 2
}

class HoldBeat extends BeatCorn {
    constructor() {
        super();
        this.headBeat = null;
        this.tailBeat = null;
        this.startMessage = messageCorn.WAITING;
    }

    static cast(headBeat) {
        var holdBeat = new HoldBeat();
        var tailBeat = new BeatCorn();

        Object.assign(holdBeat, headBeat);
        Object.assign(tailBeat, headBeat);

        tailBeat._time = headBeat._endTime;
        tailBeat.multiplyTolerance(BeatCorn.RELEASE_FACTOR);
        tailBeat._overdueTime = tailBeat.getTime() + tailBeat._tolerance[tailBeat._tolerance.length - 1];
        tailBeat._tolerableDiff = tailBeat._tolerance[tailBeat._tolerance.length - 1];
        tailBeat.setPicture("TailBeat", 20);

        holdBeat.headBeat = headBeat;
        holdBeat.tailBeat = tailBeat;

        // we have to make a new reference for tail beat though
        holdBeat.tailBeat._picture = new PictureCorn(holdBeat.tailBeat._pictureName);

        holdBeat.bodyPicture = new PictureCorn("Rhythm_Game_Body");
        holdBeat.bodyPicture.setZ(5);

        return holdBeat;
    }

    // todo: remove timer
    react(timer, movement, offset) {
        switch (this.focusState) {
            case FocusState.START:
                this._message = this.headBeat.react($timerCorn, movement, offset);

                switch (this._message) {
                    case messageCorn.WAITING:
                    case messageCorn.MISS:
                    case messageCorn.BAD:
                        // do nothing
                        break;

                    case messageCorn.EXCELLENT:
                    case messageCorn.GOOD:
                    case messageCorn.OKAY:
                        this.startMessage = this._message;
                        this.focusState = FocusState.END;
                        this._message = messageCorn.HOLD_START;
                        break;

                    case messageCorn.START:
                    case messageCorn.END:
                    case messageCorn.HOLD_START:
                        console.error(`Unhandled message: ${this._message}`);
                        break;

                    default:
                        console.error(`Message unknown: ${this._message}`);
                        break;
                }

                break;

            case FocusState.END:
                this._message = this.tailBeat.holdReact($timerCorn, offset);
                // todo: fix hold
                // It detects a hold, but it detects a miss when I release
                // Where does it go???
                // the problem might be in holdReact

                // Crossed out cases:
                // 1. They aren't null
                switch (this._message) {
                    case messageCorn.WAITING:
                    case messageCorn.MISS:
                    case messageCorn.BAD:
                        // do nothing
                        break;

                    case messageCorn.EXCELLENT:
                    case messageCorn.GOOD:
                    case messageCorn.OKAY:
                        // todo: grading here

                        const holdScore = this.startMessage + this._message;
                        this.focusState = FocusState.RELEASE;

                        if (holdScore === 2) {
                            this._message = messageCorn.EXCELLENT;
                        } else if (holdScore < 5) {
                            this._message = messageCorn.GOOD;
                        } else {
                            this._message = messageCorn.OKAY;
                        }

                        break;

                    case messageCorn.START:
                    case messageCorn.END:
                    case messageCorn.HOLD_START:
                        console.error(`Unhandled message: ${this._message}`);
                        break;

                    default:
                        console.error(`Message unknown: ${this._message}`);
                        break;
                }

                break;

            default:
                console.error(`Focus state unknown: ${this.focusState}`);
                this.destroySelf();
                this._message = messageCorn.BAD;
                break;
        }

        return this._message;
    }

    getEndTime() {
        return this._endTime;
    }

    showPicture() {
        if (!this.bodyPicture.isVisible()) {
            var y = CornVisualizerManager.STAFF_START_Y + (this.getModifiers(BeatCorn.HITMARK_Y_INDEX) * CornVisualizerManager.BEAT_SEPARATION);

            this.bodyPicture.setZ(5);
            this.bodyPicture.show(ORIGIN.CENTER, 1000, y, 85, 85, 255, BLEND_MODE.NORMAL);
        }

        this.headBeat.showPicture();
        this.tailBeat.showPicture();
    }

    destroySelf() {
        if (this.bodyPicture.isVisible()) {
            this.headBeat.destroySelf();
            this.tailBeat.destroySelf();
            this.bodyPicture.erase();
        }
    }

    setX(x1, x2) {
        this.headBeat.setX(x1);
        this.tailBeat.setX(x2);

        this.adjustBody(0);
    }
    getX() {
        return super.getX();
    }

    getY() {
        return this.headBeat.getY();
    }

    adjustBody(time) {
        // todo calculate scale x and x for body
        const x3 = (this.headBeat.getX() + this.tailBeat.getX()) / 2;
        const scaleX = (this.tailBeat.getX() - this.headBeat.getX()) / HoldBeat.BODY_PIXEL_LENGTH;

        this.bodyPicture.setX(x3);

        // wave
        var scaleY;

        if (time !== 0) {
            /*
             * bpm here is in fact a period, bad naming
             * todo: optimize cosine because cos takes > 1000 times longer
             * to compute than a single multiply function. It takes 0.2 ms to calculate
             * which is 1.21 % of the calculation time of the entire frame
             * despite being only one function
             * 
             * may be this is too complicated too optimize XD
             */
            scaleY = (42 - HoldBeat.AMP) + Math.abs(HoldBeat.AMP * (
                Math.cos(time * Math.PI / this.mapRef._bpm)
            ));
        } else {
            scaleY = 42;
        }

        this.bodyPicture.setScaleSpec(scaleX * 85, scaleY);
    }

    setScale(scale) {
        this.bodyPicture.setScaleSpec(this.bodyPicture._scaleX, 0.42 * scale);
        this.headBeat.setScale(scale);
        this.tailBeat.setScale(scale);
    }

    setOpacity(opacity) {
        this.bodyPicture.setOpacity(opacity);
        this.headBeat.setOpacity(opacity);
        this.tailBeat.setOpacity(opacity);
    }

    adjustTolerance(tolerance) {
        this.headBeat.adjustTolerance(tolerance);
        this.headBeat.multiplyTolerance(BeatCorn.HOLD_FACTOR);
        this.tailBeat.adjustTolerance(tolerance);
        this.tailBeat.multiplyTolerance(BeatCorn.RELEASE_FACTOR);
    }
}

HoldBeat.BODY_PIXEL_LENGTH = 48 * 0.85;
HoldBeat.AMP = 4;

/**
 * MapCorn
 * Organizes all the BeatCorn objects
 */
class MapCorn {
    /**
     * Heavily based on osu files
     */
    constructor() {
        // metadata
        this._audioFilename = ""; // filename of audio that ends without extension (no .ogg or .m4a)
        this._firstBeatOffset = 0; // milliseconds when the first beat occurs
        this._title = ""; // song title
        this._artist = ""; // song artist

        // game difficulty
        this._difficuly = 0; // how difficult the song is, if can't find difficulty, default to 0
        this._passRate = [0]; // value from 0 to 1, the index with the highest value less than your rate, returns -1 when didn't pass any
        this._screenTime = 5000; // milliseconds of the beat appearing from edge of screen to hitmark
        this._hitmarkX = CornVisualizerManager.HITMARK_X; // can be changed if you want to
        this._mapEndTime = 0;

        // data
        this._currentBeatIndex = 0;
        this._lastVisibleBeatIndex = 0;
        this._queue = [];
        this._soundEffectList = {};
        this._activeInd = 0; // first index to draw or yet to react
        this._drawInd = 0; // index of what's supposed to be drawn
    }

    // #region getters and setters
    getBeatSize() {
        return this._queue.length;
    }

    getBeat(index) {
        return this._queue[index];
    }

    getScreenTime() {
        return this._screenTime;
    }

    getOffset() {
        return this._firstBeatOffset;
    }

    getHitmarkX() {
        return this._hitmarkX;
    }

    getPassRate() {
        return this._passRate[0];
    }

    getMaxScore() {
        return 8 * this._queue.length;
    }

    getDrawableBeat(time) {
        if (this._drawInd < this._queue.length) {
            var beat = this._queue[this._drawInd];
            this._drawInd++;

            // check for time
            // todo: add self offset here
            if (beat.shouldDraw(time, this._firstBeatOffset, this._screenTime)) {
                beat.showPicture(); // make sure it appears
                return beat;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    getAudioFilename() {
        return this._audioFilename;
    }
    // #endregion getters and setters

    static cast(JSONData) {
        var map = new MapCorn();
        var data = JSON.parse(JSONData);

        for (var attr in data) {
            if (!isNaN(attr)) {
                // checkout if attribute is a number
                // if it is false, unshift (poll) it to queue
                map._queue.push(data[attr]);
            } else {
                map[attr] = data[attr];
            }
        }

        // convert bpm
        map._bpm = (1000 * 60) / map._bpm;

        var i;
        for (i = 0; i < map._queue.length; i++) {
            const beat = BeatCorn.cast(map._queue[i]);
            beat.mapRef = map;

            if (beat._type === beatCornType.HOLD) {
                map._queue[i] = HoldBeat.cast(beat);
            } else {
                map._queue[i] = beat;
            }

            if ("_tolerance" in map) {
                map._queue[i].adjustTolerance(map._tolerance);
            }
        }

        return map;
    }

    shift() {
        return this._queue.shift();
    }

    informDrawStart() {
        if (this._activeInd < this._queue.length) {
            this._drawInd = this._activeInd;
            return true;
        } else {
            return false;
        }
    }

    // todo: remove timer
    react(timer, movement, offset) {
        if (this._activeInd < this._queue.length) {
            var beat = this._queue[this._activeInd];
            var message = beat.react($timerCorn, movement, offset);

            if (message != messageCorn.WAITING) {
                this._activeInd++;
            }

            return beat;
        } else if (this.isGameOver($timerCorn.getTime())) {
            return BeatCorn.createEndingBeat();
        } else {
            return BeatCorn.createWaitingBeat();
        }
    }

    requestVisibleBeat(time) {
        if (this._drawInd < this._queue.length) {
            const beat = this._queue[this._drawInd];

            if (beat.shouldDraw(time, this._firstBeatOffset, this._screenTime)) {
                beat.showPicture();
                this._drawInd++;
                return beat;
            }
        }

        return null;
    }

    isGameOver(time) {
        return this._mapEndTime <= time;
    }
}

class CornChild {
    constructor() {
        this.initialize();
    }

    initialize() {
        this.map = null;
        this.isGameOver = false;
        $tscManager.registerChild(this);
    }

    receiveMap(map) {
        this.map = map;
    }

    update(movement) {
        throw "Override this";
    }

    receiveMessage(beat) {
        throw "Override this";
    }

    isDone() {
        throw "Override this";
    }
}

class CornVisualizer extends CornChild {
    constructor() {
        super();
        this.manager = null;
    }

    setVisualizerManager(manager) {
        this.manager = manager;
    }

    isDone() {
        return true;
    }
}

// #endregion Cores

// #region Visualizers

/**
 * TSCVisualizer
 * Visualizes the beats on the screen
 * Requires the beat map reference, unlike other listeners
 * */
class CornVisualizerManager {
    constructor() {
        this.interpolator = new BeatInterpolator();
        this.interpolator.setVisualizerManager(this);
        this.vfx = new CornVFX();
        this.vfx.setVisualizerManager(this);
        this.holdManager = new HoldManager();
        this.holdManager.setVisualizerManager(this);
        this.gradeEffectManager = new GradeEffectManager();
        this.gradeEffectManager.setVisualizerManager(this);
    }

    giveToVFX(beat) {
        this.vfx.addBeat(beat, $timerCorn.getTime());
    }

    giveToHoldManager(beat) {
        this.holdManager.addBeat(beat);
    }

    createYellowEffect(time, beat) {
        this.gradeEffectManager.createYellowEffect(time, beat);
    }
}

// static variables for Visualizer
CornVisualizerManager.STAFF_START_Y = 461;
CornVisualizerManager.HITMARK_X = 187;
CornVisualizerManager.useStable = true;
CornVisualizerManager.BEAT_MAGNITUDE = ScreenCorn.SCREEN_WIDTH
    + ScreenCorn.OFFSCREEN_OFFSET - CornVisualizerManager.HITMARK_X;
CornVisualizerManager.BEAT_SEPARATION = 43;

class BeatInterpolator extends CornVisualizer {
    constructor() {
        super();
        this.beatList = [];
        this.map = null;
        this.deadZone = 135;
    }

    update() {
        // beg for beats
        const time = $timerCorn.getTime();
        const newBeat = this.map.requestVisibleBeat(time);

        if (newBeat !== null) {
            this.beatList.push(newBeat);
        }

        // visualize all the beats we have
        var ind;
        const screenTime = this.map.getScreenTime();
        // optimization 1 and optimization 2
        const optimizedComputationX = CornVisualizerManager.HITMARK_X
            - (CornVisualizerManager.BEAT_MAGNITUDE * time / screenTime)

        for (ind = 0; ind < this.beatList.length; ind++) {
            const beatItem = this.beatList[ind];
            const x1 = optimizedComputationX
                + (CornVisualizerManager.BEAT_MAGNITUDE * beatItem.getTime() / screenTime);

            if (x1 > this.deadZone && beatItem.focusState === FocusState.START) {
                if (beatItem._type === beatCornType.HOLD) {
                    const x2 = optimizedComputationX
                        + (CornVisualizerManager.BEAT_MAGNITUDE * beatItem.getEndTime()
                            / screenTime);
                    beatItem.setX(x1, x2);
                } else {
                    beatItem.setX(x1);
                }
            } else {
                ind--;
                ArrayUtil.removeItem(this.beatList, beatItem);
                if (beatItem.focusState === FocusState.START) {
                    beatItem.destroySelf();
                }
            }
        }
    }

    receiveMessage(beat) {
        var message = beat.getMessage();

        switch (message) {
            case messageCorn.START:
                this.update();
                break;
            case messageCorn.EXCELLENT: case messageCorn.GOOD: case messageCorn.OKAY:
            case messageCorn.BAD: case messageCorn.MISS:
                if (beat.focusState === FocusState.START) {
                    ArrayUtil.removeItem(this.beatList, beat);
                    this.manager.giveToVFX(beat);
                }
                break;
            case messageCorn.HOLD_START:
                this.manager.giveToHoldManager(beat);
                break;
            case messageCorn.WAITING:
                // do nothing
                break;
            case messageCorn.END:
                this.isGameOver = true;
                break;
            default:
                break;
        }
    }

    isDone() {
        return this.isGameOver && this.beatList.length === 0;
    }
}

class CornVFX extends CornVisualizer {
    constructor() {
        super();
        this.beatList = [];
        this.d1 = 0;
        this.d2 = 10;
        this.d3 = 50;

        this.t2 = this.d1;
        this.t3 = this.t2 + this.d2;
        this.t4 = this.t3 + this.d3;
    }

    addBeat(beat, time) {
        // process beat to have more info for animation
        beat.vfxStartTime = time;
        this.beatList.push(beat);
    }

    update() {
        const time = $timerCorn.getTime();
        var ind;
        for (ind = this.beatList.length - 1; ind >= 0; ind--) {
            const beatItem = this.beatList[ind];
            const elapsedTime = time - beatItem.vfxStartTime;

            if (elapsedTime < this.t2) {
                const scale = VectorUtil.interpolate(this.t1, elapsedTime, this.t2, 100, 75);
                beatItem.setScale(scale);
            } else if (elapsedTime < this.t3) {
                beatItem.setScale(75);
            } else if (elapsedTime < this.t4) {
                const scale = VectorUtil.interpolate(this.t3, elapsedTime, this.t4, 75, 150);
                beatItem.setScale(scale);
                const opacity = VectorUtil.interpolate(this.t3, elapsedTime, this.t4, 255, 0);
                beatItem.setOpacity(opacity);
            } else {
                ArrayUtil.removeItem(this.beatList, beatItem);
                beatItem.destroySelf();
            }
        }
    }

    receiveMessage(beat) {
        var message = beat.getMessage();

        switch (message) {
            case messageCorn.END:
                this.isGameOver = true;
                break;
            default:
                break;
        }
    }

    isDone() {
        return this.isGameOver && this.beatList.length === 0;
    }
}

class BokehParticle {
    constructor() {
        this.initialize();
    }

    initialize() {
        this.picture = null;
        this.scale = 100;
        this.speedX = 2;
        this.speedY = 2;
        this.startingPointX = 0;
        this.startingPointY = 0;
        this.currentX = 0;
        this.currentY = 0;
        this.opacity = 255;
        this.scaleSpeed = -0.015;
    }

    /**
     * @param {String} pictureName
     */
    setPictureName(pictureName) {
        this.picture = new PictureCorn(pictureName);
        this.picture.setZ(-8);
    }

    /**
     * @param {int} [0, 100]
     */
    setScale(scale) {
        this.scale = scale;
    }

    /**
     * @param {int} x [-2, 2]
     * @param {int} y [-2, 2]
     */
    setSpeed(x, y) {
        this.speedX = x;
        this.speedY = y;
    }

    /**
     * @param {BeatCorn} beat
     */
    setStartingPoint(beat) {
        this.startingPointX = beat.getX();
        this.startingPointY = beat.getY();
    }

    /**
     * @param {int} deltaTime
     */
    update(deltaTime) {
        if (!this.isKillable() && this.picture === null) {
            console.error("Invoking update when picture is null for BokehParticle");
            return;
        }

        if (this.isKillable()) {
            return;
        }

        if (!this.picture.isVisible()) {
            this.picture.show(ORIGIN.CENTER,
                this.startingPointX,
                this.startingPointY,
                this.scale, this.scale,
                this.opacity,
                BLEND_MODE.NORMAL);

            this.currentX = this.startingPointX;
            this.currentY = this.startingPointY;
        }

        // moving code here
        this.currentX = this.currentX + (deltaTime * this.speedX);
        this.currentY = this.currentY + (deltaTime * this.speedY);
        this.scale = this.scale + (deltaTime * this.scaleSpeed);

        this.picture.setX(this.currentX);
        this.picture.setY(this.currentY);
        this.picture.setScale(this.scale);

        if (this.scale < 0) {
            this.scale = 0;
            this.picture.erase();
        }
    }

    forceKill() {
        this.scale = 0;
        this.picture.erase();
    }

    isKillable() {
        return this.scale === 0;
    }
}

BokehParticle.COUNT_LIMIT = 24;

/**
 * Handles the end beat effects such as showing the word grades
 * And particle effects and such
 **/
class GradeEffectManager extends CornVisualizer {
    constructor() {
        super();
        // grade effects
        this.gradeEffectList = []; // list of pictureCorns
        this.idx = 0;
        this.maxTime = 500;
        this.ft1 = 100;
        this.ft2 = 450;
        this.height = 16;
        this.h = this.maxTime / 2;
        this.k = this.height;

        // big white hollow sphere
        this.bigWhiteList = [];
        this.shortTime = 250;

        // big yellow hollow sphere
        this.bigYellowList = [];
        this.maxEffectTime = 400;

        // bokeh particle
        this.bokehList = [];
    }

    update() {
        const currentTime = $timerCorn.getTime();

        // grade
        for (this.idx = this.gradeEffectList.length - 1; this.idx >= 0; this.idx--) {
            var picture = this.gradeEffectList[this.idx];
            var elapsedTime = currentTime - picture.timeStart;

            if (elapsedTime >= this.maxTime) {
                ArrayUtil.removeItem(this.gradeEffectList, picture);
                picture.erase();
                continue;
            }

            var newY = this.findNewHeight(elapsedTime, this.h, this.k);
            var newFade = 255 * (1 - (elapsedTime / this.maxTime));
            picture.setY(picture.yOffset - newY);
            picture.setOpacity(newFade);
        }


        // big white
        for (this.idx = this.bigWhiteList.length - 1; this.idx >= 0; this.idx--) {
            var picture = this.bigWhiteList[this.idx];
            var elapsedTime = currentTime - picture.timeStart;

            if (elapsedTime >= this.maxEffectTime) {
                ArrayUtil.removeItem(this.bigWhiteList, picture);
                picture.erase();
                continue;
            }

            var newScale = this.findNewHeight(elapsedTime, this.maxEffectTime, 100);

            if (elapsedTime > this.shortTime) {
                var newFade = this.findNewHeight(elapsedTime - this.shortTime, 150, 255);
                picture.setOpacity(newFade);
            }

            picture.setScale(newScale);
        }

        // big yellow
        for (this.idx = this.bigYellowList.length - 1; this.idx >= 0; this.idx--) {
            var picture = this.bigYellowList[this.idx];
            var elapsedTime = currentTime - picture.timeStart;

            if (elapsedTime >= this.maxEffectTime) {
                ArrayUtil.removeItem(this.bigYellowList, picture);
                picture.erase();
                continue;
            }

            var newScale = this.findNewHeight(elapsedTime, this.maxEffectTime, 100);

            if (elapsedTime > this.shortTime) {
                var newFade = this.findNewHeight(150 + elapsedTime - this.shortTime, 150, 255);
                picture.setOpacity(newFade);
            }

            picture.setScale(newScale);
        }

        // bokeh
        const deltaTime = $timerCorn.getDeltaTime() * 2;
        for (this.idx = this.bokehList.length - 1; this.idx >= 0; this.idx--) {
            const bokeh = this.bokehList[this.idx];
            bokeh.update(deltaTime);
            if (bokeh.isKillable()) {
                ArrayUtil.removeItem(this.bokehList, bokeh);
            }
        }
    }

    receiveMessage(beat) {
        var message = beat.getMessage();
        const currentTime = $timerCorn.getTime();

        var picture;

        switch (message) {
            case messageCorn.EXCELLENT:
                picture = new PictureCorn("Great");
                this.createEffects(currentTime, beat);
                break;
            case messageCorn.GOOD:
                picture = new PictureCorn("Good");
                this.createEffects(currentTime, beat);
                break;
            case messageCorn.OKAY:
                picture = new PictureCorn("Meh");
                this.createEffects(currentTime, beat);
                break;
            case messageCorn.BAD:
                picture = new PictureCorn("Bad");
                break;
            case messageCorn.MISS:
                picture = new PictureCorn("Miss");
                break;
            case messageCorn.END:
                this.isGameOver = true;
                return;
            default:
                return;
        }

        picture.setZ(100);
        picture.show(ORIGIN.CENTER, beat.getX(), 1000, 60, 60, 255, BLEND_MODE.NORMAL);
        picture.timeStart = currentTime;
        picture.yOffset = beat.getY();
        this.gradeEffectList.push(picture);
    }

    createEffects(currentTime, beat) {
        var bigWhite = new PictureCorn("BigWhiteEffect");
        bigWhite.timeStart = currentTime;
        bigWhite.setZ(-7);
        bigWhite.show(ORIGIN.CENTER, beat.getX(), beat.getY(), 100, 100, 255, BLEND_MODE.NORMAL);
        bigWhite.timeStart = currentTime;
        this.bigWhiteList.push(bigWhite);

        this.createYellowEffect(currentTime, beat);

        if (ConfigManager._particles) {
            this.createBokehBundle(beat);
        }
    }

    createYellowEffect(currentTime, beat) {
        var bigYellow = new PictureCorn("BigYellowEffect");
        bigYellow.timeStart = currentTime;
        bigYellow.setZ(-8);
        bigYellow.show(ORIGIN.CENTER, beat.getX(), beat.getY(), 100, 100, 255, BLEND_MODE.NORMAL);
        bigYellow.timeStart = currentTime;
        this.bigYellowList.push(bigYellow);
    }

    createBokehBundle(beat) {
        const prefixName = "Particle";
        const smallestScale = 10;
        const biggestScale = 15;

        var suffix;
        var idx;
        var id2;
        var overLimit = false;

        for (suffix = 2; suffix <= 3; suffix++) {
            const count = 1 + Math.floor(Math.random() * 3);
            for (idx = 0; idx < count; idx++) {
                const newBokeh = new BokehParticle();
                newBokeh.setPictureName(prefixName + suffix);
                newBokeh.setScale(Daikon.randomInt(smallestScale, biggestScale));
                newBokeh.setSpeed(
                    Daikon.randomFloat(-0.1, 0.1),
                    Daikon.randomFloat(-0.1, 0.1)
                );
                newBokeh.setStartingPoint(beat);

                this.bokehList.push(newBokeh);

                // prevent going over the limit
                if (this.bokehList.length >= BokehParticle.COUNT_LIMIT) {
                    for (id2 = 0; id2 < this.bokehList.length; id2++) {
                        if (!this.bokehList[id2].isKillable()) {
                            this.bokehList[id2].forceKill();
                            break;
                        }
                    }

                    overLimit = true;
                    break;
                }
            }

            if (overLimit) {
                break;
            }
        }
    }

    findNewHeight(currentTime, h, k) {
        return (k * currentTime / h) * ((-currentTime / h) + 2);
    }

    isDone() {
        return this.isGameOver && this.gradeEffectList.length === 0;
    }
}

class HoldManager extends CornVisualizer {
    constructor() {
        super();
        this.beatList = [];
        this.step = 1000 / 60;
        this.speed = 15;
    }

    receiveMap(map) {
        super.receiveMap(map);
        this.interval = map._tickInterval;
        this.step = CornVisualizerManager.BEAT_MAGNITUDE / this.map.getScreenTime() * this.speed;
    }

    addBeat(beat) {
        $tscManager.setInputMode(InputMode.HOLD);
        const time = $timerCorn.getTime();
        beat._x = CornVisualizer.HITMARK_X;
        this.manager.createYellowEffect(time, beat);
        beat.effectLastMade = Math.floor(time / this.interval) * this.interval;
        this.beatList.push(beat);
    }

    update() {
        if ($tscManager.getInputMode() !== InputMode.HOLD) {
            return;
        }

        var idx;
        const time = $timerCorn.getTime();
        for (idx = this.beatList.length - 1; idx >= 0; idx--) {
            const holdBeat = this.beatList[idx];
            const headBeat = holdBeat.headBeat;
            const tailBeat = holdBeat.tailBeat;

            // move head to hitmarkx
            // todo: apply timeDelta * 1000
            const currVector = headBeat.getVectorArr();
            const newVector = VectorUtil.moveTowards(currVector, [CornVisualizerManager.HITMARK_X, currVector[1]], [this.step, this.step]);
            headBeat.setVectorArr(newVector);

            // interpolate tail
            // todo optimized
            const lerpValue = (tailBeat.getTime() - time) / this.map.getScreenTime();
            const newX = CornVisualizerManager.HITMARK_X + (CornVisualizerManager.BEAT_MAGNITUDE * lerpValue);
            tailBeat.setX(newX);

            // based on x1 and x1, scale and position body, done internally
            holdBeat.adjustBody(time);

            // add effect
            if (time > holdBeat.effectLastMade + this.interval) {
                this.manager.createYellowEffect(time, headBeat);
                holdBeat.effectLastMade = holdBeat.effectLastMade + this.interval;
            }

            // todo check for user input
            $tscManager.requestBeatReaction(holdBeat);

            if (holdBeat.getMessage() !== messageCorn.WAITING) {
                // destroy
                ArrayUtil.removeItem(this.beatList, holdBeat);
                this.manager.giveToVFX(holdBeat);
                if (this.beatList.length === 0) {
                    $tscManager.setInputMode(InputMode.TAP);
                }
            }
        }
    }

    receiveMessage(beat) {

    }

    isDone() {
        return this.beatList.length === 0;
    }
}

// #endregion Visualizers

// #region Other essential listeners

class AudioEffectManager extends CornChild {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        const soundNameList = {
            ExcellentSound: "Damage2",
            OkaySound: "Damage1",
            BadSound: "Cat",
            MissSound: "Miss"
        };

        for (var name in soundNameList) {
            const url = GreenCheesecake.encode(soundNameList[name], "se");
            this[name] = new GreenCheesecake(url);
        }
    }

    receiveMap(map) {
        super.receiveMap(map);
        const self = this;

        for (var name in map._soundEffectList) {
            const url = GreenCheesecake.encode(map._soundEffectList[name], "se");
            this[name] = new GreenCheesecake(url);
        };

        this.OkaySound2 = new GreenCheesecake(GreenCheesecake.encode(map._soundEffectList["OkaySound"], "se"));

        this.OkaySound.pitch = 0.9;
        this.OkaySound2.pitch = 0.8;

        // volume
        if ($cornField.get_picBad() === "02_Battleback_Speakeasy_Bad") {
            this.ExcellentSound.setVolume(0.27);
            this.OkaySound.setVolume(0.2);
            this.OkaySound2.setVolume(0.2);
        } else if ($cornField.get_picBad() === "03_Battleback_Greenroom_Bad") {
            this.ExcellentSound.setVolume(0.13);
            this.OkaySound.setVolume(0.1);
            this.OkaySound2.setVolume(0.1);
        } else if ($cornField.get_picBad() === "04_Battleback_Stage_Bad") {
            this.ExcellentSound.setVolume(0);
            this.OkaySound.setVolume(0);
            this.OkaySound2.setVolume(0);
        } else {
            this.ExcellentSound.setVolume(0.4);
            this.OkaySound.setVolume(0.3);
            this.OkaySound2.setVolume(0.3);
        }

        this.BadSound.setVolume(1);
        this.MissSound.setVolume(1);
    }

    update() {

    }

    receiveMessage(beat) {
        var message = beat.getMessage();

        if (beat.focusState === FocusState.RELEASE) {
            return;
        }

        switch (message) {
            case messageCorn.START:
                break;
            case messageCorn.EXCELLENT:
                this.ExcellentSound.startPlaying();
                break;
            case messageCorn.GOOD:
                this.OkaySound.startPlaying();
                break;
            case messageCorn.OKAY:
                this.OkaySound2.startPlaying();
                break;
            case messageCorn.BAD:
                this.BadSound.startPlaying();
                break;
            case messageCorn.MISS:
                this.MissSound.startPlaying();
                break;
            case messageCorn.WAITING:
                // do nothing
                break;
            case messageCorn.END:
                break;
            case messageCorn.HOLD_START:
                this.receiveMessage(beat.headBeat);
                break;
            default:
                break;
        }
    }

    isDone() {
        if ($backgroundBuffer !== null) {
            return $backgroundBuffer.hasEnded();
        } else {
            return true;
        }
    }

    setMasterSong(masterSong) {
        this.masterSong = masterSong;
    }
}

AudioEffectManager.MASTER_VOLUME = 100;

class ScoreCornManager extends CornChild {
    constructor() {
        super();

        this._maxScore = 1; // prevent division by zero error

        // HP stuff
        this._mistakeTracker = 7;
        this.hp_upperLimit = 16;
        this.hp_lowerLimit = 0;
        this.failLimit = 0; // in interval [0,1]

        // ** Slider ***********
        this._slider = new PictureCorn("RhythmSlider");
        this._slider.setZ(0);
        var slider_y = VectorUtil.interpolate(0, this._mistakeTracker, this.hp_upperLimit, ScoreCornManager.SLIDER_BIG_Y, ScoreCornManager.SLIDER_SMALL_Y);
        this._slider.show(ORIGIN.CENTER, 87, ScoreCornManager.SLIDER_BIG_Y, 85, 85, 255, BLEND_MODE.NORMAL);

        // ** Progress visualization ******
        this._progressBarMiddle = new PictureCorn("Rhythm-Game_Progress-Bar_MIDDLE");
        this._progressBarMiddle.setZ(1);
        this._progressBarMiddle.show(0, 156, 385, 0, 50, 255, BLEND_MODE.NORMAL);
        this._middleMaxScale = 84;
        this._startTime = Date.now() / 1000; // gets current time in seconds
        this.__timer = 0;
        this._songLength = 0;

        // Heart Beats
        this._heartbeats = null

        // song
        this.song = null;

        // visible difficulty gauge
        this.difficultyFactor = 0;
        this.targetDifficultyFactor = 0;
        this.difficultyAdjustmentDone = false;

        // animating the slider
        this.isInMotion = true;
        this.sliderVel = 0;
        this.targetSliderY = slider_y;
        this.currentSliderY = ScoreCornManager.SLIDER_BIG_Y;
    }

    receiveMap(map) {
        super.receiveMap(map);
        this._maxScore = map.getMaxScore();
        this._passRate = map.getPassRate();
        this.targetDifficultyFactor = this._passRate / 100;
    }

    update() {
        //this.updateSignBoard();
        this.updateProgressTracker();

        // added by Allan XD
        this.updateDifficultyGauge();
        this.updateSlider();
    }

    receiveMessage(beat) {
        var message = beat.getMessage();

        switch (message) {
            case messageCorn.START:
                return;
            case messageCorn.EXCELLENT:
                this._mistakeTracker += 2;
                break;
            case messageCorn.GOOD:
                this._mistakeTracker += 1;
                break;
            case messageCorn.OKAY:
                this._mistakeTracker += -0.5;
                break;
            case messageCorn.BAD:
                this._mistakeTracker += -1;
                break;
            case messageCorn.MISS:
                this._mistakeTracker += -3;
                break;
            default:
                break;
        }

        this.process_mistakeTracker()

        // slider update update
        this.targetSliderY = VectorUtil.interpolate(0, this._mistakeTracker, this.hp_upperLimit, ScoreCornManager.SLIDER_BIG_Y, ScoreCornManager.SLIDER_SMALL_Y);
        this.isInMotion = this.isInMotion || this.targetSliderY !== this.currentSliderY;
    }

    isWithinBounds(value, bounds) {
        return value <= bounds[1] && value >= bounds[0];
    }

    isDone() {
        return true;
    }

    getScoreRate() {
        return this._mistakeTracker / this.hp_upperLimit;
    }

    updateSlider() {
        if (this.isInMotion) {
            this.currentSliderY += this.sliderVel * $timerCorn.getDeltaTime();

            if (this.sliderVel > ScoreCornManager.MAX_VELOCITY) {
                this.sliderVel = ScoreCornManager.MAX_VELOCITY;
            } else if (this.sliderVel < ScoreCornManager.MIN_VELOCITY) {
                this.sliderVel = ScoreCornManager.MIN_VELOCITY;
            }

            if (this.currentSliderY > ScoreCornManager.SLIDER_BIG_Y) {
                this.currentSliderY = ScoreCornManager.SLIDER_BIG_Y;
                this.sliderVel = 0;
            } else if (this.currentSliderY < ScoreCornManager.SLIDER_SMALL_Y) {
                this.currentSliderY = ScoreCornManager.SLIDER_SMALL_Y;
                this.sliderVel = 0;
            }

            if (this.currentSliderY < this.targetSliderY) {
                this.sliderVel += ScoreCornManager.SLIDER_ACCELERATION;
            } else {
                this.sliderVel += -ScoreCornManager.SLIDER_ACCELERATION;
            }

            this._slider.setY(this.currentSliderY);
            this._slider.setScaleSpec(85, 76);

            this.isInMotion = (this.sliderVel > 0 && this.currentSliderY <= this.targetSliderY)
                || (this.sliderVel < 0 && this.currentSliderY >= this.targetSliderY);
        } else if (this.sliderVel !== 0) {
            this.currentSliderY = this.targetSliderY;
            this.sliderVel = 0;
            this._slider.setY(this.currentSliderY);
            this._slider.setScaleSpec(85, 85);
        }
    }

    updateSignBoard() {
        var targ;
        if (this._board === this._nextBoard) {
            targ = this.top_vec;
        } else {
            targ = this.bot_vec;
        }

        // check distance to bot
        // if abs(distance) <= bot_coord
        // then set prevboard = currboard
        if (this._board !== null) {
            var currPos = [this._board._x, this._board._y];
            var v_ = VectorUtil.moveTowards(currPos, targ, [4, 4]);
            this._board.setX(v_[0]);
            this._board.setY(v_[1]);

            //console.log(v_, ' : ', this.bot_vec, ' : ', v_ == this.bot_vec);
            if (v_[1] === this.bot_vec[1]) {
                this._board.erase();
                this._board = this._nextBoard;
                if (this._board !== null) {
                    this._board.show(0, v_[0], v_[1], 100, 75, 255, 0);
                }
            }
        } else {
            if (this._nextBoard !== null) {
                this._board = this._nextBoard;
                this._board.show(0, 315, 375, 100, 75, 255, 0);
            }
        }
    }

    updateDifficultyGauge() {
        if (this.difficultyFactor < this.targetDifficultyFactor) {
            this.difficultyFactor = this.difficultyFactor + ($timerCorn.getDeltaTime() / 500);
            $cornField.adjustDifficulty(this.difficultyFactor);
        } else if (!this.difficultyAdjustmentDone) {
            $cornField.adjustDifficulty(this.targetDifficultyFactor);
            this.difficultyAdjustmentDone = true;
        }
    }

    process_mistakeTracker() {
        //clamp
        if (this._mistakeTracker > this.hp_upperLimit) this._mistakeTracker = this.hp_upperLimit;
        if (this._mistakeTracker < this.hp_lowerLimit) this._mistakeTracker = this.hp_lowerLimit;

        var _mt = Math.floor(this._mistakeTracker);
        if (_mt < 3) {
            this.song.volume = 0;
            this._heartbeats.volume = 1;
        } else if (_mt < 6) {
            this.song.volume = 0.2;
            this._heartbeats.volume = 0.5;
        } else if (_mt < 8) {
            this.song.volume = 0.4;
            this._heartbeats.volume = 0;
        } else if (_mt < 11) {
            this.song.volume = 0.6;
            this._heartbeats.volume = 0;
        } else if (_mt < 13) {
            this.song.volume = 0.8;
            this._heartbeats.volume = 0;
        } else {
            this.song.volume = 1;
            this._heartbeats.volume = 0;
        }
    }

    setSong(song) {
        this.song = song;
    }

    get_mistakeTracker() {
        return Math.floor(this._mistakeTracker);
    }

    informLoadComplete() {
        this._songLength = this.song._buffer.duration;
        this._heartbeats.setVolume(0);
    }

    updateProgressTracker() {
        this.__timer = Date.now() / 1000; // gets current time in seconds
        var percProgress = (this.__timer - this._startTime) / this._songLength;
        if (percProgress <= 1) {
            var new_scale_x = percProgress * this._middleMaxScale;
            this._progressBarMiddle.setScaleSpec(new_scale_x, 50);
        }
    }

    setHeartbeats(hb) {
        this._heartbeats = hb;
    }

    setFailLimit(val) {
        this._failLimit = val;
    }
}

ScoreCornManager.MAX_VELOCITY = 0.5;
ScoreCornManager.MIN_VELOCITY = -0.5;
ScoreCornManager.SLIDER_SMALL_Y = 448;
ScoreCornManager.SLIDER_BIG_Y = 588;
ScoreCornManager.SLIDER_ACCELERATION = 0.03;
// #endregion Other listeners

// #region Miscellaneous listeners
/**
 * Possible bug:
 * directions should be based on the parent???
 * */
class BeatAutoPlayer extends CornChild {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        this.target = messageCorn.EXCELLENT;
    }

    update(movement) { }

    receiveMessage(beat) {

    }

    isDone() {
        return true;
    }

    evaluate() {
        var newMovement = null;

        if (this.map._activeInd < this.map._queue.length) {
            this.beat = this.map._queue[this.map._activeInd];
        }

        if (this.beat != null) {
            switch ($tscManager.getInputMode()) {
                case InputMode.TAP:
                    $cornKeyCodeDown = null;
                    $cornKeyCodeUp = null;
                    newMovement = 37 + this.beat.getModifiers(BeatCorn.HITMARK_Y_INDEX);
                    const message = this.beat.react(null, newMovement, 0)
                    if (message === this.target) {
                        // do nothing
                    } else if (message === messageCorn.HOLD_START) {
                        if (this.beat.startMessage === this.target) {
                            //
                        } else {
                            newMovement = null;
                        }
                        this.endBeat = this.beat.tailBeat;

                        this.beat.focusState = FocusState.START;
                    } else {
                        newMovement = null;
                    }
                    break;

                case InputMode.HOLD:
                    // pretend release
                    //console.log("Inspecting hold");
                    $cornKeyCodeDown = null;

                    if (Input.keyMapper[37] === "") { // wasd
                        switch (this.endBeat.getModifiers(BeatCorn.HITMARK_Y_INDEX)) {
                            case 0: // left
                                $cornKeyCodeUp = 65;
                                break;
                            case 1: // up
                                $cornKeyCodeUp = 87;
                                break;
                            case 2: // right
                                $cornKeyCodeUp = 68;
                                break;
                            case 3: // down
                                $cornKeyCodeUp = 83;
                                break;
                        }
                    } else {
                        $cornKeyCodeUp = 37 + this.endBeat.getModifiers(BeatCorn.HITMARK_Y_INDEX);;
                    }

                    if (this.endBeat.holdReact(null, 0) === this.target) {
                        //console.log("Release");
                    } else {
                        //console.log("Hold");
                        // go back to hold
                        $cornKeyCodeUp = null;

                        if (Input.keyMapper[37] === "") { // wasd
                            switch (this.endBeat.getModifiers(BeatCorn.HITMARK_Y_INDEX)) {
                                case 0: // left
                                    newMovement = 65;
                                    $cornKeyCodeDown = 65;
                                    break;
                                case 1: // up
                                    newMovement = 87;
                                    $cornKeyCodeDown = 87;
                                    break;
                                case 2: // right
                                    newMovement = 68;
                                    $cornKeyCodeDown = 68;
                                    break;
                                case 3: // down
                                    newMovement = 83;
                                    $cornKeyCodeDown = 83;
                                    break;
                            }
                        } else {
                            $cornKeyCodeDown = 37 + this.endBeat.getModifiers(BeatCorn.HITMARK_Y_INDEX);;
                            newMovement = $cornKeyCodeDown;
                        }
                    }
                    break;

                default:
                    console.error("Unknown input type: " + $tscManager.getInputMode());
                    break;
            }
        }

        return newMovement;
    }
}
// #endregion

/**
 * TimedSystemCorn
 * Extremely important class!
 * Alerts all the registered listeners about what's happening
 * Like did the player miss a beat
 * Are we starting the game?
 * Have we stopped the game?
 * Did we pause the game? // todo
 * */
class TimedSystemCorn extends CornChild {
    constructor() {
        super();
    }

    initialize() {
        super.initialize();
        $timerCorn = new MusicTimerCorn();
        this._listeners = [];
        // todo: do safety checks cause javascript is evil
        this._offset = 0; // todo change based on preferences AND map
        this.map = null;
        this._isGameOver = false;
    }

    loadMap(map) {
        this._map = map;
    }

    getOffset() {
        return this._offset;
    }

    setOffset(offset) {
        this._offset = offset;
    }

    // screenTime is the duration between the beat's appearance
    // to the time it should be executed
    start(screenTime) {
        this.pressed = false;

        $timerCorn.startTime();
        $kitchenSync.startPlaying();

        $timerCorn.updateTime();

        // start hack
        // too lazy to make new function
        // just wrap your message in a beat
        var startingBeat = BeatCorn.createStartingBeat();
        startingBeat._message = messageCorn.START;

        this.sendMessage(startingBeat);
    }

    update(movement) {
        if ($tscManager.getInputMode() !== InputMode.TAP) {
            return;
        }

        const beat = this.map.react($timerCorn, movement, this._offset);
        this.reactToBeat(beat);
    }

    reactToHold(beat) {
        beat.react($timerCorn, 0, this._offset);
        this.reactToBeat(beat);
    }

    /**
     * Beat should contain message
     * @param {any} beat
     */
    reactToBeat(beat) {
        if (beat === null) {
            console.error("This should not have had happened?");
            return;
        }

        var message = beat.getMessage();

        // todo: do stuff here like remove map top on certain cases
        switch (message) {
            case messageCorn.START:
                this.sendMessage(beat);
                break;
            case messageCorn.EXCELLENT: case messageCorn.GOOD:
            case messageCorn.OKAY: case messageCorn.BAD:
            case messageCorn.MISS: case messageCorn.HOLD_START:
                this.sendMessage(beat);
                this.pressed = true;
                break;
            case messageCorn.WAITING:
                break;
            case messageCorn.END:
                this.sendMessage(beat);
                this._isGameOver = true;
                break;
            default:
                break;
        }
    }

    receiveMessage(beat) { }

    isDone() {
        return this._isGameOver;
    }

    registerListener(listener) {
        // yes, javascript has a function with three equal signs
        // don't worry about this
        // code from https://stackoverflow.com/a/3007494
        // by Sean Vieria
        if (typeof listener.receiveMessage === "function") {
            this._listeners.push(listener);
            return;
        }

        if (typeof listener.receiveMessage === "undefined") {
            throw "Your function is undefined";
        } else {
            throw "It's neither undefined nor a function. It's a " + typeof listener.receiveMessage;
        }

    }

    sendMessage(beat) {
        // do something with listeners based on the reaction
        $tscManager.sendMessage(beat);

    }

    isGameOver() {
        if (this.map != null) {
            return this._isGameOver;
        } else {
            console.warn("Map not yet loaded. Don't check if game is over, next time.");
            return false;
        }
    }
}

const InputMode = {
    TAP: 0,
    HOLD: 1
}

/**
 * TSCManager
 * Manages all interaction
 */
class TSCManager {
    constructor() {
        this.initialize();
    }

    initialize() {
        // make sure everything has the same timer
        this._map = null;
        this._isPrepared = false;
        this.children = [];
        this.inputMode = InputMode.TAP;
        $backgroundBuffer = null;
    }

    loadMap(mapName) {
        // todo change the location???
        this._tsc = new TimedSystemCorn(); // always make first
        this._soundManager = new AudioEffectManager();
        this._tscVisualizer = new CornVisualizerManager();
        this._scoreManager = new ScoreCornManager();
        JSONUtilDaikon.readMap(mapName, this);
    }

    start() {
        // todo: if we need to separate loading data and starting data
        if (this._isPrepared) {
            this._tsc.start();
        } else {
            console.warn("Data not yet ready");
        }
    }

    isPrepared() {
        return this._isPrepared;
    }

    update(movement) {
        if (this._isPrepared) {
            $timerCorn.updateTime();

            this.children.forEach(function (child) {
                child.update(movement);
            });
        }
    }

    /**
     * 
     * @param {InputMode} inputMode
     */
    setInputMode(inputMode) {
        this.inputMode = inputMode;
    }

    getInputMode() {
        return this.inputMode;
    }

    receiveData(data) {
        const map = MapCorn.cast(data);
        this._map = map;

        // give MapCorn to everyone who needs it
        this.children.forEach(function (child) {
            child.receiveMap(map);
        });

        // add listener
        if ($kitchenSync == null) {
            $kitchenSync = new KitchenSync();
        }

        $kitchenSync.addListener(this);

        // add song
        const masterFilename = map.getAudioFilename();
        const masterBuffer = $kitchenSync.addSong(masterFilename + "Beatmap", "bgm");
        $kitchenSync.addSong(masterFilename + "Background", "bgm");
        // todo: hey HarrrrrrroooolDDD! Put yah heart baet hereere!!!!
        const heartbeats = $kitchenSync.addSong("heartbeating", "bgm");
        this._scoreManager.setHeartbeats(heartbeats);

        this._soundManager.setMasterSong(masterBuffer);
        this._scoreManager.setSong(masterBuffer);
        $backgroundBuffer = masterBuffer;

        $kitchenSync.addListener(this._scoreManager);
        $kitchenSync.loadBufferList();
    }

    /**
     * Required for kitchensync
     * */
    informLoadComplete() {
        this._isPrepared = true;
    }

    registerChild(child) {
        // todo: verify that listener has receiveMessage(beat)
        this.children.push(child);
    }

    sendMessage(beat) {
        this.children.forEach(function (child) {
            child.receiveMessage(beat);
        });
    }

    isDone() {
        var idx;

        for (idx = 0; idx < this.children.length; idx++) {
            if (!(this.children[idx]).isDone()) {
                return false;
            }
        }

        return true;
    }

    isGameOver() {
        if (!this._isPrepared) {
            console.warn("Checking if game is over when game isn't even prepared yet");
            return false;
        }

        return this._tsc.isGameOver();
    }

    getScoreRate() {
        if (this._scoreManager != null) {
            return this._scoreManager.getScoreRate();
        } else {
            return 0;
        }
    }

    didWePass() {
        if (this._scoreManager != null && this._map != null) {
            return (100 * this._scoreManager.getScoreRate()) >= this._map.getPassRate();
        } else {
            return false;
        }
    }

    clear() {
        this.initialize();
    }

    requestBeatReaction(beat) {
        this._tsc.reactToHold(beat);
    }
}

class MusicTimerCorn extends TimerCorn {
    constructor() {
        super();
        this.offset = 0 + $preferredOffset;
    }

    updateTime() {
        if ($backgroundBuffer !== null && $backgroundBuffer.isPlaying()) {
            this._previousTime = this._currentTime;
            this._currentTime = ($backgroundBuffer.seek() * 1000) + this.offset;
            this._deltaTime = this._currentTime - this._previousTime;
        } else {
            super.updateTime();
        }
    }

    getTime() {
        if ($backgroundBuffer !== null && $backgroundBuffer.isPlaying()) {
            return this._currentTime;
        } else {
            return super.getTime();
        }
    }
}

var $backgroundBuffer = null;
var $preferredOffset = 0;
var $timerCorn = new MusicTimerCorn();
var $kitchenSync = null;
var $tscManager = new TSCManager();