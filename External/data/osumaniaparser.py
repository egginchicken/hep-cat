import math
import json
import jsonpickle

# change this variables:
filename_in = "Dreamy.txt"
filename_out = "Dreamy.json"

class Beat:
    COLUMN_COUNT = 4
    SECTION_SIZE = 512
    INDEX_CEIL = COLUMN_COUNT - 1

    def __init__(self, line):
        # default values for now
        self._tolerance = [
            200,
            500,
            1000
        ]
        self._overdueTime = 1200
        self._tolerableDiff = 1000
        self._pictureName = "BeatCorn"
        self._specialMessage = ""

        # parse
        arr = line.split(",")
        self._time = int(arr[(2)])
        self._endTime = int(arr[5].split(":")[0])

        if (self._endTime == 0):
            self._type = 0
        else:
            self._type = 1
        
        yNum = math.floor(int(arr[0]) * Beat.COLUMN_COUNT / Beat.SECTION_SIZE)

        if (yNum < 0):
            yNum = 0
        elif (yNum > Beat.INDEX_CEIL):
            yNum = 4

        self._modifiers = {}
        self._modifiers["HITMARK_Y_INDEX"] = yNum

class Map:
    def __init__(self, f):
        # default values
        self._tolerance = [
            21,
            102,
            180
        ]

        self._firstBeatOffset = 0
        self._queue = []
        self._difficulty = 0
        self._passRate = [
                50
            ]
        self._screenTime = 5000
        self._hitmarkX = 96
        self._mapEndTime = "YOU HAVE TO SET THIS UP MANUALLY"
        self._songEndTime = "YOU HAVE TO SET THIS UP MANUALLY"
        self._soundEffectList = {
                "ExcellentSound": "Damage2",
                "OkaySound": "Damage1",
                "BadSound": "Cat",
                "MissSound": "Miss"
            }

        beat_flag = False
        
        for line in f:
            if beat_flag and line.strip() != "":
                self._queue.append(Beat(line))
            elif "AudioFilename:" in line:
                self._audioFilename = line.split()[1]
            elif "Title:" in line:
                self._title = ":".join(line.split(":")[1:])
            elif "Artist:" in line:
                self._artist = ":".join(line.split(":")[1:])

            if "[HitObjects]" in line:
                beat_flag = True

class OldCode:
    def test():
        with open("magicalmixer.txt", "r") as f:
            flag = False
            for line in f:
                if flag and line.strip() != "":
                    infoList.append(Info(line))

                if "[HitObjects]" in line:
                    flag = True

        with open("demo1.json", "w") as f:
            f.write("[\n")
            isFirst = True
            for info in infoList:
                if isFirst:
                    isFirst = False
                else:
                    f.write(",\n")
        
                f.write("{{\"_pictureName\":\"BeatCorn\",\"_type\":0,\"_time\":{0},\"_tolerance\":[200,500,1000],\"_overdueTime\":5200,\"_toleableDiff\":1000,\"_modifier\":[{1}],\"_picture\":null,\"_message\":0}}".format(info.time, info.yNum))

            f.write("]")

def parse(filename_in, filename_out):
    map = None

    with open(filename_in, "r") as f:
        map = Map(f)

    with open(filename_out, "w") as f:
        f.write(jsonpickle.encode(map))

parse(filename_in, filename_out)