/*:
 * @plugindesc Tree structured framework for anything
 * @author Allan Manuba
 * 
 * Several hacks to speed up creation:
 * 1. I have to copy paste CornTree classes because you import external modules. I'm not
 * making an app just for this. I don't want to learn all those weird stuff in making it work
 * properly.
 * 2. Why not use python? Well because every change I make in CornTree would have to be
 * reflected here. I might as wellcopy paste the entire thing here.
 */

/* Instruction:
 * @Link: stackoverflow.com/a/41777656
 * @Author: GOTO 0 {stackoverflow.com/users/1083663/goto-0}
1) Install Node.js if you haven't done so yet.

2) Change your converter.js file like this:

function bin_dec(num) {
    return parseInt(num, 2);
}

console.log(bin_dec(process.argv[2]));
3) Open a new command prompt in the folder where your script is and run

$ node converter.js 01001100
*/

/**
 * todo:
 * Parse in RPGMakerMV
 */

// #region Parser
const DataType = {
    SCENE_TREE : 0
}

const SceneTreeProp = {
    SIMPLE: 0,
    EXT_RESOURCE: 1,
    SUB_RESOURCE: 2,
    CHILD: 3,
    NODE: 4
}

// sample
class TreeTest {
    receiveTree(tree) {
        tree.init();
    }
}

class CornNodeParser {
    static sample() {
        var treeTest = new TreeTest();
        var parser = new CornNodeParser();
        parser.parse("sample2", treeTest);
    }

    receiveData(textData) {
        this.treeReceiver.receiveTree(CornNodeParser.createInstanceWithText(textData));
    }

    async parse(filename, treeReceiver) {
        if (typeof treeReceiver.receiveTree !== "function") {
            throw "Cannot receive tree without receiveTree(tree) function";
        }

        this.treeReceiver = treeReceiver;
        JSONUtilDaikon.readGodotTree(filename, this);
    }

    static createInstanceWithText(text) {
        var sceneTree = new CornSceneTree();
        var idx = 0;

        var lineList = text.split("\n")
        for (idx = 0; idx < lineList.length;) {
            idx = CornNodeParser.analyzeLine(sceneTree, lineList, idx);
        }

        return sceneTree;
    }
    
    /**
     * 
     * @param {CornSceneTree} sceneTree
     * @param {Array} lineList
     * Array of {String}
     * @param {int} idx
     */
    static analyzeLine(sceneTree, lineList, idx) {
        var firstLine = lineList[idx];
        idx++;
        var child;

        if (firstLine.startsWith("[gd_scene "))
        {
            firstLine = firstLine.replace("[gd_scene ", "");
            var propList = firstLine.split(" ");
            propList.forEach(function (line) {
                CornNodeParser.extractProperty(
                    sceneTree,
                    line,
                    SceneTreeProp.SIMPLE);
            });
        }
        else if (firstLine.startsWith("[ext_resource "))
        {
            firstLine = firstLine.replace("[ext_resource ", "");
            CornNodeParser.extractProperty(
                sceneTree,
                firstLine,
                SceneTreeProp.EXT_RESOURCE);
        }
        else if (firstLine.startsWith("[sub_resource ")) {
            firstLine = firstLine.replace("[sub_resource ", "");

            child = CornNodeParser.extractProperty(
                sceneTree,
                firstLine,
                SceneTreeProp.SUB_RESOURCE);

            var pair = CornNodeParser.specifyResource(sceneTree, child, lineList, idx);
            var specificObject = pair[0];
            idx = pair[1];

            sceneTree.sub_resource_list.push(specificObject);
        } else if (firstLine.startsWith("[node ")) {
            firstLine = firstLine.replace("[node ", "");
            firstLine = firstLine.replace("]", "");

            child = CornNodeParser.extractProperty(
                sceneTree,
                firstLine,
                SceneTreeProp.NODE);

            var pair = CornNodeParser.specifyNode(sceneTree, child, lineList, idx);
            var specificObject = pair[0];
            idx = pair[1];

            sceneTree.addNode(specificObject.name, specificObject);
        }

        return idx;
    }

    /**
     *
     * @param {CornSceneTree} sceneTree
     * @param {CornResource} nodeObj
     * @param {Array} lineList
     * Array of {String}
     * @param {int} idx
     */
    static specifyNode(sceneTree, nodeObj, lineList, idx) {
        // todo: specifications go here for sub resources
        var specObj = new CornResource();
        var locIdx;
        const STATE = {
            SIMPLE: 0,
            NESTED: 1
        };
        var currentState = STATE.SIMPLE;
        var lineCache = "";

        switch (nodeObj.type) {
            //case "AnimationNodeAnimation":
            //    specObj = new AnimationNodeAnimation();
            //    CornNodeParser.extractProperty(specObj, lineList[idx], SceneTreeProp.SIMPLE);
            //    idx++;
            //    break;

            case "Node2D":
                specObj = new CornNode2D();
                break;

            case "Node":
                console.warn("We might break here");
                specObj = new CornNode();
                break;

            case "AnimationTree":
                specObj = new CornAnimationTree();
                for (locIdx = 0; idx + locIdx < lineList.length && lineList[locIdx + idx].trim() !== ""; locIdx++) {
                    specObj.startAssignment(lineList[locIdx + idx], [sceneTree]);
                }
                break;

            case "AnimationPlayer":
                specObj = new AnimationPlayer();
                for (locIdx = 0; idx + locIdx < lineList.length && lineList[locIdx + idx].trim() !== ""; locIdx++) {
                    specObj.startAssignment(lineList[locIdx + idx], [sceneTree]);
                }
                break;

            case "Sprite":
                specObj = new CornSprite();
                for (locIdx = 0; idx + locIdx < lineList.length && lineList[locIdx + idx].trim() !== ""; locIdx++) {
                    specObj.startAssignment(lineList[locIdx + idx], [sceneTree]);
                }
                break;

            default:
                console.error("Object not yet specified: " + nodeObj.type);
                specObj = new CornNode();
                break;
        }


        specObj.getProperties(nodeObj);
        return [specObj, idx];
    }

    /**
     *
     * @param {CornSceneTree} sceneTree
     * @param {CornResource} resObj
     * @param {Array} lineList
     * Array of {String}
     * @param {int} idx
     */
    static specifyResource(sceneTree, resObj, lineList, idx) {
        // todo: specifications go here for sub resources
        var specObj = new CornResource();
        var locIdx;
        const STATE = {
            SIMPLE: 0,
            NESTED: 1
        };
        var currentState = STATE.SIMPLE;
        var lineCache = "";

        switch (resObj.type) {
            // #region Resources
            case "AnimationNodeAnimation":
                specObj = new AnimationNodeAnimation();
                CornNodeParser.extractProperty(specObj, lineList[idx], SceneTreeProp.SIMPLE);
                idx++;
                break;

            case "AnimationNodeBlend2":
                specObj = new AnimationNodeBlend2();
                break;

            case "AnimationNodeTransition":
                specObj = new AnimationNodeTransition();
                for (locIdx = 0; idx + locIdx < lineList.length && lineList[locIdx + idx].trim() !== ""; locIdx++) {
                    specObj.startAssignment(lineList[locIdx + idx], [sceneTree]);
                }
                break;

            case "AnimationNodeBlendTree":
                specObj = new AnimationNodeBlendTree();
                for (locIdx = 0; idx + locIdx < lineList.length && lineList[locIdx + idx].trim() !== ""; locIdx++) {
                    specObj.startAssignment(lineList[locIdx + idx], [sceneTree]);
                }
                break;

            case "Animation":
                specObj = new CornAnimation();
                for (locIdx = 0; idx + locIdx < lineList.length && lineList[locIdx + idx].trim() !== ""; locIdx++) {
                    var currentLine = lineList[locIdx + idx];
                    if (currentLine.includes(" = {")) {
                        currentState = STATE.NESTED;
                        lineCache = currentLine;
                    } else if (currentState == STATE.NESTED && currentLine.startsWith("}")) {
                        currentState = STATE.SIMPLE;
                        lineCache += "\n" + currentLine;
                        specObj.startAssignment(lineCache, [sceneTree]);
                        lineCache = "";
                    } else if (currentState == STATE.NESTED) {
                        lineCache += "\n" + currentLine;
                    } else {
                        specObj.startAssignment(currentLine, [sceneTree]);
                    }



                }
                break;
            // #endregion Resources

            default:
                console.warn("Object not yet specified: " + resObj.type);
                specObj = new AnimationNode();

                break;
        }


        specObj.getProperties(resObj);
        return [specObj, idx];
    }

    /**
     * 
     * @param {Object} obj
     * This object may of type {CornSceneTree}, or {CornResource}
     * @param {string} propertyText
     * @param {SceneTreeProp} treeProp
     */
    static extractProperty(obj, propertyText, treeProp) {
        var propertyList;
        var res;
        var propKey = "ERROR";
        var propValue = "ERROR";

        switch (treeProp) {
            case SceneTreeProp.SIMPLE:
                propertyList = propertyText.replace("]", "").split("=");
                obj[propertyList[0].trim()] = propertyList[1].replace(/"|res:\/\/|.png/g, "").trim();
                return null;

            case SceneTreeProp.EXT_RESOURCE:
                res = new CornResource();
                res.resourceType = ResourceType.EXT;
                propertyList = propertyText.replace("]", "").split(" ");
                propertyList.forEach(function (line) {
                    CornNodeParser.extractProperty(
                        res,
                        line,
                        SceneTreeProp.SIMPLE);
                });
                obj.ext_resource_list.push(res);
                return null;

            case SceneTreeProp.SUB_RESOURCE:
                res = new CornResource();
                res.resourceType = ResourceType.SUB;
                propertyList = propertyText.replace("]", "").split(" ");
                propertyList.forEach(function (line) {
                    CornNodeParser.extractProperty(
                        res,
                        line,
                        SceneTreeProp.SIMPLE);
                });
                return res;

            case SceneTreeProp.CHILD:
                obj.startAssignment(propertyText);
                break;

            case SceneTreeProp.NODE:
                var node = new CornNode();
                propertyList = propertyText.replace("]", "").split(" ");
                propertyList.forEach(function (line) {
                    CornNodeParser.extractProperty(
                        node,
                        line,
                        SceneTreeProp.SIMPLE);
                });
                return node;

            default:
                throw "Unknown propertyType: " + treeProp.toString();
        }

        return null;
    }
}
// #endregion Parser

// #region CornTree Framework
class TreeReceiver {
    receiveTree(tree) {
        tree.init();
    }
}

/**
 * Step 1: Load tree
 * Step 2: Wait until isPrepared
 * Step 3: Create $messageHandler
 * Step 4: Call start
 * Step 5: Check for end updates or something
 * Step 6: Do a clear for CornSceneTree
 * Step 7: nullify $messageHandler
 * */
class CornTreeFramework {
    static loadTree(name) {
        CornTreeFramework.receiver = new TreeReceiver();
        var parser = new CornNodeParser();
        parser.parse(name, CornTreeFramework.receiver);
    }

    static isPrepared() {
        if (CornSceneTree.prepared) {
            CornTreeFramework.receiver = null;
        }

        return CornSceneTree.prepared;
    }

        // only call this if the CornSceneTree.prepared is true
    static start() {
        CornSceneTree.isDone = false;
        $treeInstance.start();
        console.log($treeInstance);
    }

    static update() {
        $cornMessenger.mainPostMessage(FromMainMessage.CallLoop);
        //CornSceneTree.isDone = true;
    }

    static isDone() {
        return CornSceneTree.isDone;
    }

    static clear() {
        $treeInstance.clear();
        $treeInstance = null;
        CornSceneTree.prepared = false;
        CornSceneTree.isDone = false;
    }
}
// #endregion

// #region Copy Paste Tree

// #region CornTree Core
class CornObject {
    // #region Leave as-is
    constructor() {
        this.cornType = "CornObject";
        this.isDirty = true;
    }

    startAssignment(propertyText, extraArg = []) {
        var pair = CornObject.prepareAssignmentPair(propertyText);
        this.findAssignment(pair, extraArg);
    }

    /**
     * 
     * @param {Array} assignmentPair
     * index 0: {CornPath} path to property
     * index 1: {string} value of property
     */
    findAssignment(assignmentPair, extraArg = []) {
        var path = assignmentPair[0];

        if (path.getSize() > 1) {
            var target = path.poll();
            if (!this.hasOwnProperty(target)) {
                this.setDefaultCornProperty(target);
            }

            this[target].findAssignment(assignmentPair, extraArg);
        } else if (path.getSize() == 1) {
            this.setPropertyViaPair(assignmentPair, extraArg);
        } else {
            throw "Path is empty: " + assignmentPair;
        }
    }

    /**
     * 
     * @param {String} text
     * @returns {Array} cornPath
     * {{CornPath}, {string}}
     */
    static prepareAssignmentPair(text) {
        var pair = text.split("=");
        var value;

        if (pair[1].includes("[") || pair[1].includes[")"]) {
            // this implies that it is special
            value = pair[1].trim();
        } else {
            value = pair[1].replace(/"|res:\/\//g, "").trim();
        }

        return [CornPath.createInstanceViaString(pair[0]), value];
    }

    getProperties(other) {
        for (var prop in other) {
            this[prop] = other[prop];
        }
    }
    // #endregion Leave as-is

    // #region Things you may need to override
    /**
     * 
     * @param {String} key
     * Usually to be overriden if you want to use this
     */
    setDefaultCornProperty(key) {
        throw `This object {${typeof this}} has no CornObjectProperty with key ${key}`;
        // this[key] = new CornObject();
    }

    /**
     * 
     * @param {any} value
     * Usually to be overriden, unless you want a String value;
     * Always call super
     */
    setPropertyViaPair(pair, extraArg = []) {
        this[pair[0].poll()] = pair[1];
    }

    update(deltaTime) {

    }
    // #endregion
}

/**
 * Don't use primitive types for our structure to work.
 * */
class CornArray extends CornObject {
    // #region Leave as-is
    constructor() {
        super();
        this.cornType = "CornArray";
        this.array = [];
    }

    poll() {
        return this.array.shift();
    }

    peek() {
        return this.array[this.array.length - 1];
    }

    push(item) {
        this.array.push(item);
    }

    getSize() {
        return this.array.length;
    }

    static cornifyArray(arr) {
        var cornArr = new CornArray();
        cornArr.array = arr;
        return cornArr;
    }

    getValue(idx) {
        return this.array[idx];
    }
    /**
     * 
     * @param {Array} assignmentPair
     * index 0: {CornPath} path to property
     * index 1: {string} value of property
     */
    findAssignment(assignmentPair, extraArg = []) {
        var path = assignmentPair[0];

        if (path.getSize() > 1) {
            const idx = parseInt(assignmentPair[0].poll());

            if (isNaN(idx)) {
                throw "CornArray should only receive int as path. Use CornHashMap instead.";
            } else if (idx < this.array.length) {
                // nothing
            } else if (idx == this.array.length) {
                this.setDefaultCornProperty(idx);
            } else {
                throw `Index exceeds what is expected: (${idx} > ${this.array.length})`;
            }

            this.getValue(idx).findAssignment(assignmentPair, extraArg);
        } else if (path.getSize() == 1) {
            this.setPropertyViaPair(assignmentPair, extraArg);
        } else {
            throw "Path is empty: " + assignmentPair;
        }
    }
    // #endregion Leave as-is
    /**
     * 
     * @param {String} key
     * Usually to be overriden if you want to use this
     */
    setDefaultCornProperty(key) {
        this.array.push("");
    }

    /**
     * 
     * @param {any} pair
     * @param {Array} extraArg
     */
    setPropertyViaPair(pair, extraArg = []) {
        const idx = parseInt(pair[0].poll());

        if (Number.isNan(idx)) {
            throw "CornArray should only receive int as path. Use CornHashMap instead.";
        } else if (idx < this.array.length) {
            this.array[idx] = this.convertValue(pair[1]);
        } else if (idx == this.array.length) {
            this.array.push(this.convertValue(pair[1]));
        } else {
            throw `Index exceeds what is expected: (${idx} > ${this.array.length})`;
        }
    }

    /**
     * 
     * @param {any} value
     * Override this when you don't want an array of String
     */
    convertValue(value) {
        return value;
    }
}

class CornHashMap extends CornObject {
    // #region Leave as-is
    constructor() {
        super();
        this.cornType = "CornHashMap";
        this.map = new Map();
    }

    getSize() {
        return this.map.length;
    }

    getValue(key) {
        return this.map.get(key);
    }

    setValue(key, value) {
        this.map.set(key, value);
    }

    has(key) {
        return this.map.has(key);
    }
    /**
     * 
     * @param {Array} assignmentPair
     * index 0: {CornPath} path to property
     * index 1: {string} value of property
     */
    findAssignment(assignmentPair, extraArg = []) {
        var path = assignmentPair[0];

        if (path.getSize() > 1) {
            var target = path.poll();
            if (!this.has(target)) {
                this.setDefaultCornProperty(target);
            }
            this.getValue(target).findAssignment(assignmentPair, extraArg);
        } else if (path.getSize() == 1) {
            this.setPropertyViaPair(assignmentPair, extraArg);
        } else {
            throw "Path is empty: " + assignmentPair;
        }
    }
    // #endregion
    /**
     * 
     * @param {String} key
     * Usually to be overriden if you want to use this
     */
    setDefaultCornProperty(key) {
        this.map.set(key, "");
    }

    setPropertyViaPair(pair, extraArg = []) {
        this.map.set(pair[0], this.convertValue(pair[1]));
    }

    /**
     * 
     * @param {any} value
     * Override this when you don't want an array of String
     */
    convertValue(value) {
        return value;
    }
}

class CornPath extends CornObject {
    constructor() {
        super();
        this.pathList = new CornArray();
    }

    poll() {
        return this.pathList.poll().trim();
    }

    peek() {
        return this.pathList.peek().trim();
    }

    getSize() {
        return this.pathList.getSize();
    }

    static createInstanceViaString(stringPath) {
        var path = new CornPath();
        path.pathList = CornArray.cornifyArray(stringPath.trim().replace(/"/g, "").split("/"));
        return path;
    }

    static scriptToPath(scriptText) {
        var path = new CornPath();
        scriptText = scriptText.trim();
        scriptText = scriptText.replace("NodePath(", "");
        scriptText = scriptText.replace(")", "");
        var initList = scriptText.split("/");
        var postList = initList.pop().split(":");
        path.pathList.array = initList.concat(postList);
        return path;
    }
}

class Vector2 extends CornObject {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }

    percentify() {
        this.x = this.x * 100;
        this.y = this.y * 100;
    }

    static scriptToVector(scriptText) {
        var v2 = new Vector2();
        scriptText = scriptText.trim();
        scriptText = scriptText.replace("Vector2( ", "");
        scriptText = scriptText.replace(" )", "");
        var pair = scriptText.split(", ");
        v2.x = pair[0];
        v2.y = pair[1];
        return v2;
    }

    setPropertyViaPair(pair, extraArg = []) {
        this[pair[0].poll()] = parseInt(pair[1]);
    }
}
// #endregion CornTreeComponents Core

/**
 * Based on: sitepoint.com/quick-tip-game-loop-in-javascript
 * */
class MainLoop extends CornObject {
    constructor() {
        super();
        this.isLocked = true;
        this.timestamp = 0;
        this.deltaTime = 0;
        this.lastRender = 0;
    }

    start() {
        this.lastRender = (new Date()).getTime();
        this.isLocked = false;
        this.loop();
    }

    // don't call outside
    updateLoop() {

    }

    // don't call outside
    draw() {

    }

    loop() {
        if (this.isLocked) {
            return;
        }

        try {
            this.isLocked = true;
            this.timestamp = (new Date()).getTime();
            this.deltaTime = (this.timestamp - this.lastRender);

            this.updateLoop();
            this.draw();

            this.lastRender = this.timestamp;
        } finally {
            this.isLocked = false;
        }
    }

    stopLoop() {
        this.shouldLoop = false;
    }
}

/**
 * Assumptions:
 * Root node is first node in node map
 * */
class CornSceneTree extends MainLoop {
    constructor() {
        super();

        this.load_steps = 0;
        this.format = 2;
        this.ext_resource_list = [];
        this.sub_resource_list = [];
        this.node_map = new CornHashMap();
        this.root = null;
    }

    getExtResource(id) {
        // todo: add validation
        // warn: consider that scene trees start with index 1? What's 0???
        return this.ext_resource_list[id - 1];
    }

    getSubResource(id) {
        // todo: add validation
        // warn: consider that scene trees start with index 1? What's 0???
        return this.sub_resource_list[id - 1];
    }

    fixTypes() {
        // todo: fix types for each property after parsing
    }

    scriptToResource(textScript) {
        if (textScript.startsWith("SubResource")) {
            return this.getSubResource(parseInt(textScript.split(" ")[1]));
        } else {
            return this.getExtResource(parseInt(textScript.split(" ")[1]));
        }
    }

    addNode(name, node) {
        this.node_map.setValue(name, node);
    }

    findAllChildren(childNode) {
        if (childNode.parent !== null) {
            const parentPath = childNode.parent.split("/");
            const parentKey = parentPath[parentPath.length - 1];
            CornSceneTree.addChild(parentKey, childNode);
        } else {
            CornSceneTree.setRootNode(childNode);
        }
    }

    initNodes() {
        // todo find all children for each node
        this.node_map.map.forEach(this.findAllChildren);
    }

    init() {
        $treeInstance = this;
        this.initNodes();
        $cornMessenger.start();
        $cornMessenger.mainPostMessage(FromMainMessage.Initialize, [this]);
    }

    start() {
        $cornMessenger.mainPostMessage(FromMainMessage.StartLoop);
    }

    // for consistency
    clear() {
        $cornMessenger.stop();
    }

    addChild(parentKey, childNode) {
        if (parentKey !== ".") {
            this.node_map.getValue(parentKey).addChild(childNode);
        } else {
            this.root.addChild(childNode);
        }
    }

    static addChild(parentKey, childNode) {
        $treeInstance.addChild(parentKey, childNode);
    }

    static setRootNode(node) {
        $treeInstance.root = node;
    }
}

// #region CornSceneTree global information
var $treeInstance = null;
CornSceneTree.prepared = false;
CornSceneTree.isDone = false;
CornSceneTree.workerName = "cornflour/workers/CornTreeWorker.js";
// #endregion CornSceneTree global information

// #region CornTree Components
const ResourceType = {
    EXT: 0,
    SUB: 1
}

/**
 * @property {String} path filename of resource
 * Stick to RPGMakerMV Standards for this one
 * todo: List resource standards here
 * @property {String} path
 * @property {String} type
 * @property {int} id
 * @property {ResourceType} resourceType
 * */
class CornResource extends CornObject {
    constructor() {
        super();
        this.path = "";
        this.type = "";
        this.id = 0;
        this.resourceType = ResourceType.EXT;
    }

    fixTypes() {
        // todo: fix types for each property after parsing
    }
}

class CornFloatArray extends CornArray {
    constructor() {
        super();
        // todo: do the conversion
    }

    static scriptToPoolArray(scriptText) {
        var floatArray = new CornFloatArray();
        scriptText = scriptText.trim();
        scriptText = scriptText.replace("PoolRealArray(", "[");
        scriptText = scriptText.replace(")", "]");
        floatArray.array = JSON.parse(scriptText);
        return floatArray;
    }
}

class CornRefArray extends CornArray {
    constructor() {
        super();
        // todo: do the conversion
    }

    static scriptToRefArray(scriptText, sceneTree) {
        var refArray = new CornFloatArray();
        scriptText = scriptText.replace(/"| |ExtResource|\(|\)|\[|\]/g, "");
        scriptText.split(",").forEach(function (stringValue) {
            refArray.array.push(sceneTree.getExtResource(parseInt(stringValue)));
        });
        return refArray;
    }
}

class KeyFrames extends CornObject {
    constructor() {
        super();
        // guess: x0, y0, yc, x1, y1
        this.points = new CornFloatArray();
        // are the x values of the center of the bezier handle
        this.times = new CornFloatArray();

        // different properties for textures??
        this.transitions = new CornFloatArray();
        this.updateLoop = 1;
        this.values = new CornRefArray();
    }

    static scriptToKeyFrames(scriptText, sceneTree) {
        var keyFrames = new KeyFrames();
        scriptText = scriptText.trim();

        // parse
        scriptText.split("\n").forEach(function (line) {
            // get key
            line = line.trim();
            if (line.length < 2) {
                return;
            }

            var pair = line.split(":");
            var key = pair[0].replace(/"/g, "").trim();

            // warning: you have to individually remove the last comma
            var value = pair[1];
            if (value[value.length - 1] == ",") {
                value = value.slice(0, -1);
            }

            switch (key) {
                case "times":
                case "points":
                case "transitions":
                    keyFrames[key] = CornFloatArray.scriptToPoolArray(value);
                    break;

                case "update":
                    value = parseInt(value);
                    keyFrames[key] = value;
                    break;

                case "values":
                    keyFrames[key] = CornRefArray.scriptToRefArray(value, sceneTree);
                    break;

                default:
                    break;
            }

        });
        return keyFrames;
    }

}

class CornAnimationTrack extends CornObject {
    constructor() {
        super();
        this.type = "bezier";
        this.path = null; // NodePath todo
        this.interp = 1;
        this.loop_wrap = true;
        this.imported = false;
        this.enabled = true;
        this.keys = new KeyFrames();
    }

    setPropertyViaPair(pair, extraArg = []) {
        const key = pair[0].peek();

        switch (key) {
            case "interp":
                this[key] = parseInt(pair[1]);
                break;

            case "loop_wrap":
            case "imported":
            case "enabled":
                this[key] = (pair[1] == "true");
                break;

            case "keys":
                extraArg.push("KeyFrames");
                this[key] = ScriptToObjectUtil.run(pair[1], extraArg);
                break;

            case "path":
                this[key] = ScriptToObjectUtil.run(pair[1], extraArg);
                break;

            default:
                super.setPropertyViaPair(pair, extraArg);
                break;
        }
    }
}

class CATArray extends CornArray {
    constructor() {
        super();
        // todo: convert to CornAnimationTrack
    }

    setDefaultCornProperty(key) {
        this.array.push(new CornAnimationTrack());
    }
}

class CornAnimation extends CornResource {
    constructor() {
        super();
        this.id = 0;
        this.resource_name = "";
        this.length = 1.0;
        this.tracks = new CATArray();
    }
}
// #endregion CornTree Components

// #region Animation Nodes
class AnimationNode extends CornResource {
    constructor() {
        super();
    }
}

class AnimationNodeAnimation extends AnimationNode {
    constructor() {
        super();
        this.animation = "";
    }
}

class AnimationNodeBlend2 extends AnimationNode {
    constructor() {
        super();
    }
}

class TransitionInput extends CornObject {
    constructor() {
        super();
        this.name = "";
        this.auto_advance = false;
    }

    setPropertyViaPair(pair, extraArg = []) {
        const key = pair[0].peek();

        switch (key) {
            case "auto_advance":
                this[key] = ("true" == pair[1]);
                break;
            default:
                super.setPropertyViaPair(pair, extraArg);
                break;
        }
    }
}

class AnimationNodeTransition extends AnimationNode {
    constructor() {
        super();
        this.input_count = 0;
    }

    setDefaultCornProperty(key) {
        if (key.startsWith("input_")) {
            this[key] = new TransitionInput();
        } else {
            super.setDefaultCornProperty(key);
        }
    }

    setPropertyViaPair(pair, extraArg = []) {
        const key = pair[0].peek();

        switch (key) {
            case "input_count":
                this[key] = parseInt(pair[1]);
                break;
            default:
                super.setPropertyViaPair(pair, extraArg);
                break;
        }
    }
}

class BlendTreeItem extends CornObject {
    constructor() {
        super();
        this.node = null;
        this.position = null;
    }

    setPropertyViaPair(pair, extraArg = []) {
        const key = pair[0].peek();

        switch (key) {
            case "node":
                this[key] = ScriptToObjectUtil.run(pair[1], extraArg);
                break;
            default:
                super.setPropertyViaPair(pair, extraArg);
                break;
        }
    }
}

BlendTreeItem.ARG_SCENE_TREE = 0;

class BlendTreeMap extends CornHashMap {
    constructor() {
        super();
    }

    setDefaultCornProperty(key) {
        this.map.set(key, new BlendTreeItem());
    }
}

class AnimationNodeBlendTree extends AnimationNode {
    constructor() {
        super();
        this.graph_offset = null;
        this.nodes = new BlendTreeMap();
        this.node_connections = new CornArray();
    }

    setPropertyViaPair(pair, extraArg = []) {
        const key = pair[0].peek();

        switch (key) {
            case "graph_offset":
                this[key] = ScriptToObjectUtil.run(pair[1]);
                break;
            case "node_connections":
                this[key] = JSON.parse(pair[1]);
                break;
            default:
                super.setPropertyViaPair(pair, extraArg);
                break;
        }
    }
}
// #endregion

// #region Node
class CornNode extends CornObject {
    constructor() {
        super();
        this.name = "";
        this.type = "";

        // if null, it is root
        this.parent = null;
        this.childrenArray = new CornArray();
    }

    update(deltaTime) {
        super.update();
        this.updateChildren();
    }

    updateChildren() {
        this.childrenArray.array.forEach(function (element) {
            element.update();
        });
    }

    addChild(childNode) {
        this.childrenArray.push(childNode);
    }
}

class CornCanvasItem extends CornNode {
    constructor() {
        super();
    }
}

class CornNode2D extends CornCanvasItem {
    constructor() {
        super();
    }
}

class CornState extends CornObject {
    constructor() {
        super();
        this.current = 0;
    }
}

class CornBlend2 extends CornObject {
    constructor() {
        super();
        this.blend_amount = 0;
    }
}

class AnimationTreeParams extends CornObject {
    constructor() {
        super();
        this.Blend2 = new CornObject();
        this.State = new CornState();
    }
}

class CornAnimationTree extends CornNode {
    constructor() {
        super();
        this.tree_root = null;
        this.anim_player = null;
        this.active = true;
        this.parameters = new AnimationTreeParams();
    }
}

class AnimList extends CornHashMap {
    constructor() {
        super();
    }

    setPropertyViaPair(pair, extraArg) {
        this.setValue(pair[0].peek(), ScriptToObjectUtil.run(pair[1], extraArg));
    }
}

class AnimationPlayer extends CornNode {
    constructor() {
        super();
        this.anims = new AnimList();
    }
}

class CornSprite extends CornNode2D {
    constructor() {
        super();
        this.position = null;
        this.scale = null;
        this.texture = null;
        this.isVisible = true;
    }

    setPropertyViaPair(pair, extraArgs) {
        const key = pair[0].peek(0);

        switch (key) {
            case "position": case "scale": case "texture":
                this[key] = ScriptToObjectUtil.run(pair[1], extraArgs);

                if (key == "scale") {
                    this[key].percentify();
                }

                break;

            default:
                super.setPropertyViaPair(pair, extraArgs);
                break;
        }
    }
}
// #endregion

// #endregion Copy Paste Tree

// #region Util
class ScriptToObjectUtil {
    constructor() {
        throw "Do not instantiate static class";
    }

    /**
     * 
     * @param {String} scriptText
     * @param {Array} extraArg
     * 0: should be SceneTree
     * 1: should be a String to represent what type of object is to be created
     */
    static run(scriptText, extraArg = []) {
        scriptText = scriptText.trim();

        if (scriptText.startsWith("Vector2")) {
            return Vector2.scriptToVector(scriptText);
        }
        else if (scriptText.startsWith("SubResource")) {
            return extraArg[0].scriptToResource(scriptText);
        }
        else if (scriptText.startsWith("ExtResource")) {
            return extraArg[0].scriptToResource(scriptText);
        }
        else if (scriptText.startsWith("NodePath")) {
            return CornPath.scriptToPath(scriptText);
        }
        else if (scriptText.startsWith("{")) {
            // check extra arg for what type of object to expect

            switch (extraArg[1]) {
                case "KeyFrames":
                    return KeyFrames.scriptToKeyFrames(scriptText, extraArg[0]);
                default:
                    throw "Unknown type: " + extraArg[1];
            }
        }
        else {
            throw "Unknown constructor script: " + scriptText;
        }
    }
}
// #endregion

// #region Message handler

// #region Copy Paste
class SubjectWrapper {
    constructor(subject, keyArg = []) {
        this.subject = subject;
        this.keyArg = keyArg;
    }
}

const FromMainMessage = {
    Initialize: new SubjectWrapper("Initialize", ["Tree"]),
    StartLoop: new SubjectWrapper("StartLoop"),
    CallLoop: new SubjectWrapper("CallLoop")
}

const FromWorkerMessage = {
    InitializeFinished: new SubjectWrapper("InitializeFinished"),
    CornSprite: new SubjectWrapper("CornSprite", ["Arg"]),
    End: new SubjectWrapper("End")
}

Object.freeze(FromWorkerMessage);
Object.freeze(FromMainMessage);

/**
 * Functions are not transferred to worker
 * */
class CornMessageWrapper {
    constructor() {
        this.subject = "";
        this.body = new Map();
    }
}

class MainMessenger {
    constructor() { }

    receiveMessage(event) {
        throw "Override this";
    }

    postMessage(subject, extraArg = []) {
        throw "Override this";
    }
}
// #endregion Copy Paste

class CornMessenger extends MainMessenger {
    constructor() {
        super();
        if (!window.Worker) {
            throw "Sorry, web worker is not working :((\nCannot play game.";
        }
    }

    start() {
        this.worker = new Worker(CornSceneTree.workerName);
        this.worker.onmessage = function (event) {
            if (event !== undefined) {
                // make sure message handler is not null
                $cornMessenger.receiveMessage(event);
            }
        };
    }

    stop() {
        // todo stop running
        this.worker.terminate();
        this.worker = null;
    }

    receiveMessage(event) {
        switch (event.data.subject.subject) {
            case FromWorkerMessage.InitializeFinished.subject:
                CornSceneTree.prepared = true;
                break;

            case FromWorkerMessage.End.subject:
                CornSceneTree.isDone = true
                break;

            case FromWorkerMessage.CornSprite.subject:
                const sprite = event.data.body.get("Arg");
                $pictureManagerCorn.processSprite(sprite);
                break;

            default:
                throw event.data.subject;
        }
    }

    mainPostMessage(subject, extraArg = []) {
        const message = new CornMessageWrapper();
        message.subject = subject;

        var idx;
        for (idx = 0; idx < extraArg.length; idx++) {
            message.body.set(subject.keyArg[idx], extraArg[idx]);
        }

        this.worker.postMessage(message);
    }
}

var $cornMessenger = new CornMessenger();
// #endregion Message handler