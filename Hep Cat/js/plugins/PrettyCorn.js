/*:
 * @plugindesc Helps with handling picture pooling. Do NOT use with RPGMaker's show picture.
 * @author Allan Manuba
 *
 * @help This plugin does not provide plugin commands.
 *
 * todo:
 * Fix graphics
 * Don't use picture corn when in battle
 * override rpg object for game screen to give warning when using this
 * Animation?
 * Fade in entry and exit?
 * Erase all stuff when transferring.
 * 
 * Errors: manually remove png name in godot tree???
 */

// constants
'use strict';
const NO_FREE_ID = -2;
const NOT_VISIBLE_ID = -1;

const ORIGIN = {
    UPPER_LEFT : 0,
    CENTER : 1
}

const BLEND_MODE = {
    NORMAL : 0,
    ADDITIVE : 1,
    MULTIPLY : 2,
    SCREEN : 3
}

// This doesn't apply anymore 
// todo: remove all references
// this our my recommended z-value, please make them consistent
// or our picture manager's gonna slow down
// use other values ONLY when needed
// this makes everything faster when I optimize this later
// todo: optimize list
const DEFAULT_Z = {
    BACKGROUND : -100,
    NORMAL : 0,
    FOREGROUND : 100
}

class PrettyCorn {
    constructor() {
    }

    static test() {
        var character = new Game_Character();
        var sprite = new Sprite_Character(character);
        var bitmap = ImageManager.loadCharacter("Actor2", null);
        sprite._bitmap = bitmap;
        //sprite.transform.position._x = 2;
        //sprite.transform.position._y = 2;
        //sprite.transform.scale._y = 2;
        sprite.scaleX = 200;
        SceneManager._scene._spriteset._characterSprites.push(sprite);
        console.log($gamePlayer);
        console.log(sprite);
    }

    static test2() {
        console.log($dataAnimations);
    }
}

class CornQueue {
    constructor() {
        this.initialize();
    }

    initialize() {
        this._lock = false;
        this._pictureList = null;
        this._delay = 1000 / 60;
        this._isFirst = true;
        this._considerDepth = true;
    }

    safetyCheck() {
        // optimization
        if (!(this._pictureList != null)) {
            const temp = new PictureCorn("testcat");
            temp.setZ(-100000000);
            $gameScreen._pictures = [temp];
            this._pictureList = $gameScreen._pictures;
        }
    }

    waitForLock() {
        setTimeout(function(){}, this._delay);
        this._lock = true;
    }

    _erase(pictureCorn) {
        const ind = this._pictureList.indexOf(pictureCorn);

        if (ind != -1) {
            this._pictureList.splice(ind, 1);
        } else {
            console.error("picture corn not found ://");
        }

        pictureCorn._erase();
    }

    _show(pictureCorn) {
        const nPictures = this._pictureList.length;
        if (!this._considerDepth || nPictures == 0 ||
            this.getPictureZ(nPictures - 1) <= pictureCorn.getZ()) 
        {
            /*
             * if you don't care about arrangements
             * if the picture size is empty
             * if the first picture has a smaller size than the current picture
             */
            this._pictureList.push(pictureCorn);
        } 
        else if (this.getPictureZ(0) >= pictureCorn.getZ())
        {
            this._pictureList.unshift(pictureCorn);
        }
        else
        {
            // value is in between...
            var ind;
            for(ind = 1; ind < nPictures; ind++) {
                if (this.getPictureZ(ind) >= pictureCorn.getZ()) {
                    this._pictureList.splice(ind, 0, pictureCorn);
                    break;
                }
            }

            if (ind >= nPictures) {
                console.error("Unhandled case: this shouldn't happen");
                this._pictureList.push(pictureCorn);
            }
        }

        if (nPictures >= 100) {
            console.warn("RPGMaker MV's picture limit exceeded.\nThis plugin ignores this limit.");
        }
    }

    getPictureZ(ind) {
        if (this._pictureList[ind]._z === null) {
            this._pictureList[ind]._z = 0;
        }

        return this._pictureList[ind]._z;
    }

    clear() {
        $gameScreen._pictures = [];
    }

    sort() {
        $gameScreen._pictures.sort(this.compare);
    }

    compare(a, b) {
        if (!("_z" in a)) {
            a._z = 0;
        }

        if (!("_z" in b)) {
            b._z = 0;
        }

        return a._z - b._z;
    }
}

class PictureManagerCorn {
    constructor() {
        this.initialize();
    }

    initialize() {
        this._considerDepth = true;
        this._pictureList = new CornQueue();
        this.pictureMap = new Map();
    }

    shouldConsiderDepth(should) {
        this._considerDepth = should;

        // TODO: if turning on, always reshuffle...
    }

    async _erase(pictureCorn) {
        this._pictureList.safetyCheck();
        this._pictureList._erase(pictureCorn);
    }

    async _show(pictureCorn) {
        this._pictureList.safetyCheck();
        this._pictureList._show(pictureCorn);
    }

    async sortPictureList() {
        this._pictureList.sort();
    }

    clear() {
        this._pictureList.clear();
    }

    async processSprite(cornSprite) {
        var pictureRef = this.pictureMap.get(cornSprite.name);
        if (cornSprite.isVisible) {
            if (pictureRef === undefined) {
                pictureRef = new PictureCorn(cornSprite.texture.path);
                pictureRef.show(ORIGIN.UPPER_LEFT, cornSprite.position.x, cornSprite.position.y,
                    cornSprite.scale.x, cornSprite.scale.y, 255, BLEND_MODE.NORMAL);
            } else {
                pictureRef.set(cornSprite.texture.path, ORIGIN.UPPER_LEFT, cornSprite.position.x, cornSprite.position.y,
                    cornSprite.scale.x, cornSprite.scale.y, 255, BLEND_MODE.NORMAL, [0, 0, 0, 0], 0);
            }
        } else if (pictureRef !== null) {
            pictureRef.erase();
            this.pictureMap.delete(pictureRef);
        }
    }
}

// closer to the screen => 0
// further to the screen => the bigger the number
// more of a background => the bigger the number
// note: RPGMaker MV uses targetX; don't use it
class PictureCorn extends Game_Picture {
    constructor(pictureName) {
        super();

        this._permPictureName = pictureName;
        this._z = DEFAULT_Z.NORMAL;
        this._diff = 0;
        this._lerpValue = 1;
        this.isShown = false;
    }

    getVectorArr() {
        return [this._x, this._y];
    }

    setVectorArr(arr) {
        this.setX(arr[0]);
        this.setY(arr[1]);
    }
    
    setX(x) {
        this._x = x;
    }

    setY(y) {
        this._y = y;
    }
    
    setZ(value) {
        this._z = value;

        if (this.isShown) {
            console.log("Improve performance by setting z before showing: " + this._permPictureName);
            $pictureManagerCorn.sortPictureList();
        }
    }

    getZ() {
        return this._z;
    }

    show(origin, x, y, scaleX, scaleY, opacity, blendMode) {


        super.show(this._permPictureName, origin, x, y, scaleX, scaleY, opacity, blendMode);
        $pictureManagerCorn._show(this);
    }

    setPictureName(name) {
        this._name = name;
    }

    set(pictureName, origin, x, y, z, scaleX, scaleY, opacity, blendMode, tone, angle) {
        this.setZ(z);
        this._permPictureName = pictureName;

        this._name = this._permPictureName;
        this._origin = origin;
        this._x = x;
        this._y = y;
        this._scaleX = scaleX;
        this._scaleY = scaleY;
        this._opacity = opacity;
        this._blendMode = blendMode;
        if (Array.isArray(tone)) {
            tone = tone.toArray();
        }
        this._tone = tone;
        this._angle = angle;
    }

    erase() {
        $pictureManagerCorn._erase(this);
    }

    _erase() {
        this.initTarget();
        super.erase();
    }

    setAngle(angle) {
        this._angle = angle;
    }

    isVisible() {
        return this._name != '';
    }

    lerpMovement(targetX, targetY, duration) {
        this._targetX = targetX;
        this._targetY = targetY;
        this._duration = duration;
    }

    setScale(scale) {
        this._scaleX = scale;
        this._scaleY = scale;
    }

    setScaleSpec(scaleX, scaleY) {
        this._scaleX = scaleX;
        this._scaleY = scaleY;
    }

    setOpacity(opacity) {
        this._opacity = opacity;
    }
}

/**
 * AnimationCornMessenger:
 * Receives data from and sends data to AnimationCornWorker
 * Communicates with all Animators 
 * */
const PATH_TO_WORKERS = "cornflour/workers/";

class AnimationCornMessenger {
    constructor() {
        if (window.Worker) {
            this._worker = new Worker(PATH_TO_WORKERS + "AnimationCornWorker.js");
            this._worker.parent = this;

            this._worker.onmessage = function (event) {
                this.parent.receiveMessage(event);
            }
        } else {
            throw "Sorry, web worker is not working :((\nCannot play game.";
            // todo: may be support more crazy async stuff or dependency injection magic over here
        }
    }

    static getInstance() {
        if (AnimationCornMessenger._INSTANCE === null) {
            AnimationCornMessenger._INSTANCE = new AnimationCornMessenger();
        }

        return AnimationCornMessenger._INSTANCE;
    }

    static cleanUp() {
        AnimationCornMessenger._INSTANCE = null;
    }

    receiveMessage(event) {
        console.log(event);
    }
}

AnimationCornMessenger._INSTANCE = null;

/**
 * AnimatorCorn:
 * Receives data on what to show in a PictureCorn
 * Holds all PictureCorn that is associated with an object */
class AnimatorCorn{
    constructor() {
        // todo FSM here
    }

    receiveMessage(data) {
        // todo separate data and set it to pictureCorn
    }

    clear() {
        // todo
    }
}

// global
var $pictureManagerCorn = new PictureManagerCorn();